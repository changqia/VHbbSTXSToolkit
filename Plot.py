#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
from ROOT import *
import numpy as np
import Plot as plot
import math
import colorsys
import random

ROOT.gROOT.LoadMacro("macros/AtlasStyle.C")
ROOT.gROOT.LoadMacro("macros/AtlasUtils.C")
ROOT.gROOT.LoadMacro("macros/AtlasLabels.C")
ROOT.SetAtlasStyle()


debug = False
drawsuf = 'E'
suffix = ['.png','.pdf','.eps', '.root']
outputDir = './'

Number = {
  'Inclusive': [0.1,0.1,0.1,0.2,0.15],
  'GE1J': [0.2,0.1,0.2,0.1,0.35],
  'GE2J': [0.5,0.4,0.3,0.2,0.1],
}

def makeHistograms(nums,fillAbs=True,AddSum=False,binNames = [], acc = []):
  hists = []
  bins =-1
  num_for_sum = []
  num_for_cumulative = []
  for num in nums:
    print 'Plotting the histogram for %s'%num
    if bins == -1: 
     bins = len(nums[num])
     print 'bin size is %d'%bins
    hist = ROOT.TH1F(num,num,bins,0,bins)
    for i,v in enumerate(nums[num]):
      if AddSum:
        if i == 0: index =0
        else: index = 3*((i-1)//3)+1
        relative_error_above_list =  nums[num][index:len(nums[num])]
        acceptance_above_list     =  acc[index:len(acc)]
        #for value in [error * acceptance for error, acceptance in zip(relative_error_above_list, acceptance_above_list)]:
        #  print '--> the content of the error*acceptance: %f'%value
        absolute_error_above = sum([error * acceptance for error, acceptance in zip(relative_error_above_list, acceptance_above_list)])
        #print 'above %s, the above error is %f, from %s'%(binNames[i],absolute_error_above,num)
        if (len(num_for_sum)) >= i+1: 
          num_for_sum[i] += v*v
          num_for_cumulative[i] += absolute_error_above*absolute_error_above
        else:
          num_for_sum.append(v*v)
          num_for_cumulative.append( absolute_error_above*absolute_error_above )
      if fillAbs: hist.SetBinContent(i+1,abs(v))
      else: hist.SetBinContent(i+1,v)
      if len(binNames) != 0: hist.GetXaxis().SetBinLabel(i+1,TransBinName(binNames[i]))
    SetHistStyle(hist,num)
    hists.append(hist)
  # add the histSum
  histCum = None
  if AddSum: 
    histSum = ROOT.TH1F('Sum','Total',len(num_for_sum),0,len(num_for_sum))
    for i,v in enumerate(num_for_sum):
      histSum.SetBinContent(i+1,math.sqrt(v))
    SetHistStyle(histSum,histSum.GetName())
    histCum = ROOT.TH1F('Cumulative','cumulative',len(num_for_cumulative),0,len(num_for_cumulative))
    for i,v in enumerate(num_for_cumulative):
      if i == 0: index =0
      else: index = 3*((i-1)//3)+1
      #above_error = sum(num_for_cumulative[index:len(num_for_cumulative)])
      above_acceptance = sum(acc[index:len(acc)])
      print 'above %25s, the error is %5f, and the acc is %5f'%(binNames[i],math.sqrt(num_for_cumulative[i]),above_acceptance)
      histCum.SetBinContent(i+1,math.sqrt(num_for_cumulative[i])/above_acceptance)
      if len(binNames) != 0: histCum.GetXaxis().SetBinLabel(i+1,TransBinName(binNames[i],True))
    SetHistStyle(histCum,histCum.GetName())
    hists.append(histSum)
    #hists.append(histCum)
  return hists,histCum

def SetHistStyle(hist,name):
  color = GetColor(name)
  style = GetLineStyle(name)
  hist.SetLineColor(color)
  hist.SetLineStyle(style)
  hist.SetLineWidth(3)

def GetLineStyle(name):
  if name.endswith('_2'): return 7 # dashed line
  elif name == 'Cumulative': return 7
  else: return 1

def GetColor(name):
  if name == 'Inclusive': return ROOT.kBlue+2
  elif name == 'GE1J': return ROOT.kRed
  elif name == 'GE2J': return ROOT.kBlue
  elif name == 'Y': return ROOT.kBlue+2
  elif name == 'Cumulative': return ROOT.kOrange+7
  #elif name == '75': return ROOT.kRed
  #elif name == '150': return ROOT.kBlue
  #elif name == '250': return ROOT.kGreen+2
  #elif name == '400': return ROOT.kMagenta+2
  elif name.startswith('0'): return ROOT.kBlue+2
  elif name.startswith('75'): return ROOT.kRed
  elif name.startswith('150'): return ROOT.kBlue
  elif name.startswith('250'): return ROOT.kGreen+2
  elif name.startswith('400'): return ROOT.kMagenta+2
  # new set
  elif name.startswith('QCD1'): return ROOT.kBlue+2
  elif name.startswith('QCD2'): return ROOT.kRed
  elif name.startswith('QCD4'): return ROOT.kBlue
  elif name.startswith('QCD5'): return ROOT.kGreen+2
  elif name.startswith('QCD7'): return ROOT.kMagenta+2
  elif name.startswith('QCD8'): return ROOT.kOrange+7
  return ROOT.kBlack

def TransBinName(name, doMerge = False):
  if doMerge:
    short_name = name.replace('QQ2HLNU_','').replace('PTV_','').replace('_',',').replace('GT','')
    #if short_name.endswith('0J'): return '['+short_name.split(',')[0]
    #elif short_name.endswith('1J'): return '-'
    #elif short_name.endswith('GE2J'): return '#infty]'
    if short_name.endswith('0J'): return ''
    elif short_name.endswith('1J'): return '#geq'+short_name.split(',')[0]
    elif short_name.endswith('GE2J'): return ''
    else: return short_name
  if name == '0': return     '[0,75]or[0,#infty]'
  elif name == '75': return  '[75,150]or[75,#infty]'
  elif name == '150': return '[150,250]or[150,#infty]'
  elif name == '250': return '[250,400]or[250,#infty]'
  elif name == '400': return '[400,#infty]'
  else: return name.replace('QQ2HLNU_','').replace('PTV_','').replace('_',',')

def TransName(name):
  if name == '0': return '#Delta 0'
  elif name == '75': return '#Delta 75'
  elif name == '150': return '#Delta 150'
  elif name == '250': return '#Delta 250'
  elif name == '400': return '#Delta 400'
  elif '_1'in name: return '#Delta '+name.replace('_1',' 1 J')
  elif '_2'in name: return '#Delta '+name.replace('_2',' #geq 2 J')
  elif name == 'inclusive': return '>= PTV cut'
  elif name == 'GE1J': return '>= 1 Jet cut in each PTV bin'
  elif name == 'GE2J': return '>= 2 Jet cut in each PTV bin'
  elif name.startswith('QCD1'): return '(0.5,0.5)'
  elif name.startswith('QCD2'): return '(0.5,1.0)'
  elif name.startswith('QCD4'): return '(1.0,0.5)'
  elif name.startswith('QCD5'): return '(1.0,2.0)'
  elif name.startswith('QCD7'): return '(2.0,1.0)'
  elif name.startswith('QCD8'): return '(2.0,2.0)'
  else: return name

def main(numbs,maximum = 1.0,minimum = -0.1, outputname = 'RelativeErrors', fillAbs=True, AddSum=False,  binNames = [], acc = [], leg_pos = [0.9, 0.6, 0.95, 0.9], autoLogY = True):
  ROOT.gStyle.SetPaintTextFormat("1.4f")
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetPalette(1)
  ROOT.gROOT.SetBatch(1)
  histograms,histCum = makeHistograms( numbs, fillAbs, AddSum, binNames , acc = acc )
  c1 = ROOT.TCanvas( 'c1', 'Histogram Drawing Options', 800, 600)
  logY = False
  if autoLogY:
    for h in histograms:
      if h.GetName() == 'maximum':
        logY = True
        break
  histograms[0].SetMaximum(maximum)
  if logY:
    c1.SetLogy()
  else:
    histograms[0].SetMinimum(minimum)
  #leg = ROOT.TLegend( 0.9, 0.6, 0.95, 0.9 )
  #leg = ROOT.TLegend( 0.7, 0.6, 0.75, 0.9 )
  leg = ROOT.TLegend( leg_pos[0], leg_pos[1], leg_pos[2], leg_pos[3] )
  leg.SetFillStyle(0)
  leg.SetBorderSize(0)
  leg.SetTextSize(0.02)
  for h in histograms:
    h.SetName(TransName(h.GetName()))
    #h.Draw('sameTEXT45')
    if h.GetName() == 'maximum':
      h.Draw('sameTEXT45')
    else:
      h.Draw('same')
      leg.AddEntry(h, h.GetName(), 'l')
  leg.Draw()
  if (outputname == 'ggZllH125_RelativeErrorsIncPTVMerge'):
    sampleLabel= "\\sqrt{s} = 13 TeV, gg\\rightarrow ZH";
  elif (outputname == 'qqWlvH125P_RelativeErrorsIncPTVMerge'):
    sampleLabel= "\\sqrt{s} = 13 TeV, qq\\rightarrow WH";
  elif (outputname == 'qqZllH125_RelativeErrorsIncPTVMerge'):
    sampleLabel= "\\sqrt{s} = 13 TeV, qq\\rightarrow ZH";
  if (outputname == 'ggZllH125_RelativeErrorsIncPTVMerge' or outputname == 'qqWlvH125P_RelativeErrorsIncPTVMerge' or outputname == 'qqZllH125_RelativeErrorsIncPTVMerge' ):
    myText( 0.50, 0.35, 1, "%s" % sampleLabel );
    ATLASLabel( 0.50, 0.25,"Simulation Internal", 1 );
  #c1.Print('%s'%outputname+suffix)
  CanvasOutput(c1,'%s'%outputname)
  if histCum is not None:
    c2 = ROOT.TCanvas( 'c2', 'Histogram Drawing Options', 800, 600)
    histCum.SetName(TransName(h.GetName()))
    histCum.Draw('same')
    histCum.Draw('sameTEXT45')
    #c2.Print('%s.cumulative'%outputname+suffix)
    CanvasOutput(c2,'%s.cumulative'%outputname)

def CanvasOutput(c,name):
  full_name = outputDir + '/' + name
  for suf in suffix:
    c.Print(full_name+suf)

if __name__ == '__main__':
  main(Number)
