#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import numpy as np
import Plot as plot
import math
import colorsys
import random
import sys
import os
from collections import OrderedDict
ROOT.gStyle.SetPaintTextFormat("1.2f")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.SetAtlasStyle()

import socket
LocalRun = True
hostname =  socket.gethostname()
if hostname.startswith('lxplus'):
  LocalRun = False
  print 'Run on lxplus'
  print hostname
else:
  print 'You run locally, you need define the localtion of the inputs'
  print hostname

debug = True
logscale = True
#drawsuf = 'E'
ShowPercentage = False
ShowError      = False
doNorm = False # normalized back to inclusive
WithOutEWCorrection = False # default False, we applied EW correction
UsePaoloApproach = False # default False, we follows Frank's suggestion

#SplitScheme = 'Fine'
SplitScheme = 'Nicest'
corrFact    = 0.5
ShowAllSys = True # show all, or only PTV
MCType = 'mc16a'

#InitialYieldsFile = 'Initial/hist-sample.root'
#InitialYieldsFile = 'Stag1PP.09/hist-sample.root'
#InitialYieldsFile = 'Stag1PP.08.NominalNoEW/hist-sample.root'
#InitialYieldsFile = 'Stag1PP.08.AddingNoEW/hist-sample.root'
#InitialYieldsFile = 'submitDir_Adv_Stage08PP_Nominal_PDF0/hist-sample.root'
#InitialYieldsFile = 'submitDir_Adv_Stage09PP/hist-sample.root'
#InitialYieldsFile = 'submitDir_Adv_Stage09PP_No_normalization/hist-sample.root'
#InitialYieldsFile = 'submitDir_Adv_Stage11.NoCutOff.OnlyWpH.FixQCDToRaw/hist-sample.root'
#InitialYieldsFile = 'submitDir_Adv.NoCutOff.QCD.Fixed/hist-sample.root'
#InitialYieldsFile = 'submitDir_Stage13.CutOffAt50.OnlyWpH/hist-sample.root'
#InitialYieldsFile = 'submitDir_Stage13.CutOffAt50.All/hist-sample.root'
#InitialYieldsFile = 'submitDir_Stage15.CutOffAt50.OnlyWpH/hist-sample.root'
#InitialYieldsFile = 'submitDir_Stage15.NoCutOff.all/hist-sample.root'
#InitialYieldsFile = '/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_mc16d_ICHEP_update/hist-sample.root'
# set the input files
if LocalRun:
  InitialYieldsFile = '/Users/changqiao/Workspace/STXS/DxAOD_output_stage16/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'
else:
  InitialYieldsFile = '/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'

OutputName = 'QCDScaleVariationCalculation.'+(SplitScheme)+('.NOEW'if WithOutEWCorrection else '')+'.v16'
if not os.path.exists(OutputName):
  os.makedirs(OutputName)
plot.outputDir = OutputName

'''
signal_names = {
 'qqWlvH125P': [0.108535*3  , 94.26,  'QQ2HLNU'],
 'qqWlvH125M': [0.108535*3  , 59.83,  'QQ2HLNU'],
 'qqZllH125' : [0.0335962*3 , 25.68,  'QQ2HLL'],
 'ggZllH125' : [0.0335962*3 , 4.14,   'GG2HLL'],
 'qqZvvH125' : [0.201030    , 153.05, 'QQ2HNUNU'],
 'ggZvvH125' : [0.201030    , 24.57,  'GG2HNUNU'],
}
'''
#	name of signal|	br fraction| 	XS|		name filter|	QCD scale variation from YR
signal_names = OrderedDict( [
 	('qqWlvH125P',	[0.108535*3,	94.26*3,	'QQ2HLNU',	0.007]),
	('qqWlvH125M',	[0.108535*3,	59.83*3,	'QQ2HLNU',	0.007]),
	('qqZllH125',	[0.0335962*3,	25.68,		'QQ2HLL',	0.006]),
	('ggZllH125',	[0.0335962*3,	4.14,		'GG2HLL',	0.250]),
	('qqZvvH125',	[0.201030,	153.05,		'QQ2HNUNU',	0.006]),
	('ggZvvH125',	[0.201030,	24.57,		'GG2HNUNU',	0.250]),
]
)

def GetRangeForPlot(signame,plotType):
  Range = {}
  Range['Absolute'] = {
   'qqWlvH125P': [7.0, 0.0001],
   'qqWlvH125M': [7.0, 0.0001],
   'qqZllH125' : [7.0, 0.0001],
   'qqZvvH125' : [7.0, 0.0001],
   'ggZllH125' : [7.0, 0.0001],
   'ggZvvH125' : [7.0, 0.0001],
  }

  Range['NJET'] = {
   'qqWlvH125P': [0.14, -0.09],
   'qqWlvH125M': [0.14, -0.09],
   'qqZllH125' : [0.14, -0.09],
   'qqZvvH125' : [0.14, -0.09],
   'ggZllH125' : [0.35, -4.00],
   'ggZvvH125' : [0.35, -4.00],
  }

  Range['PTV'] = {
   'qqWlvH125P': [0.05, -0.04],
   'qqWlvH125M': [0.05, -0.04],
   'qqZllH125' : [0.07, -0.04],
   'qqZvvH125' : [0.07, -0.04],
   'ggZllH125' : [1.40, -1.40],
   'ggZvvH125' : [1.40, -1.40],
  }
  Range['Relative'] = {
   'qqWlvH125P': [0.15, 0.00],
   'qqWlvH125M': [0.15, 0.00],
   'qqZllH125' : [0.15, 0.00],
   'qqZvvH125' : [0.15, 0.00],
   'ggZllH125' : [0.30, 0.24],
   'ggZvvH125' : [0.30, 0.24],
  }
  Range['RelativeIncPTV'] = {
   'qqWlvH125P': [0.06, 0.00],
   'qqWlvH125M': [0.06, 0.00],
   'qqZllH125' : [0.06, 0.00],
   'qqZvvH125' : [0.06, 0.00],
   'ggZllH125' : [0.60, 0.00],
   'ggZvvH125' : [0.60, 0.00],
  }

  return Range[plotType][signame]

def GetSTXSName(signame):
    name = ''
    TransNameDict = {
      #'ggZllH125' :'GG2HLL',
      #'ggZvvH125' :'GG2HNUNU',
      'ggZllH125' :'GG2HLL',
      'ggZvvH125' :'GG2HLL',
      #'qqWlvH125M':'QQ2HLNUM',
      #'qqWlvH125P':'QQ2HLNUP',
      'qqWlvH125M':'QQ2HLNU',
      'qqWlvH125P':'QQ2HLNU',
      #'qqZllH125' :'QQ2HLL',
      #'qqZvvH125' :'QQ2HNUNU',
      'qqZllH125' :'QQ2HLL',
      'qqZvvH125' :'QQ2HLL',
    }
    if signame in TransNameDict:
      return TransNameDict[signame]
    else:
      print 'not found %s in GetSTXSName'%signame
      exit(0)

weight_var   = [
  'Weight0',
  'Nominal', # nominal w/ EW correction
  'PDF1',
  'PDF2',
  'PDF3',
  'PDF4',
  'PDF5',
  'PDF6',
  'PDF7',
  'PDF8',
  'PDF9',
  'PDF10',
  'PDF11',
  'PDF12',
  'PDF13',
  'PDF14',
  'PDF15',
  'PDF16',
  'PDF17',
  'PDF18',
  'PDF19',
  'PDF20',
  'PDF21',
  'PDF22',
  'PDF23',
  'PDF24',
  'PDF25',
  'PDF26',
  'PDF27',
  'PDF28',
  'PDF29',
  'PDF30',
  'alphaSup',
  'alphaSdn',
  'QCD1',
  'QCD2',
  'QCD3',
  'QCD4',
  'QCD5',
  'QCD6',
  'QCD7',
  'QCD8',
]

Sigs = []

''' histograms toolkit '''

def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

'''plotting function'''

def drawRelativeErrors(qcd,maximum=0.11,minimum=-0.01):
  Number,bin_names = qcd.get_global_relative_error_map()
  print 'drawRelativeErrors() bin names are:',
  for Bin in bin_names:
    print ' %s'%Bin,
  print
  histograms = plot.main(Number,maximum,minimum,qcd.get_sig_name()+'_RelativeErrors', binNames = bin_names, leg_pos = [0.5, 0.6, 0.55, 0.9])

def drawAbsoluteErrors(qcd,maximum=0.11,minimum=-0.01):
  Number,bin_names = qcd.get_global_absolute_error_map()
  print 'drawAbsoluteErrors() bin names are:',
  for Bin in bin_names:
    print ' %s'%Bin,
  print
  print Number
  histograms = plot.main( Number, maximum, minimum, qcd.get_sig_name()+'_AbsoluteErrors', binNames = bin_names, leg_pos = [0.7, 0.6, 0.75, 0.9])

def drawRelativeErrors_inclusivePTV(qcd,maximum=0.11,minimum=0.0):
  Number,bin_names = qcd.get_global_relative_error_map_inclusive_ptv()
  print 'drawRelativeErrors_inclusivePTV() bin names are:',
  for Bin in bin_names:
    print ' %s'%Bin,
  print
  print Number
  histograms = plot.main( Number, maximum, minimum, qcd.get_sig_name()+'_RelativeErrorsIncPTV', binNames = bin_names, leg_pos = [0.7, 0.6, 0.75, 0.9], autoLogY = False)

def drawRelativeErrors_inclusivePTV_merge(qcd, qcd_couple, maximum=0.11, minimum=0.0, doWHscale = False):
  Number,bin_names = qcd.get_global_relative_error_map_inclusive_ptv()
  Number_couple = qcd_couple.get_global_relative_error_map_inclusive_ptv()[0]
  Number_merge = {}
  print 'drawRelativeErrors_inclusivePTV_merge() bin names are:',
  for var in Number:
    print 'var in Number %s'%var,
    Variations = Number[var] # it is a list for qqWlvH125P
    Variations_couple = Number_couple[var]
    if doWHscale:
      #if var == 'maximum':
      #  print 'The maximum of WH,'
      #  print Variations
      #  print Variations_couple
      # here I am using the absolute value, but in same case, the direction of the variation are opposite 
      Variations_merge = [(abs(x)*282.780+abs(y)*179.49)/(282.780+179.49) for x,y in zip(Variations,Variations_couple)]
    else:
      Variations_merge = [(abs(x)+abs(y))/2 for x,y in zip(Variations,Variations_couple)]
    Number_merge[var] = Variations_merge
    # then merge these two list
  histograms = plot.main( Number_merge, maximum, minimum, qcd.get_sig_name()+'_RelativeErrorsIncPTVMerge', binNames = bin_names, leg_pos = [0.7, 0.6, 0.75, 0.9], autoLogY = False)

def drawRelativeErrorFromEachDelta(qcd,maximum=0.05,minimum=-0.025,outputname = 'RelativeErrorFromEachDelta',max_njet=0.35,min_njet=-0.15):
  ptvs,njets = qcd.get_relative_err_for_each_bin()
  var = qcd.qcd_vars[0]
  bin_names = []
  acceptance = []
  for vb in var.var_bins:
    name = vb.bin_name
    acc  = vb.bin_acceptance
    bin_names.append(name)
    acceptance.append(acc)
  print 'length of the ptvs: %d'%len(ptvs)
  plot.main(ptvs,maximum,minimum,qcd.get_sig_name()+'_'+outputname+'PTVwithCorrFact%s'%str(corrFact), False, True, bin_names, acc=acceptance ) # for False, mean we don't fill the absolute values
  plot.main(njets,max_njet,min_njet,qcd.get_sig_name()+'_'+outputname+'NJET', False, binNames = bin_names) # for False, mean we don't fill the absolute values

def SetupTopFrame(hist):
  hist.GetYaxis().SetTitleOffset(1.0)
  hist.GetXaxis().SetTitleOffset(1.4)
  hist.SetLabelOffset(0.005)

  hist.GetYaxis().SetTitleSize(0.04)
  hist.GetXaxis().SetTitleSize(0.04)

  Size = 0.02
  hist.GetXaxis().SetLabelSize(Size)
  hist.GetYaxis().SetLabelSize(0.04)

def SetupTopFrameAdv(hist,xtitle,ytitle,maximum):
  hist_frame = clone(hist)
  hist_frame.SetTitle("")
  SetupTopFrame(hist_frame)
  hist_frame.GetXaxis().SetTitle(xtitle)
  hist_frame.GetYaxis().SetTitle(ytitle)
  if logscale :
    bit = math.floor(math.log10(maximum))
    factor = math.pow(10,math.floor(0.3*bit))
    maximum_log = maximum*factor
    if debug:
      print "raw maximum ",maximum
      print "scale factor ",factor
      print "log maximum ",maximum_log
    hist_frame.SetMaximum(maximum_log)
  else :
    hist_frame.SetMaximum(maximum*1.5)
  return hist_frame

def SetupPad(TopPad,BotPad):
  #TopPad.SetBottomMargin(0.02)
  #TopPad.SetTopMargin(0.05)
  TopPad.SetRightMargin(0.0)
  TopPad.SetBottomMargin(0.14)

  #BotPad.SetTopMargin(0.03)
  #BotPad.SetBottomMargin(0.3)
  BotPad.SetLeftMargin(0.0)
  BotPad.SetRightMargin(0.0)

def SetErrorZero(hist):
  Nbins = hist.GetXaxis().GetNbins()
  if debug:
    print 'NBins: ',Nbins
  for count in range(0, Nbins):
    hist.SetBinError(count+1,0.00000000000000001)
    #hist.SetBinError(count+1,0.)

def GetError(Ea,Va,Eb,Vb):
  if Va == 0 : return 0.
  return math.sqrt((Ea/Va)*(Ea/Va)+(Eb/Vb)*(Eb/Vb))*Va/Vb

can_width=2400
can_height = 1200
c1 = ROOT.TCanvas("Yields","Yields",can_width,can_height)
p_l = ROOT.TPad("pl","pl",0.,0.,0.9,1.)
p_r = ROOT.TPad("pr","pr",0.9,0.,1.,1.)
SetupPad(p_l,p_r)
p_r.Draw()
p_l.Draw()
if logscale : p_l.SetLogy()
HistVar={}
rootFile = ROOT.TFile(InitialYieldsFile)
leg = ROOT.TLegend( 0.2, 0.1, 0.8, 0.9 )
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextSize(0.08)
#HIST = rootFile.Get(name)
maximum  = -999
icolor   = 1
#color_list_adv = GetNColor(len(weight_var))
Yields = {}
OrderBins = []

# Fill the list
for sig in signal_names:
  normFact = signal_names[sig][0]
  Var = {}
  for var in weight_var:
    if debug: print 'collecting: ',var.replace('_','')
    # find the initial binned histograms
    # ggZllH125_wEW_Stage1
    if WithOutEWCorrection:
      histoname = sig+'_'+var+'_Stage1'+SplitScheme
    else:
      histoname = sig+'_'+var+'NOEW_Stage1'+SplitScheme
    if debug: print 'histname: %s'%histoname
    histo_initial = rootFile.Get(histoname)
    if debug: 
      print 'histname: %s with Entries %i'%(histoname, histo_initial.GetEntries())
      print 'histname: %s with Yields  %f'%(histoname, histo_initial.Integral())
    x_axis=histo_initial.GetXaxis()
    Nbins=x_axis.GetNbins()
    Bins = {}
    FillOrderBins = False
    if len(OrderBins) == 0 : FillOrderBins = True
    for count in range(0, Nbins):
      name = x_axis.GetBinLabel(count+1)
      if FillOrderBins: OrderBins.append(name)
      yields = histo_initial.GetBinContent(count+1)
      error  = histo_initial.GetBinError(count+1)
      if debug: print 'No. %2d: %25s: %10.2f'%(count+1,name,yields)
      if (name == 'TOTAL') and doNorm:
        if debug: print 'normFact = %f'%normFact
        Bins[name]={'Value':yields/normFact,'Error':error/normFact}
      else:
        Bins[name]={'Value':yields,'Error':error}
    Var[var]=Bins
  Yields[sig]=Var
      
if debug:
  print Yields
  print OrderBins

def TransName(var):
  if   var is 'alphaSup': return 'aSup'
  elif var is 'alphaSdn': return 'aSdn'
  elif var is 'PDF1':     return 'EV1'
  elif var is 'PDF2':     return 'EV2'
  elif var is 'PDF3':     return 'EV3'
  elif var is 'PDF4':     return 'EV4'
  elif var is 'PDF5':     return 'EV5'
  elif var is 'PDF6':     return 'EV6'
  elif var is 'PDF7':     return 'EV7'
  elif var is 'PDF8':     return 'EV8'
  elif var is 'PDF9':     return 'EV9'
  elif var is 'PDF10':    return 'EV10'
  elif var is 'PDF11':    return 'EV11'
  elif var is 'PDF12':    return 'EV12'
  elif var is 'PDF13':    return 'EV13'
  elif var is 'PDF14':    return 'EV14'
  elif var is 'PDF15':    return 'EV15'
  elif var is 'PDF16':    return 'EV16'
  elif var is 'PDF17':    return 'EV17'
  elif var is 'PDF18':    return 'EV18'
  elif var is 'PDF19':    return 'EV19'
  elif var is 'PDF20':    return 'EV20'
  elif var is 'PDF21':    return 'EV21'
  elif var is 'PDF22':    return 'EV22'
  elif var is 'PDF23':    return 'EV23'
  elif var is 'PDF24':    return 'EV24'
  elif var is 'PDF25':    return 'EV25'
  elif var is 'PDF26':    return 'EV26'
  elif var is 'PDF27':    return 'EV27'
  elif var is 'PDF28':    return 'EV28'
  elif var is 'PDF29':    return 'EV29'
  elif var is 'PDF30':    return 'EV30'
  else : return var

def GenerateAcceptanceDiff():
  weight_show   = ['Nominal']
  for sig in signal_names:
    f = open('%s/%s.acceptance.nomi.txt'%(OutputName,sig),'w+')
    temp = sys.stdout
    sys.stdout = f
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_var:
      if ShowPercentage: print '%6s'%var,
      #else : print '%10s'%var, 
      else : print '%6s'%TransName(var), 
    print
    for Bin in OrderBins:
      print '%25s,'%Bin,
      for var in weight_var:
        if var == "Nominal" : continue
        Va = Yields[sig][var][Bin]['Value']
        reference = Yields[sig][var]['TOTAL']['Value']
        value = Va
        if value != 0 and reference!=0 :
          acceptance = value/reference
          acceptance_central = Yields[sig]["Nominal"][Bin]['Value'] / Yields[sig]["Nominal"]['TOTAL']['Value']
          print '%6.4f'%acceptance,
          #print '%6.4f'%(( acceptance - acceptance_central ) / acceptance_central ),
        else:
          print '%6s'%'-',
      print '|'
    f.close()

def GenerateAcceptance():
  weight_show   = ['Nominal']
  for sig in signal_names:
    f = open('%s/%s.acceptance.nomi.txt'%(OutputName,sig),'w+')
    temp = sys.stdout
    sys.stdout = f
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_show:
      if ShowPercentage: print '%6s'%var,
      #else : print '%10s'%var, 
      else : print '%6s'%TransName(var), 
    print
    for Bin in OrderBins:
      print '%25s,'%Bin,
      for var in weight_show:
        Va = Yields[sig][var][Bin]['Value']
        reference = Yields[sig][var]['TOTAL']['Value']
        value = Va
        if value != 0 and reference!=0 :
          acceptance = value/reference
          print '%6.4f'%acceptance,
          #print '%6.4f'%(( acceptance - acceptance_central ) / acceptance_central ),
        else:
          print '%6s'%'-',
      print '|'
    f.close()

def GenerateYieldsDiffForQCD():
  weight_show   = [ 'Weight0', 'QCD1', 'QCD2', 'QCD3', 'QCD4', 'QCD5', 'QCD6', 'QCD7', 'QCD8'  ]
  for sig in signal_names:
    f = open('%s/%s.yields.qcd.txt'%(OutputName,sig),'w+')
    temp = sys.stdout
    sys.stdout = f
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_show:
      if var == weight_show[0] : continue
      print '%8s'%TransName(var), 
    print
    for Bin in OrderBins:
      print '%25s,'%Bin,
      for var in weight_show:
        if var == weight_show[0] : continue
        value = Yields[sig][var][Bin]['Value']
        if value != 0 :
          value_central = Yields[sig][weight_show[0]][Bin]['Value']
          print '%7.4f,'%(( value - value_central ) / value_central ),
        else:
          print '%8s'%'-',
      print '|'
    f.close()


def GenerateYieldsDiffForPDF():
  weight_show   = [ 'Nominal', 
                    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
                    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
                    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
                    'alphaSup', 'alphaSdn',
  ]
  for sig in signal_names:
    f = open('%s/%s.yields.pdf.txt'%(OutputName,sig),'w+')
    temp = sys.stdout
    sys.stdout = f
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_show:
      if var == weight_show[0] : continue
      print '%8s'%TransName(var), 
    print
    for Bin in OrderBins:
      print '%25s,'%Bin,
      for var in weight_show:
        if var == weight_show[0] : continue
        value = Yields[sig][var][Bin]['Value']
        if value != 0 :
          value_central = Yields[sig][weight_show[0]][Bin]['Value']
          print '%7.4f,'%(( value - value_central ) / value_central ),
        else:
          print '%8s'%'-',
      print '|'
    f.close()

def GenerateYields():
  weight_show   = [ 
    'Nominal', 
#    'Weight0', 
#    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
#    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
#    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
#    'alphaSup', 'alphaSdn',
#    'QCD1', 
#    'QCD2', 
#    'QCD3', 
#    'QCD4', 
#    'QCD5', 
#    'QCD6', 
#    'QCD7', 
#    'QCD8',
  ]
  f = open('%s/all.yields.check.txt'%(OutputName),'w+')
  temp = sys.stdout
  sys.stdout = f
  for sig in signal_names:
    '''
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_show:
      print '%25s,'%TransName(var), 
    print
    '''
    for Bin in OrderBins:
      '''check name'''
      checkname = signal_names[sig][2]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      print '%25s,'%Bin,
      for var in weight_show:
        value = Yields[sig][var][Bin]['Value']
        if value != 0 :
          print '%9.1f,'%value,
        else:
          print '%10s'%'-',
      print '|'
  f.close()

def GenerateAcceptanceAndVariation():
  weight_show   = [ 
    'Nominal', 
    'Weight0', 
    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
    'alphaSup', 'alphaSdn',
    'QCD1', 'QCD2',  'QCD3',  'QCD4',  'QCD5',  'QCD6',  'QCD7',  'QCD8', 
  ]
  f = open('%s/all.AccAndVar.txt'%(OutputName),'w+')
  temp = sys.stdout
  sys.stdout = f
  for sig in signal_names:
    '''
    print 'This is for %s'%sig
    print '%26s'%'Bins',
    for var in weight_show:
      print '%25s,'%TransName(var), 
    print
    '''
    for Bin in OrderBins:
      '''check name'''
      checkname = signal_names[sig][2]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      print '%25s,'%Bin,
      for var in weight_show:
        value = 0

        if var == 'Nominal':
          value = Yields[sig][var][Bin]['Value'] / Yields[sig][var]['TOTAL']['Value'] # we show the acceptance
        elif var.startswith('PDF') or var.startswith('alphaS') :
          value = (Yields[sig][var][Bin]['Value'] / Yields[sig]['Nominal'][Bin]['Value'])*100. - 100.
        elif var.startswith('QCD') :
          value = (Yields[sig][var][Bin]['Value'] / Yields[sig]['Weight0'][Bin]['Value'])*100. - 100.

        if value != 0 :
          print '%9.4f,'%value,
        else:
          print '%10s'%'-,',
      print '|'
  f.close()

def PrepareQCDScaleVariation(sig):
  weight_show   = [
    'QCD1',
    'QCD2',
#    'QCD3',
    'QCD4',
    'QCD5',
#    'QCD6',
    'QCD7',
    'QCD8',
  ]
  # create the sig class
  sig_qcd = SigQCDError()
  sig_qcd.set_sig_name(sig)
  checkname = signal_names[sig][2]
  CrossSection = signal_names[sig][1]
  sig_qcd.set_sig_xs(CrossSection)
  for w in weight_show:
    # create a var class
    var = Variation()
    var.set_var_name(w)
    # then loop all bins of this signal
    for Bin in OrderBins:
      '''
      check name to skip :
        Meanless bins: like QQ2HLNU for ggZH signals
        UNKNOWN bins: the bin doesn't perfrom the bin split successfully!
      '''
      if not Bin.startswith(checkname): continue
      # create a bin class
      bin_info = BinInfo()
      acc =  Yields[sig]['Nominal'][Bin]['Value'] / Yields[sig]['Nominal']['TOTAL']['Value']
      relative_error = Yields[sig][w][Bin]['Value'] / Yields[sig]['Weight0'][Bin]['Value'] - 1.
      bin_info.set_bin_info(Bin,acc,relative_error)
      #if debug: print'------>var.add_one_bin() for %s'%Bin
      var.add_one_bin(bin_info)
    #if debug: print '---->finish the bins adding for var %s'%w
    sig_qcd.add_one_var(var)
  return sig_qcd

def PrepareQCDScaleVariationWithDemo(sig, Demo, DemoBins):
  weight_show   = [
    'QCD1',
  ]
  # create the sig class
  sig_qcd = SigQCDError()
  sig_qcd.set_sig_name(sig)
  checkname = signal_names[sig][2]
  CrossSection = signal_names[sig][1]
  sig_qcd.set_sig_xs(CrossSection)
  for w in weight_show:
    # create a var class
    var = Variation()
    var.set_var_name(w)
    # then loop all bins of this signal
    for Bin in DemoBins:
      '''
      check name to skip :
        Meanless bins: like QQ2HLNU for ggZH signals
        UNKNOWN bins: the bin doesn't perfrom the bin split successfully!
      '''
      if not Bin.startswith(checkname): continue
      # create a bin class
      bin_info = BinInfo()
      #acc =  Yields[sig]['Nominal'][Bin]['Value'] / Yields[sig]['Nominal']['TOTAL']['Value']
      acc = Demo[sig][Bin][0]
      #relative_error = Yields[sig][w][Bin]['Value'] / Yields[sig]['Weight0'][Bin]['Value'] - 1.
      relative_error = Demo[sig][Bin][1]
      bin_info.set_bin_info(Bin,acc,relative_error)
      #if debug: print'------>var.add_one_bin() for %s'%Bin
      var.add_one_bin(bin_info)
    #if debug: print '---->finish the bins adding for var %s'%w
    sig_qcd.add_one_var(var)
  return sig_qcd

class BinInfo:
  def __init__(self):
    self.bin_name = 'not set'
    self.bin_acceptance = -1.0
    self.bin_variation  = -1.0 # relative errors
  def set_bin_info(self,name,acc,rel_err):
    self.set_bin_name(name)
    self.set_bin_acc(acc)
    self.set_bin_err(rel_err)

  def set_bin_name(self,name):
    if self.bin_name == 'not set':
      self.bin_name = name
      self.bin_info = ParseBinName(name)
    else:
      print 'bin name is already set as %s'%bin_name
      exit()
  def get_bin_name(self):
    return self.bin_name
  def set_bin_acc(self,acc):
      self.bin_acceptance = acc
  def get_bin_acc(self):
      return self.bin_acceptance
  def set_bin_err(self,err): # relative errors
      self.bin_variation = err
  def get_bin_err(self):
      return self.bin_variation

class Variation:
  def __init__(self):
    self.var_name     = 'not set'
    self.var_bins     = []
    self.var_ptv_map  = []
    self.var_bin_map  = {}
    self.abs_err      = {}
    self.inclusive_xs = {}
    self.relative_err = {}
  def set_var_name(self,name):
    if self.var_name == 'not set':
      self.var_name = name
    else:
      print 'var name is already set as %s'%var_name
      exit()
  def get_var_name(self):
    return self.var_name
  def add_one_bin(self, onebin):
    #if debug: 
    #  print 'add for bin %26s'%onebin.get_bin_name()
    #  print 'size of the bins is: %d'%len(self.var_bins)
    self.var_bins.append(onebin)
  #def set_xs_for_all_bins(self,cross_section): # and also set the map for calculation
  def perform_calculation_to_get_relative_error(self,cross_section): # and also set the map for calculation
    for vb in self.var_bins:
      xs_for_bin = vb.get_bin_acc()*cross_section
      vb.bin_info['cross_section'] = xs_for_bin
      vb.bin_info['absolute_error'] = xs_for_bin*vb.get_bin_err()
    self.set_ptv_map()
    self.set_njet_map()
    #self.PerformeCalculation()
    self.GenerateTheRelativeErrorForMap()
  def show_bin_var(self):
    for vb in self.var_bins:
      print 'Bin:  %25s, acceptance: %4f, relative error is: %4f'%( vb.get_bin_name(), vb.get_bin_acc(), vb.get_bin_err() )
      for key in vb.bin_info:
        print '  %5s :'%key,vb.bin_info[key]

  def set_ptv_map(self):
    for vb in self.var_bins:
      ptv_for_this_bin = vb.bin_info['ptv_low']
      if ptv_for_this_bin not in self.var_ptv_map: self.var_ptv_map.append(ptv_for_this_bin)
      self.var_ptv_map.sort()

  def set_njet_map(self):
    temp_dict = {}
    ''' insure this is called after get_ptv_map '''
    for ptv_in_map in self.var_ptv_map:
      temp_list = []
      for vb in self.var_bins:
        ptv_for_this_bin = vb.bin_info['ptv_low']
        if ptv_in_map != ptv_for_this_bin: continue
        njet_for_this_bin = vb.bin_info['njet']
        if njet_for_this_bin == -1: continue # that means this bin don't have the njet split 
        temp_list.append(njet_for_this_bin)
      if len(temp_list) > 0: temp_list.sort()
      temp_dict[ptv_in_map] = temp_list
    self.var_bin_map = temp_dict

  def show_the_bin_map(self):
    for ptv in self.var_ptv_map:
      print 'PTV %d -> '%ptv,
      for nj in self.var_bin_map[ptv]:
        print '%dJ '%nj,
      print

  def get_bin_map(self):
    return self.var_ptv_map,self.var_bin_map

  def generate_errors_for_njet(self): # relative 0_1J 0_2J 75_1J 75_2J 150_1J 150_2J 250_1J 250_2J
    abs_err = {}
    inclusive_xs = {}
    relative_err = {}
    for ptv in self.var_ptv_map:
      if ptv == -1: continue
      if len(self.var_bin_map[ptv]) == 0: continue
      elif len(self.var_bin_map[ptv]) == 1:
        print 'var_bin_map gets weired at ptv: %d'%(ptv)
        exit()
      else:
        for njet in self.var_bin_map[ptv][1:]:
          suffix = str(ptv)+'_'+str(njet)
          print 'calculate error for %s'%suffix
          abs_err[suffix]  = self.extract_float_from_bins_above_njet_in_a_given_ptv_bin('absolute_error',ptv,njet)
          inclusive_xs[suffix] = self.extract_float_from_bins_above_njet_in_a_given_ptv_bin('cross_section',ptv,njet)
          relative_err[suffix] = abs_err[suffix]/inclusive_xs[suffix]
          print 'Calculate relative error suffix %s, inclusive_xs %f, relative_err %f'%(suffix,inclusive_xs[suffix],relative_err[suffix])
    self.abs_err.update(abs_err)
    self.inclusive_xs.update(inclusive_xs)
    self.relative_err.update(relative_err)

  def generate_errors_for_ptv(self): # relative Y 75 150 250 
    abs_err = {}
    inclusive_xs = {}
    relative_err = {}
    for ptv in self.var_ptv_map:
      if ptv == -1: suffix = 'Y'
      elif ptv == 0: continue
      else: suffix = str(ptv)
      abs_err[suffix] = 0
      inclusive_xs[suffix] = 0
      '''
      for vb in self.var_bins:
        ptv_for_this_bin = vb.bin_info['ptv_low']
        if ptv_for_this_bin < ptv: continue
        abs_err[suffix] += vb.bin_info['absolute_error']
        inclusive_xs[suffix] += vb.bin_info['cross_section']
      '''
      abs_err[suffix]  = self.extract_float_from_bins_above_ptv('absolute_error',ptv)
      inclusive_xs[suffix] = self.extract_float_from_bins_above_ptv('cross_section',ptv)
      relative_err[suffix] = abs_err[suffix]/inclusive_xs[suffix]
    self.abs_err.update(abs_err)
    self.inclusive_xs.update(inclusive_xs)
    self.relative_err.update(relative_err)

  def get_inclusive_xs(self):
    return self.inclusive_xs

  def extract_float_from_bins_above_ptv(self,info,ptv):
    value = 0.
    for vb in self.var_bins:
      ptv_for_this_bin = vb.bin_info['ptv_low']
      if ptv_for_this_bin < ptv: continue
      value += vb.bin_info[info]
    return value
    
  def extract_float_from_bins_above_njet_in_a_given_ptv_bin(self,info,ptv,njet):
    value = 0.
    for vb in self.var_bins:
      ptv_for_this_bin = vb.bin_info['ptv_low']
      if ptv_for_this_bin != ptv: continue
      njet_for_this_bin = vb.bin_info['njet']
      if njet_for_this_bin < njet: continue
      print 'For ptv ( %d , %d ) includes ( %d , %d )'%(ptv,njet,ptv_for_this_bin,njet_for_this_bin)
      value += vb.bin_info[info]
    return value

  def get_relative_err_map(self):
    return self.relative_err

  def ShowErrors(self):
    for err in self.relative_err:
      print 'relative error %5s is %f'%(err,self.relative_err[err])
    #for err in self.abs_err:
    #  print 'absolute error %s is %f'%(err,self.abs_err[err])

  #def PerformeCalculation(self):
  def GenerateTheRelativeErrorForMap(self):
    self.generate_errors_for_ptv()
    self.generate_errors_for_njet()
    #self.ShowErrors()

class SigQCDError:
  def __init__(self):
    self.sig_name = 'not set'
    self.qcd_vars = []
    self.xs       = -1
    self.global_ptv_bins = []
    self.global_jet_map = {}
    self.global_relative_error = {}
    self.relative_err_order = []
    self.global_Remnant_error = {}
    self.global_Deltas = {}
    self.global_above_ptv_xs = {}
    self.global_compl_ptv_xs = {}
    self.global_above_njet_xs_in_ptv = {}
    self.global_compl_njet_xs_in_ptv = {}
    self.global_x_Deltas = {}
    self.global_delta_abs_err_for_each_bin = {}
    self.global_delta_relative_err_for_each_bin = {}
  def set_sig_name(self,name):
    self.sig_name = name
  def get_sig_name(self):
    return self.sig_name
  def add_one_var(self,var):
    self.qcd_vars.append(var)
    #if debug: print 'Now the size of qcd_vars is %d'%len(self.qcd_vars)
  def set_sig_xs(self,cross_section):
    self.xs = cross_section
  #def set_xs_for_all_vars(self):
  def perform_calculation_for_variation(self):
    if self.xs < 0:
      print 'Attampe to set the cross setion < 0 for all bins in %s, exit!!'%self.sig_namew
      exit()
    for q in self.qcd_vars:
      #q.set_xs_for_all_bins(self.xs)
      q.perform_calculation_to_get_relative_error(self.xs)
    if len(self.qcd_vars) > 0:
      print 'The size of the qcd variation is %d'%len(self.qcd_vars)
      ptv_list,njet_map = self.qcd_vars[0].get_bin_map()
      self.set_global_bin_map(ptv_list,njet_map)
    else:
      print 'Attampe to perfrome the calculation with empty variation exit!!'
      exit()
    # before we move to the calculation for the Remnant relative errors, we collecte relative errors
    self.collect_relative_errors_map()
    self.show_global_relative_error_map()
    self.calculate_remnant_relative_errors()
    #self.show_inclusive_cross_section()
    self.calculate_Deltas()
    self.calculate_x_Deltas()
    self.calculate_relative_for_each_Deltas()
    self.show_relative_err_for_each_bin()
    #self.PrintOutrelative_err_for_each_bin()

  def show_global_relative_error_map(self):
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error:
        print 'relative error %5s for global qcd variation is %.3f'%(err,self.global_relative_error[err])
    for ptv in self.global_ptv_bins:
      for nj in self.global_jet_map[ptv]:
        err = str(ptv)+'_'+str(nj)
        if err in self.global_relative_error:
          print 'relative error %5s for global qcd variation is %.3f'%(err,self.global_relative_error[err])

  def get_global_relative_error_map(self):
    Number = {}
    inclusive = []
    binNames = []
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error:
        inclusive.append(self.global_relative_error[err])
        binNames.append(str(ptv))
    Number['inclusive'] = inclusive
    for nj in self.global_jet_map[ptv]:
      Number_nj = []
      for ptv in self.global_ptv_bins:
        err = str(ptv)+'_'+str(nj)
        if err in self.global_relative_error:
          Number_nj.append(self.global_relative_error[err])
      if len(Number_nj) >0 : Number['GE%dJ'%nj] = Number_nj
    return Number,binNames


  def get_global_absolute_error_map(self):
    Number = {}
    inclusive = []
    binNames = []
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error:
        inclusive.append(self.global_relative_error[err]*self.global_above_ptv_xs[str(ptv)])
        binNames.append('pt > '+str(ptv))
    Number['maximum'] = inclusive
    for q in self.qcd_vars:
      print 'variation name is %s'%q.var_name
      relative_err_map_for_variation = q.get_relative_err_map()
      variation = []
      for ptv in self.global_ptv_bins:
        err = str(ptv) if ptv != 0 else 'Y'
        if err in self.global_relative_error:
          if ptv == 0: relative_error = self.global_relative_error[err] # for the cut > 0, we use global relative error
          else: relative_error = relative_err_map_for_variation[err]
          print ' %f'%(relative_error*self.global_above_ptv_xs[str(ptv)]),
          variation.append(relative_error*self.global_above_ptv_xs[str(ptv)])
      print
      Number[q.var_name] = variation
    return Number,binNames

  def get_global_relative_error_map_inclusive_ptv(self):
    Number = {}
    inclusive = []
    binNames = []
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error:
        inclusive.append(self.global_relative_error[err])
        binNames.append('> '+str(ptv) ) 
    Number['maximum'] = inclusive
    for q in self.qcd_vars:
      print 'variation name is %s'%q.var_name
      relative_err_map_for_variation = q.get_relative_err_map()
      variation = []
      for ptv in self.global_ptv_bins:
        err = str(ptv) if ptv != 0 else 'Y'
        if err in self.global_relative_error:
          if ptv == 0: relative_error = self.global_relative_error[err] # for the cut > 0, we use global relative error
          else: relative_error = relative_err_map_for_variation[err]
          print ' %f'%(relative_error),
          variation.append(relative_error)
      print
      Number[q.var_name] = variation
    return Number,binNames

  def collect_relative_errors_map(self):
    relative_error_map = {}
    for q in self.qcd_vars:
      relative_err_map_for_variation = q.get_relative_err_map()
      for err in relative_err_map_for_variation:
        if err in relative_error_map: # so it is already filled
          err_old = relative_error_map[err]
          err_new = relative_err_map_for_variation[err]
          if abs(err_old) < abs(err_new): # in this case, we update the relative error to get the bigger error
            relative_error_map[err] = err_new
        else:
          relative_error_map[err] = relative_err_map_for_variation[err]
    # here we insert the scale variation for inclusive signal from YR
    err_YR = signal_names[self.sig_name][3]
    print 'Insert the scale variation for inclusive signal from YR: %.3f instead of the estimated one: %.3f value with MC'%(err_YR,relative_error_map['Y'])
    relative_error_map['Y'] = err_YR
    self.global_relative_error = relative_error_map

  def set_global_bin_map(self, ptv_bins, njet_map):
    # TODO, add the check if one to be added is as same as the existing one
    if not self.global_ptv_bins:
      self.global_ptv_bins = ptv_bins
    if not self.global_jet_map:
      self.global_jet_map = njet_map

  def show_bin_map_for_var(self,var_name_to_show):
   for q in self.qcd_vars:
     if debug: print 'Now check if this variation is what we want to show %s'%q.get_var_name()
     if q.get_var_name() == var_name_to_show:
       print 'Show the var info for %s'%var_name_to_show
       q.show_the_bin_map()
       q.ShowErrors()

  def find_up_class_relative_from_map(self,err):
    #print 'look for relative error %s from map'%err
    up_class_name = 'Not decided'
    if '_' in err: # so it is a # of jets relative error
      ptv = int(err.split('_')[0])
      jet = int(err.split('_')[1])
      #print 'This error is %d PTV, >= %d Jet'%(ptv,jet)
      ptv_class_index = self.global_ptv_bins.index(ptv)
      jet_class_index = self.global_jet_map[ptv].index(jet)
      if jet_class_index == 0: 
        print 'This relative error %s is not expected'%err
        exit()
      elif jet_class_index == 1: # this mean we should use inclusive ptv bin
        up_class_name = '%d'%ptv
      else: 
        up_jet_class = self.global_jet_map[ptv].index(jet_class_index-1)
        up_class_name = '%s_%s'%(ptv,up_jet_class)

    else: # it is inclusive ptv
      if err is 'Y': return err # Need to be checked
      else:
        if int(err) in self.global_ptv_bins:
          up_class_index = self.global_ptv_bins.index(int(err))-1
          up_class =  self.global_ptv_bins[up_class_index]
          up_class_name = str(up_class)
    if up_class_name == '0': up_class_name = 'Y'
    return up_class_name

  def show_inclusive_cross_section(self):
    # we take 1st qcd variation to calculate inclusive cross section
    var = self.qcd_vars[0]
    cross_section = var.get_inclusive_xs()
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error:
        print 'For inclusive region %5s, the cross section is %.3f'%(err,cross_section[err])
    for ptv in self.global_ptv_bins:
      for nj in self.global_jet_map[ptv]:
        err = str(ptv)+'_'+str(nj)
        if err in self.global_relative_error:
          print 'For inclusive region %5s, the cross section is %.3f'%(err,cross_section[err])

  def calculate_Deltas(self):
    # we take 1st qcd variation to calculate inclusive cross section
    if len(self.qcd_vars) < 1:
      print 'The variation is empty, please check, exit!'
      exit()
    var = self.qcd_vars[0]
    cross_section = var.get_inclusive_xs()
    for err in self.relative_err_order:
        print 'The Delta%s is %.3f'%(err,cross_section[err]*self.global_Remnant_error[err])
        self.global_Deltas[err] = cross_section[err]*self.global_Remnant_error[err]
      

  def calculate_x_Deltas(self):
    if len(self.qcd_vars) < 1:
      print 'The variation is empty, please check, exit!'
      exit()
    var = self.qcd_vars[0]
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv >= 0 else 'Y'
      above_ptv_xs = var.extract_float_from_bins_above_ptv('cross_section',ptv)
      if ptv > 0:
        above_ptv_xs_complementary = var.extract_float_from_bins_above_ptv('cross_section',0) - var.extract_float_from_bins_above_ptv('cross_section',ptv)
      else:
        above_ptv_xs_complementary = 0
      #print 'Above ptv %5s, the xs is %8.3f, the complementary xs is %8.3f'%(err, above_ptv_xs, above_ptv_xs_complementary)
      self.global_above_ptv_xs[err] = above_ptv_xs
      self.global_compl_ptv_xs[err] = above_ptv_xs_complementary

    for ptv in self.global_ptv_bins:
      for nj in self.global_jet_map[ptv]:
        err = (str(ptv) if ptv >= 0 else 'Y')+'_'+str(nj)
        above_njet_xs_in_ptv = var.extract_float_from_bins_above_njet_in_a_given_ptv_bin('cross_section',ptv,nj)
        if ptv >= 0:
          above_njet_xs_in_ptv_complementary = var.extract_float_from_bins_above_njet_in_a_given_ptv_bin('cross_section',ptv,0) - var.extract_float_from_bins_above_njet_in_a_given_ptv_bin('cross_section',ptv,nj)
        else:
          above_ptv_xs_complementary = 0
        #print 'Above bin %5s, the xs is %8.3f, the complementary xs is %8.3f'%(err, above_njet_xs_in_ptv, above_njet_xs_in_ptv_complementary)
        self.global_above_njet_xs_in_ptv[err] = above_njet_xs_in_ptv
        self.global_compl_njet_xs_in_ptv[err] = above_njet_xs_in_ptv_complementary

  def calculate_relative_for_each_Deltas(self):
    if len(self.qcd_vars) < 1:
      print 'The variation is empty, please check, exit!'
      exit()
    var = self.qcd_vars[0]
    for Delta in self.global_Deltas:
      print 'Calculation x for %s'%Delta
      doCalculationInclusiveNjet = '_' in Delta
      ptv_Delta = int( (Delta.split('_')[0]).replace('Y','-1') )
      if doCalculationInclusiveNjet : njet_for_Delta = int( Delta.split('_')[1] ) 
      x_Deltas = {}
      x_abs_err = {}
      x_relative_err = {}
      for vb in var.var_bins:
        name = vb.bin_name
        x_fraction = 0.0
        ptv_bin = vb.bin_info['ptv_low']
        xs_for_this_bin  = vb.bin_info['cross_section']
        if doCalculationInclusiveNjet and (ptv_Delta == ptv_bin):
          njet_for_this_bin = vb.bin_info['njet']
          err = (str(ptv_Delta) if ptv_Delta >= 0 else 'Y')+'_'+str(njet_for_Delta)
          if njet_for_this_bin >= njet_for_Delta:
            x_fraction = xs_for_this_bin / self.global_above_njet_xs_in_ptv[err]
            #print 'x fraction for this bin %25s is %.3f'%(name,x_fraction)
          else:
            x_fraction = xs_for_this_bin / self.global_compl_njet_xs_in_ptv[err] * (-1)
            #print 'x fraction for this bin %25s is %.3f'%(name,x_fraction)
        elif not doCalculationInclusiveNjet :
          if ptv_bin < 0 and ptv_Delta >= 0: x_fraction = 0.0
          else:
            err = (str(ptv_Delta) if ptv_Delta >= 0 else 'Y')
            if ptv_bin >= ptv_Delta:
              x_fraction = xs_for_this_bin / self.global_above_ptv_xs[err]
              #print 'x fraction for this bin %25s is %.3f'%(name,x_fraction)
            else:
              x_fraction = xs_for_this_bin / self.global_compl_ptv_xs[err] * (-1)
              #print 'x fraction for this bin %25s is %.3f'%(name,x_fraction)
        x_Deltas[name] = x_fraction
        x_abs_err[name] = x_fraction*self.global_Deltas[Delta]
        x_relative_err[name] = x_abs_err[name] / xs_for_this_bin 
      self.global_x_Deltas[Delta] = x_Deltas
      self.global_delta_abs_err_for_each_bin[Delta] = x_abs_err
      self.global_delta_relative_err_for_each_bin[Delta] = x_relative_err
      # +1 * self.global_Deltas[Delta] / self.global_above_ptv_xs[err]
      # -1 * self.global_Deltas[Delta] / self.global_compl_ptv_xs[err]

  def Collect_Delta_impact_for_ws(self):
    if len(self.qcd_vars) < 1:
      print 'The variation is empty, please check, exit!'
      exit()
    print 'Calculation ws inputs for %s'%self.sig_name
    STXS_name = GetSTXSName(self.sig_name)
    '''
      qqWlvH125M	:	QQ2HLNUM
      normSys("SysTheoryQCDscaleDelta250", -0.02, {"STXSQQWHPTV0x250"} );
    '''
    QCD_info = {}
    for Delta in self.global_Deltas:
      doCalculationInclusiveNjet = '_' in Delta
      deltaname_ws = Delta.replace('_','J')
      if doCalculationInclusiveNjet:
        above =  1 * self.global_Deltas[Delta] / self.global_above_njet_xs_in_ptv[Delta]
        below = -1 * self.global_Deltas[Delta] / self.global_compl_njet_xs_in_ptv[Delta]
        #print 'The impact on above: %.4f from Delta %s'%( above, Delta)
        #print 'The impact on below: %.4f from Delta %s'%( below, Delta)
        print 'normSys("SysTheoryQCDscaleDelta%s", %.4f, {"%s_Delta%s_Above"};'%(deltaname_ws,above,STXS_name,deltaname_ws)
        print 'normSys("SysTheoryQCDscaleDelta%s", %.4f, {"%s_Delta%s_Below"};'%(deltaname_ws,below,STXS_name,deltaname_ws)
        QCD_info['%s_Delta%s_Above'%(STXS_name,deltaname_ws)] = [ 'SysTheoryQCDscaleDelta%s'%deltaname_ws, above]
        QCD_info['%s_Delta%s_Below'%(STXS_name,deltaname_ws)] = [ 'SysTheoryQCDscaleDelta%s'%deltaname_ws, below]
      elif not doCalculationInclusiveNjet:
        above =  1 * self.global_Deltas[Delta] / self.global_above_ptv_xs[Delta]
        print 'normSys("SysTheoryQCDscaleDelta%s", %.4f, {"%s_Delta%s_Above"};'%(deltaname_ws,above,STXS_name,deltaname_ws)
        QCD_info['%s_Delta%s_Above'%(STXS_name,deltaname_ws)] = [ 'SysTheoryQCDscaleDelta%s'%deltaname_ws, above]
        #print 'The impact on above: %.4f from Delta %s'%( above, Delta)
        if Delta != 'Y':
          below = -1 * self.global_Deltas[Delta] / self.global_compl_ptv_xs[Delta]
          #print 'The impact on below: %.4f from Delta %s'%( below, Delta)
          print 'normSys("SysTheoryQCDscaleDelta%s", %.4f, {"%s_Delta%s_Below"});'%(deltaname_ws,below,STXS_name,deltaname_ws)
          QCD_info['%s_Delta%s_Below'%(STXS_name,deltaname_ws)] = [ 'SysTheoryQCDscaleDelta%s'%deltaname_ws, below]
    #return QCD_info
    return OrderedDict(sorted(QCD_info.items()))

  def Collect_Delta_group_for_ws(self):
    STXS_name = GetSTXSName(self.sig_name)
    var = self.qcd_vars[0]

    GroupInfo = {}
    # first show all bins
    print '{'
    for vb in var.var_bins:
      name = vb.bin_name
      print '"%s",'%name.replace('_','x'), # bcz they are x in inputs
    print
    print '}'
    print 'Show key words for QCD scale'
    for err in self.relative_err_order:
      deltaname_ws = err.replace('_','J')
      above = []
      below = []
      #print '%25s '%err
      for vb in var.var_bins:
        name = vb.bin_name
        value = self.global_delta_relative_err_for_each_bin[err][name]
        if   value > 0 :
          above.append(name)
          #print '  %s_Delta%s_Above -> %s'%(STXS_name,deltaname_ws,name)
        elif value < 0 :
          below.append(name)
          #print '  %s_Delta%s_Below -> %s'%(STXS_name,deltaname_ws,name)
      if len(above) >0:
        print '    {"%s_Delta%s_Above", {'%(STXS_name,deltaname_ws),
        GroupInfo['%s_Delta%s_Above'%(STXS_name,deltaname_ws)] = above
        for name in above:
          print '"%s",'%name,
        print '}},'
      if len(below) >0:
        print '    {"%s_Delta%s_Below", {'%(STXS_name,deltaname_ws),
        GroupInfo['%s_Delta%s_Below'%(STXS_name,deltaname_ws)] = below
        for name in below:
          print '"%s",'%name.replace('_','x'),
        print '}},'
    #return GroupInfo
    return OrderedDict(sorted(GroupInfo.items()))

  def show_relative_err_for_each_bin_from_delta(self,delta):
    var = self.qcd_vars[0]
    for vb in var.var_bins:
      name = vb.bin_name
      if self.global_delta_relative_err_for_each_bin[delta][name] != 0:
        print 'Relative error for bin %25s: %.3f'%(name,self.global_delta_relative_err_for_each_bin[delta][name])

  def show_relative_err_for_each_bin(self):
    for err in self.relative_err_order:
      print 'Show relative error for %8s'%(err)
      self.show_relative_err_for_each_bin_from_delta(err)

  def get_relative_err_for_each_bin(self):
    relative_errors_PTV = {}
    relative_errors_J   = {}
    for err in self.relative_err_order:
      if '_' in err: relative_errors_J[err] = self.get_relative_err_for_each_bin_from_delta(err)
      else : relative_errors_PTV[err] = self.get_relative_err_for_each_bin_from_delta(err)
    return relative_errors_PTV,relative_errors_J

  def get_relative_err_for_each_bin_from_delta(self,delta):
    var = self.qcd_vars[0]
    relative_errors = []
    for vb in var.var_bins:
      name = vb.bin_name
      #self.global_delta_relative_err_for_each_bin[delta][name]
      relative_errors.append(self.global_delta_relative_err_for_each_bin[delta][name])
    return relative_errors

  def PrintOutrelative_err_for_each_bin(self):
    var = self.qcd_vars[0]
    ''' 1st line '''
    print '%25s,'%('Bin'),
    for err in self.relative_err_order:
      print '%9s,'%err,
    print
    ''' 2nd line '''
    print '%25s,'%('Inclusive error'),
    for err in self.relative_err_order:
      print '%9.4f,'%self.global_relative_error[err],
    print
    ''' 3rd line '''
    print '%25s,'%('Remnant error'),
    for err in self.relative_err_order:
      print '%9.4f,'%self.global_Remnant_error[err],
    print
    ''' 4th line '''
    print '%25s,'%('Delta value'),
    for err in self.relative_err_order:
      print '%9.4f,'%self.global_Deltas[err],
    print
    for vb in var.var_bins:
      name = vb.bin_name
      print '%25s,'%name,
      for err in self.relative_err_order:
        value = self.global_delta_relative_err_for_each_bin[err][name]
        if value != 0 :
          print '%9.4f,'%value,
        else:
          print '%10s'%'-,',
      print '|'


  def calculate_remnant_relative_errors(self):
    err_list = []
    # no special reason, we make this list just want to show the err in order
    for ptv in self.global_ptv_bins:
      err = str(ptv) if ptv != 0 else 'Y'
      if err in self.global_relative_error: err_list.append(err)

    for ptv in self.global_ptv_bins:
      for nj in self.global_jet_map[ptv]:
        err = str(ptv)+'_'+str(nj)
        if err in self.global_relative_error: err_list.append(err)
    # list making finished
    self.relative_err_order = err_list

    for err in err_list:
      err_remnant = 0.
      err_exclusive = self.global_relative_error[err]
      up_class = self.find_up_class_relative_from_map(err)
      if err == 'Y':
        print 'Remnant relative error cal for Y as %.3f'%(err_exclusive)
        err_remnant = self.global_relative_error[err]
      else: 
        err_inclusive = self.global_relative_error[up_class]
        if UsePaoloApproach:
          ''' Paolo's proposal '''
          if abs(err_exclusive) > abs(err_inclusive):
            #print 'Remnant relative error cal with %.3f - %.3f'%(err_exclusive, err_inclusive)
            err_remnant = math.sqrt( err_exclusive*err_exclusive - err_inclusive*err_inclusive )
          else:
            err_remnant = 0.
          print 'Remnant relative error %5s has the related class err %5s, and the value is %.3f'%(err,up_class,err_remnant)
        else:
          #''' Frank's proposal '''
          #if abs(err_exclusive) > abs(err_inclusive):
          #  #print 'Remnant relative error cal with %.3f - %.3f'%(err_exclusive, err_inclusive)
          #  err_remnant = math.sqrt( err_exclusive*err_exclusive - err_inclusive*err_inclusive )
          #else:
          #  err_remnant = abs(err_exclusive) / 2.
          if '_' in err: err_remnant = abs(err_exclusive)
          elif '75' in err: err_remnant = abs(err_exclusive)
          else: err_remnant = abs(err_exclusive) * corrFact
      self.global_Remnant_error[err] = err_remnant
    
  def show_var(self,var_name_to_show):
    for q in self.qcd_vars:
      #if debug: print 'Now check if this variation is what we want to show %s'%q.get_var_name()
      if q.get_var_name() == var_name_to_show:
        print 'Show the var info for %s'%var_name_to_show
        q.show_bin_var()

def ParseJetInfo(name):
  jet_infi = name.startswith('GE')
  njet = int(name.replace('GE', '').replace('J',''))
  return njet,jet_infi

def ParseBinName(name):
  components = name.split('_')
  Production_mode = '' # can be QQ2HLNU etc.
  Bin_type  = '' # can be PTV or FWDH
  PTV_low   = -1
  PTV_high  = -1
  Njet      = -1
  PTV_infi  = False
  Njet_infi = False
  if debug: 
    print 'Input name is %s'%name
    for i,c in enumerate(components):
      print 'the %d of the name is %s'%(i,c)
  if len(components) < 2:
    print 'The length after cutting at _ of the name %s is less than 2, exit!'%name
    exit()
  Production_mode = components[0]
  Bin_type = components[1]
  if Bin_type == 'FWDH': # so this is the forward, we don't care so much
    pass
  elif Bin_type != 'PTV': # this bin we care about
    print 'we do not understand this bin type: %s'%Bin_type
    exit()
  else: # this is PTV we know the, can be X_X or GTX
    if len(components) < 3:
      print 'The structure of the name is strange, I can not parse it: %s, exit!'%name 
      exit()
    if components[2].startswith('GT'): # so it is GTX, so the low edge is X
      if len(components) == 3 or len(components) == 4:
        ''' components[2] after GT is low edge '''
        low_edge = components[2].replace('GT', '')
        PTV_low = int(low_edge)
        PTV_infi = True
        ''' TODO consider the PTV_high and Njet '''
        if len(components) == 4: # njet splitting 
          jet_info = components[3]
          Njet,Njet_infi = ParseJetInfo(jet_info)
      else:
        print 'The structure with GT is strange, I can not parse it: %s, exit!'%name 
        exit()
    else: # so it is X_X case
      if len(components) == 4 or len(components) == 5:
        PTV_low  = int(components[2])
        PTV_high = int(components[3])
        if len(components) == 5: # njet splitting
          jet_info = components[4]
          Njet,Njet_infi = ParseJetInfo(jet_info)
  return {'pro_mode':Production_mode, 'bin_type':Bin_type, 'ptv_low':PTV_low, 'ptv_high':PTV_high, 'njet':Njet, 'ptv_infi': PTV_infi, 'njet_infi':Njet_infi}

#print ParseBinName('QQ2HLNU_FWDH')
#print ParseBinName('QQ2HLNU_PTV_0_75')
#print ParseBinName('QQ2HLNU_PTV_75_150')
#print ParseBinName('QQ2HLNU_PTV_150_250_0J')
#print ParseBinName('QQ2HLNU_PTV_150_250_GE1J')
#print ParseBinName('QQ2HLNU_PTV_GT250_0J')
#print ParseBinName('QQ2HLNU_PTV_GT250_GE1J')

f = open('%s/Relative.Error.From.QCD.Variation.With.%s.txt'%(OutputName,'Paolo'if UsePaoloApproach else 'FrankWith%s'%(str(corrFact))),'w+')
temp = sys.stdout
QCD_impact = {}
Group = {}
qcd_raw = {}
for sig in signal_names:
  qcd = PrepareQCDScaleVariation(sig)
  print 'name is:',qcd.get_sig_name()
  #qcd.show_var('QCD1')
  qcd.perform_calculation_for_variation()
  sys.stdout = f
  qcd.PrintOutrelative_err_for_each_bin()
  sys.stdout = temp
  drawRelativeErrors( qcd, GetRangeForPlot(sig,'Relative')[0], GetRangeForPlot(sig,'Relative')[1] ) # Relative
  drawAbsoluteErrors( qcd, GetRangeForPlot(sig,'Absolute')[0] ) # Absolute 7.0
  drawRelativeErrors_inclusivePTV(qcd, GetRangeForPlot(sig,'RelativeIncPTV')[0])
  drawRelativeErrorFromEachDelta( qcd, GetRangeForPlot(sig,'PTV')[0], GetRangeForPlot(sig,'PTV')[1], max_njet=GetRangeForPlot(sig,'NJET')[0], min_njet=GetRangeForPlot(sig,'NJET')[1] ) # NJET PTV
  QCD_impact[sig] = qcd.Collect_Delta_impact_for_ws()
  Group[sig] = qcd.Collect_Delta_group_for_ws()
  qcd_raw[sig] = qcd
f.close()
#print QCD_impact
#print Group

# draw the merged RelativeErrors for inclusivePTV
for sig in ['qqWlvH125P', 'qqZllH125', 'ggZllH125']:
  couple = sig.replace('ll', 'vv').replace('P','M')
  WHscale = 'WlvH' in sig
  qcd=qcd_raw[sig]
  qcd_couple=qcd_raw[couple]
  drawRelativeErrors_inclusivePTV_merge(qcd, qcd_couple, GetRangeForPlot(sig,'RelativeIncPTV')[0], 0, WHscale)

print 'Next let us generate the ws inputs'
for sig in ['qqWlvH125P', 'qqZllH125', 'ggZllH125']:
  couple = sig.replace('ll', 'vv').replace('P','M')
  WHscale = 'WlvH' in sig
  STXS_name = GetSTXSName(sig)
  for item in QCD_impact[sig]:
    sysname = QCD_impact[sig][item][0]
    sys_val_in_this_sig = QCD_impact[sig][item][1]
    sys_val_from_couple = QCD_impact[couple][item][1]
    #if debug: print 'for %s, the sig has: %.4f, the couple has: %.4f'%(item,sys_val_in_this_sig,sys_val_from_couple)
    if WHscale: sys_val = (sys_val_in_this_sig*282.780 + sys_val_from_couple* 179.49) / (282.780+179.49)
    else: sys_val = (sys_val_in_this_sig + sys_val_from_couple) / 2.
    if abs(sys_val) < 0.005: continue
    if ShowAllSys:
      print 'normSys("%s_%s", %.4f, {"%s"};'%(sysname,STXS_name,sys_val,item)
    elif 'J' not in sysname:
      print 'normSys("%s_%s", %.4f, {"%s"};'%(sysname,STXS_name,sys_val,item)
  print

for sig in ['qqWlvH125P', 'qqZllH125', 'ggZllH125']:
  isWH = 'WlvH' in sig
  for item in Group[sig]:
    if not ShowAllSys and 'J' in item: continue
    sampleList = Group[sig][item]
    print '    {"%s", {'%(item),
    for name in sampleList:
      print '"%s",'%name.replace('_','x'),
    if not isWH:
      for name in sampleList:
        print '"%s",'%name.replace('_','x').replace('LL', 'NUNU'),
    print '}},'
  print


# create a demo to test stage1+
'''
ExmBins = [
  'QQ2HLNU_FWDH',
  'QQ2HLNU_PTV_0_75',
  'QQ2HLNU_PTV_75_150',
  'QQ2HLNU_PTV_150_250_0J',
  'QQ2HLNU_PTV_150_250_GE1J',
  'QQ2HLNU_PTV_GT250_0J',
  'QQ2HLNU_PTV_GT250_GE1J',
]

Exm = {}
Exm['qqWlvH125P'] = OrderedDict( [
  ('QQ2HLNU_FWDH',               [0.1408, 0.007]),
  ('QQ2HLNU_PTV_0_75',           [0.44,   0.015]),
  ('QQ2HLNU_PTV_75_150',         [0.2895, 0.020]),
  ('QQ2HLNU_PTV_150_250_0J',     [0.0527, 0.030]),
  ('QQ2HLNU_PTV_150_250_GE1J',   [0.0427, 0.040]),
  ('QQ2HLNU_PTV_GT250_0J',       [0.0155, 0.045]),
  ('QQ2HLNU_PTV_GT250_GE1J',     [0.0162, 0.050]),
])

qcd = PrepareQCDScaleVariationWithDemo(sig, Exm, ExmBins)
print 'name is:',qcd.get_sig_name()
qcd.perform_calculation_for_variation()
#qcd.show_bin_map_for_var('QCD1')
'''

def TestTheDemo():
  # create a demo to test stage1++
  ExmBins_2 = [
  'QQ2HLNU_FWDH',            
  'QQ2HLNU_PTV_0_75_0J',     
  'QQ2HLNU_PTV_0_75_1J',     
  'QQ2HLNU_PTV_0_75_GE2J',   
  'QQ2HLNU_PTV_75_150_0J',   
  'QQ2HLNU_PTV_75_150_1J',   
  'QQ2HLNU_PTV_75_150_GE2J', 
  'QQ2HLNU_PTV_150_250_0J',  
  'QQ2HLNU_PTV_150_250_1J',  
  'QQ2HLNU_PTV_150_250_GE2J',
  'QQ2HLNU_PTV_250_400_0J',  
  'QQ2HLNU_PTV_250_400_1J',  
  'QQ2HLNU_PTV_250_400_GE2J',
  'QQ2HLNU_PTV_GT400_0J',    
  'QQ2HLNU_PTV_GT400_1J',    
  'QQ2HLNU_PTV_GT400_GE2J',  
  ]
  
  Exm_2 = {}
  Exm_2['qqWlvH125P'] = OrderedDict( [
    ('QQ2HLNU_FWDH',               [0.14407665, 0.1]),
    ('QQ2HLNU_PTV_0_75_0J',        [0.311927,   0.0190361]),
    ('QQ2HLNU_PTV_0_75_1J',        [0.0953426,  0.0529412]),
    ('QQ2HLNU_PTV_0_75_GE2J',      [0.0417214,  0.102725]),
    ('QQ2HLNU_PTV_75_150_0J',      [0.179818,   0.0173054]),
    ('QQ2HLNU_PTV_75_150_1J',      [0.0710579,  0.043228]),
    ('QQ2HLNU_PTV_75_150_GE2J',    [0.0363498,  0.0921083]),
    ('QQ2HLNU_PTV_150_250_0J',     [0.0504287,  0.00771292]),
    ('QQ2HLNU_PTV_150_250_1J',     [0.0251189,  0.0300202]),
    ('QQ2HLNU_PTV_150_250_GE2J',   [0.0157595,  0.0788208]),
    ('QQ2HLNU_PTV_250_400_0J',     [0.0115115,  -0.000570466]),
    ('QQ2HLNU_PTV_250_400_1J',     [0.00668874, 0.0140808]),
    ('QQ2HLNU_PTV_250_400_GE2J',   [0.00498559, 0.0635219]),
    ('QQ2HLNU_PTV_GT400_0J',       [0.00234572, -0.0201996]),
    ('QQ2HLNU_PTV_GT400_1J',       [0.00155092, -0.00675864]),
    ('QQ2HLNU_PTV_GT400_GE2J',     [0.00131708, 0.041503]),
  ])
  
  qcd_2 = PrepareQCDScaleVariationWithDemo(sig, Exm_2, ExmBins_2)
  print 'name is:',qcd_2.get_sig_name()
  qcd_2.perform_calculation_for_variation()
  #qcd_2.PrintOutrelative_err_for_each_bin()
  drawRelativeErrors(qcd_2)

#GenerateAcceptance()
#GenerateYieldsDiffForQCD()
GenerateYields()
