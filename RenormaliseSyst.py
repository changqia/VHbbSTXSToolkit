import os
import ROOT
import collections
import group as group

ROOT.gStyle.SetPaintTextFormat("1.3f")
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

# define the group
debug = False
MergeScheme = '6POI'
DoSysType   = 0 # 0: PDF, 1: QCD
outputdir = 'Syst_Renormalisation_'+( 'PDF' if DoSysType == 0 else 'QCD' )

if not os.path.exists(outputdir):
    os.makedirs(outputdir)

# first we need the load the xs for each bin
def RetrieveXSFromFile():
  XS_file = ROOT.TFile('plots.ICHEP2018.mc16ad.v04/Initial_XS_mc16ad.root','read')
  XS_hist = XS_file.Get('Initial')
  nbins = XS_hist.GetXaxis().GetNbins()
  # TODO: may be have a check the # of bins?
  print 'Total number of bins %d'%nbins
  truthbins = []
  XSs = []
  for ib in xrange(nbins):
    truthbins.append(XS_hist.GetXaxis().GetBinLabel(ib+1))
    XSs.append(XS_hist.GetBinContent(ib+1))
  XS_info = collections.OrderedDict(zip(truthbins,XSs))
  return XS_info

def GetTheXSForTheGroup( ListOfBins, XSForAll ):
  xs = 0.
  for truthbin in ListOfBins:
    if debug : print 'adding xs for truth bin %s'%truthbin
    xs += XSForAll[truthbin.replace('x','_')]
  return xs
# load the syst for the each bin

def Convet2DHistToDictionary(Hist1):
  Variation = collections.OrderedDict()
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  VarNames = []
  BinNames = []
  for ix in xrange(nX1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    BinNames.append(xBinName)
    truthBinVar = collections.OrderedDict()
    for iy in xrange(nY1):
      yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
      if yBinName in ['Nominal','alphaSdn']: continue
      if ix == 0: VarNames.append(yBinName)
      content = Hist1.GetBinContent(ix+1,iy+1)
      truthBinVar[yBinName] = content
    Variation[xBinName] = truthBinVar
  return Variation,VarNames,BinNames

def GetTheSyst(filename, histlist):
  VariationAll = collections.OrderedDict()
  BinsAll = []
  #PDFFile = ROOT.TFile('stage1PPsys/PDFDiff2D.Value.EW.mc16ad.Nominal.root','read')
  #for histName in ['PDFDiff2DMerge_WH', 'PDFDiff2DMerge_qqZH', 'PDFDiff2DMerge_ggZH']:
  PDFFile = ROOT.TFile(filename,'read')
  for histName in histlist:
    hist2D = PDFFile.Get(histName)
    VarBin2D,Vars,Bins = Convet2DHistToDictionary(hist2D)
    VariationAll.update(VarBin2D)
    BinsAll = BinsAll + Bins
  return VariationAll,Vars,BinsAll

# merge the syst group
def MergeTheSystForTheGroup( ListOfBins, XSForAll, SystAll, var ):
  xs_group = GetTheXSForTheGroup( listOfBins, xs_info )
  syst_group = 0.
  for truthbin in ListOfBins:
    #print truthbin
    if debug : print 'adding xs for truth bin %s'%truthbin
    syst_group += XSForAll[truthbin.replace('x','_')]*SystAll[truthbin.replace('NUNU','LL').replace('x','_')][var]
  return syst_group/xs_group

def Make2DHist( xbins, ybins, xyMap, plotName ):
  #print xyMap
  print xbins
  print ybins
  nX1 = len(xbins)
  nY1 = len(ybins)
  hist2D = ROOT.TH2D(plotName,plotName+';truth bin or group;EV;', nX1, 0, nX1, nY1, 0, nY1)
  for ix in xrange(nX1):
    xBinName = xbins[ix]
    hist2D.GetXaxis().SetBinLabel(ix+1,xBinName)
    for iy in xrange(nY1):
      yBinName = ybins[iy]
      if ix == 0:
        hist2D.GetYaxis().SetBinLabel(iy+1,yBinName)
      content = xyMap[xBinName][yBinName]
      hist2D.SetBinContent(ix+1,iy+1,content*100.)
  return hist2D

# import the xs
xs_info = RetrieveXSFromFile()
print xs_info

# import the syst
if DoSysType == 0:
  syst_info,systs,bins_raw = GetTheSyst(
      'stage1PPsys/PDFDiff2D.Value.EW.mc16ad.Nominal.root',
     ['PDFDiff2DMerge_WH', 'PDFDiff2DMerge_qqZH', 'PDFDiff2DMerge_ggZH'])
elif DoSysType == 1:
  syst_info,systs,bins_raw = GetTheSyst(
      'stage1PPsys/QCDDiff.root',
     ['QCDDiff2DMerge_WH', 'QCDDiff2DMerge_qqZH', 'QCDDiff2DMerge_ggZH'])
print systs

def GetMerScheme(MergeScheme):
  if MergeScheme == '6POI':
    group.GroupScheme.clear() # clear the scheme in group
    # define the group scheme
    if DoSysType ==0: group.GroupScheme['FWDH']                  = {'bin_type': 'FWDH'}  #  we dont do QCD scale for FWDH
    group.GroupScheme['WHx0x150PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['WHx150x250PTV']         = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['WHxGT250PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
    group.GroupScheme['ZHx0x150PTV']           = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['ZHx150x250PTV']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['ZHxGT250PTV']           = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':250}
    return group.main(group.GroupScheme)
  elif MergeScheme == '9POI':
    group.GroupScheme.clear() # clear the scheme in group
    # define the group scheme
    if DoSysType ==0: group.GroupScheme['FWDH']                  = {'bin_type': 'FWDH'} #  we dont do QCD scale for FWDH
    group.GroupScheme['WHx0x150PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['WHx150x250PTV']         = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['WHxGT250PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
    group.GroupScheme['qqZHx0x150PTV']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['qqZHx150x250PTV']       = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['qqZHxGT250PTV']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':250}
    group.GroupScheme['ggZHx0x150PTV']         = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['ggZHx150x250PTV']       = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['ggZHxGT250PTV']         = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':250}
    return group.main(group.GroupScheme)
  elif MergeScheme == '3POI':
    group.GroupScheme.clear() # clear the scheme in group
    # define the group scheme
    if DoSysType ==0: group.GroupScheme['FWDH']                  = {'bin_type': 'FWDH'} #  we dont do QCD scale for FWDH
    group.GroupScheme['VHx0x150PTV']           = {'ptv_low':0, 'ptv_high':150}
    group.GroupScheme['VHx150x250PTV']         = {'ptv_low':150, 'ptv_high':250}
    group.GroupScheme['VHxGT250PTV']           = {'ptv_low':250}
    return group.main(group.GroupScheme)

GroupForRenormalization = GetMerScheme(MergeScheme)
output_file = ROOT.TFile('%s/%s.%s.root'%(outputdir,( 'PDF' if DoSysType == 0 else 'QCD' ),MergeScheme),'recreate')

if debug: print GroupForRenormalization
# Raw value
hist2D_raw = Make2DHist( bins_raw, systs, syst_info, 'Raw' )
c1 = ROOT.TCanvas('Raw','Raw', 2400, 1200)
hist2D_raw.Draw('colzTEXT')
hist2D_raw.Write()
hist2D_raw.SetMarkerSize(0.5)
hist2D_raw.GetXaxis().SetLabelSize(0.02)
c1.Print('%s/raw.pdf'%outputdir)

# inclusive effect
nX1 = len(GroupForRenormalization.keys())
nY1 = len(systs)
InclusiveEff = collections.OrderedDict()
for ix in xrange(nX1):
  xBinName = GroupForRenormalization.keys()[ix]
  listOfBins = GroupForRenormalization[xBinName]
  systVal = collections.OrderedDict()
  for iy in xrange(nY1):
    yBinName = systs[iy]
    if ix == 0: print yBinName
    systVal[yBinName] = MergeTheSystForTheGroup( listOfBins, xs_info, syst_info, yBinName)
  InclusiveEff[xBinName] = systVal
# plot merge plot
hist2D_group_inclusive = Make2DHist(GroupForRenormalization.keys(),systs,InclusiveEff, 'Group_Inclusive_'+MergeScheme)
c1 = ROOT.TCanvas('GroupInclusive','GroupInclusive', 1100, 600)
hist2D_group_inclusive.Draw('colzTEXT')
hist2D_group_inclusive.Write()
c1.Print('%s/Group_%s_Inclusive.pdf'%(outputdir,MergeScheme))

def FindGroupForBin(Bin,group):
  BinName = Bin.replace('_','x')
  for TruthBinList in group:
    if BinName in group[TruthBinList]: return TruthBinList
  print "Can't find bin %s for any group, existing!"%BinName
  exit()

# residual effect
nX1 = len(bins_raw)
nY1 = len(systs)
Individual_Inclusive = collections.OrderedDict()
Individual_Residual  = collections.OrderedDict()
for ix in xrange(nX1):
  xBinName = bins_raw[ix]
  residualVal = collections.OrderedDict()
  inclusivVal = collections.OrderedDict()
  GroupBelongTo = FindGroupForBin(xBinName,GroupForRenormalization)
  for iy in xrange(nY1):
    yBinName = systs[iy]
    if ix == 0: print yBinName
    inclusivVal[yBinName] = InclusiveEff[GroupBelongTo][yBinName]
    residualVal[yBinName] = syst_info[xBinName][yBinName] - InclusiveEff[GroupBelongTo][yBinName]
  Individual_Inclusive[xBinName] = inclusivVal
  Individual_Residual[xBinName]  = residualVal

hist2D_individual_inclusive = Make2DHist(bins_raw, systs, Individual_Inclusive, 'Individual_Inclusive_'+MergeScheme)
c1 = ROOT.TCanvas('Individual_Inclusive','Individual_Inclusive', 2400, 1200)
hist2D_individual_inclusive.Draw('colzTEXT')
hist2D_individual_inclusive.Write()
hist2D_individual_inclusive.SetMarkerSize(0.5)
hist2D_individual_inclusive.GetXaxis().SetLabelSize(0.02)
c1.Print('%s/Individual_%s_Inclusive.pdf'%(outputdir,MergeScheme))

hist2D_residual_inclusive = Make2DHist(bins_raw, systs, Individual_Residual, 'Individual_Residual_'+MergeScheme)
c1 = ROOT.TCanvas('Individual_Residual','Individual_Residual', 2400, 1200)
hist2D_residual_inclusive.Draw('colzTEXT')
hist2D_residual_inclusive.Write()
hist2D_residual_inclusive.SetMarkerSize(0.5)
hist2D_residual_inclusive.GetXaxis().SetLabelSize(0.02)
c1.Print('%s/Individual_%s_Residual.pdf'%(outputdir,MergeScheme))

