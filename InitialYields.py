#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import numpy as np
import Plot as plot
import math
import colorsys
import random
import sys
import os
#from collections import OrderedDict
import collections
ROOT.gStyle.SetPaintTextFormat("1.3f")
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

import socket
LocalRun = True
hostname =  socket.gethostname()
if hostname.startswith('lxplus'):
  LocalRun = False
  print 'Run on lxplus'
  print hostname
else:
  print 'You run locally, you need define the localtion of the inputs'
  print hostname

debug = True
doNorm = False # normalized back to inclusive
WithOutEWCorrection = False # default False, we applied EW correction
ApplyThreshold = False

#SplitScheme = 'Fine'
SplitScheme = 'Nicest'
ShowAllSys = True # show all, or only PTV
MCType = 'mc16ad'

#       name of signal| name filter| br | XS | sumOfWeights_mc16a | sumOfWeights_mc16d
signal_names = collections.OrderedDict( [
        ('qqWlvH125M',  ['QQ2HLNU', 0.582, 0.17949, 350906.073237,   350871.2965, ]),
        ('qqWlvH125P',  ['QQ2HLNU', 0.582, 0.28278, 1095288.431198,  1112980.506, ]),
        ('qqZllH125',   ['QQ2HLL',  0.582, 0.07704, 228235.974688,   228273.3358, ]),
        ('qqZvvH125',   ['QQ2HNUNU',0.582, 0.15305, 300453.345575,   300335.4856, ]),
        ('ggZllH125',   ['GG2HLL',  0.582, 0.01242, 4340.116968,     15079.88772, ]),
        ('ggZvvH125',   ['GG2HNUNU',0.582, 0.02457, 5693.437834,     5685.469866, ]),
]
)

if LocalRun:
  InitialYieldsFile = '/Users/changqiao/Workspace/STXS/DxAOD_output_stage16/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'
else:
  InitialYieldsFile = '/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'

rootFile = ROOT.TFile(InitialYieldsFile)

OutputName = 'QCDScaleVariationCalculation.'+(SplitScheme)+('.NOEW'if WithOutEWCorrection else '')+'.v16'
if not os.path.exists(OutputName):
  os.makedirs(OutputName)

weight_var   = [
  'Weight0',
  'Nominal', # nominal w/ EW correction
  'PDF1',
  'PDF2',
  'PDF3',
  'PDF4',
  'PDF5',
  'PDF6',
  'PDF7',
  'PDF8',
  'PDF9',
  'PDF10',
  'PDF11',
  'PDF12',
  'PDF13',
  'PDF14',
  'PDF15',
  'PDF16',
  'PDF17',
  'PDF18',
  'PDF19',
  'PDF20',
  'PDF21',
  'PDF22',
  'PDF23',
  'PDF24',
  'PDF25',
  'PDF26',
  'PDF27',
  'PDF28',
  'PDF29',
  'PDF30',
  'alphaSup',
  'alphaSdn',
  'QCD1',
  'QCD2',
  'QCD3',
  'QCD4',
  'QCD5',
  'QCD6',
  'QCD7',
  'QCD8',
]

''' histograms toolkit '''

def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

def TransName(var):
  if   var is 'alphaSup': return 'aSup'
  elif var is 'alphaSdn': return 'aSdn'
  elif var is 'PDF1':     return 'EV1'
  elif var is 'PDF2':     return 'EV2'
  elif var is 'PDF3':     return 'EV3'
  elif var is 'PDF4':     return 'EV4'
  elif var is 'PDF5':     return 'EV5'
  elif var is 'PDF6':     return 'EV6'
  elif var is 'PDF7':     return 'EV7'
  elif var is 'PDF8':     return 'EV8'
  elif var is 'PDF9':     return 'EV9'
  elif var is 'PDF10':    return 'EV10'
  elif var is 'PDF11':    return 'EV11'
  elif var is 'PDF12':    return 'EV12'
  elif var is 'PDF13':    return 'EV13'
  elif var is 'PDF14':    return 'EV14'
  elif var is 'PDF15':    return 'EV15'
  elif var is 'PDF16':    return 'EV16'
  elif var is 'PDF17':    return 'EV17'
  elif var is 'PDF18':    return 'EV18'
  elif var is 'PDF19':    return 'EV19'
  elif var is 'PDF20':    return 'EV20'
  elif var is 'PDF21':    return 'EV21'
  elif var is 'PDF22':    return 'EV22'
  elif var is 'PDF23':    return 'EV23'
  elif var is 'PDF24':    return 'EV24'
  elif var is 'PDF25':    return 'EV25'
  elif var is 'PDF26':    return 'EV26'
  elif var is 'PDF27':    return 'EV27'
  elif var is 'PDF28':    return 'EV28'
  elif var is 'PDF29':    return 'EV29'
  elif var is 'PDF30':    return 'EV30'
  else : return var


def MergeYields(yields_org, bins_org, MergeDict):
  YieldsMerged = {}
  Var = {}
  for var in weight_var:
    Bins = {}
    for bin_name_meg in MergeDict.keys():
      yields = 0.
      entry = 0
      for bin_name_org in MergeDict[bin_name_meg]:
        for sig in signal_names:
          yields += yields_org[sig][var][bin_name_org]['Value']
          entry += yields_org[sig][var][bin_name_org]['Entry']
      Bins[bin_name_meg] = {'Value':yields, 'Entry':entry}
    Var[var]=Bins
  YieldsMerged['Merged']=Var
  return YieldsMerged,MergeDict.keys()

def FillYields(signals):
  OrderBins = []
  Yields = {}
  # Fill the list
  for sig in signals:
    # name filter | br | XS | sumOfWeights_mc16a | sumOfWeights_mc16d
    br = signals[sig][1]
    xs = signals[sig][2]
    if MCType == 'mc16a':
      lumi = 36.07456
      sumOfWeights = signals[sig][3]
    else :
      lumi = 43.8
      sumOfWeights = signals[sig][4]
    normFact = lumi * xs * 1000. * br / sumOfWeights
    Var = {}
    for var in weight_var:
      if debug: print 'collecting: ',var.replace('_','')
      # find the initial binned histograms
      # ggZllH125_wEW_Stage1
      if WithOutEWCorrection:
        histoname = sig+'_'+var+'NOEW_Stage1'+SplitScheme
      else:
        histoname = sig+'_'+var+'_Stage1'+SplitScheme
      if debug: print 'histname: %s'%histoname
      histo_initial = rootFile.Get(histoname)
      histo_initial_entries = rootFile.Get(histoname+'_NoWeight')
      if debug:
        print 'histname: %20s with Entries %i'%(histoname, histo_initial.GetEntries())
        print 'histname: %20s with Entries %i'%(histoname+'_NoWeight', histo_initial_entries.GetEntries())
        print 'histname: %20s with Yields  %f'%(histoname, histo_initial.Integral())
      if not histo_initial.GetEntries() == histo_initial_entries.GetEntries():
        print 'The entries is not equal, please check'
        exit()
      x_axis=histo_initial.GetXaxis()
      Nbins=x_axis.GetNbins()
      Bins = {}
      FillOrderBins = False
      if len(OrderBins) == 0 : FillOrderBins = True
      for count in range(0, Nbins):
        name = x_axis.GetBinLabel(count+1)
        if FillOrderBins: OrderBins.append(name)
        yields = histo_initial.GetBinContent(count+1)
        error  = histo_initial.GetBinError(count+1)
        entry  = histo_initial_entries.GetBinContent(count+1)
        if debug: print 'No. %2d: %25s: %10.2f'%(count+1,name,yields)
        if doNorm:
          if debug: print 'normFact = %f'%normFact
          Bins[name]={'Value':yields*normFact,'Error':error*normFact,'Entry':entry}
        else:
          Bins[name]={'Value':yields,'Error':error,'Entry':entry}
      Var[var]=Bins
    Yields[sig]=Var
  return Yields,OrderBins

def GenerateYields(content, yields, orderbins, signals, postfix = 'normal'):
  weight_show   = [
    'Weight0', 
    'Nominal',
#    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
#    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
#    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
#    'alphaSup', 'alphaSdn',
#    'QCD1', 
#    'QCD2', 
#    'QCD3', 
#    'QCD4', 
#    'QCD5', 
#    'QCD6', 
#    'QCD7', 
#    'QCD8',
  ]
  f = open('%s/%s.%s.%s.%s.%s.txt'%(OutputName,content,('doNorm' if doNorm else 'NoNorm'),('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix),'w+')
  temp = sys.stdout
  sys.stdout = f
  print '%26s'%'Bins,',
  for var in weight_show:
    print '%9s,'%TransName(var), 
  print
  for sig in signals:
    for Bin in orderbins:
      '''check name'''
      checkname = signals[sig][0]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      print '%25s,'%Bin,
      for var in weight_show:
        value = yields[sig][var][Bin][content]
        if value != 0 :
          if content == 'Entry':
            print '%9d,'%value,
          else:
            print '%9.1f,'%value,
        else:
          print '%10s'%'-,',
      print '|'
  f.close()

def GenerateYields_PDF(content, yields, orderbins, signals, postfix = 'pdf'):
  weight_show   = [
    'Nominal',
    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
    'alphaSup', 'alphaSdn',
  ]
  if doNorm:
    print 'Should not use nom for PDF'
    exit()
  f = open('%s/PDF.%s.%s.%s.%s.txt'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix),'w+')
  temp = sys.stdout
  sys.stdout = f
  print '%26s'%'Bins,',
  for var in weight_show:
    print '%9s,'%TransName(var), 
  print
  for sig in signals:
    for Bin in orderbins:
      '''check name'''
      checkname = signals[sig][0]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      print '%25s,'%Bin,
      for var in weight_show:
        value = yields[sig][var][Bin][content]
        if value != 0 :
          if content == 'Entry':
            print '%9d,'%value,
          else:
            print '%9.1f,'%value,
        else:
          print '%10s'%'-,',
      print '|'
  f.close()

def GenerateYields_PDF_Diff(content, yields, orderbins, signals, postfix = 'pdf'):
  weight_show   = [
    'Nominal',
#    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
#    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
#    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
    'alphaSup', 'alphaSdn',
  ]
  if doNorm:
    print 'Should not use nom for PDF'
    exit()
  f = open('%s/PDFDiff.%s.%s.%s.%s.txt'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix),'w+')
  temp = sys.stdout
  sys.stdout = f
  print '%26s'%'Bins,',
  for var in weight_show:
#    if var in  ['alphaSdn', 'Nominal' ]: continue
    if var in  ['Nominal' ]: continue
    print '%9s,'%TransName(var), 
  print
  for sig in signals:
    for Bin in orderbins:
      '''check name'''
      checkname = signals[sig][0]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      print '%25s,'%Bin,
      for var in weight_show:
        if var in  ['Nominal' ]: continue
        #if var in  ['alphaSdn', 'Nominal' ]: continue
        else:
          value = yields[sig][var][Bin][content]
          value_nom = yields[sig]['Nominal'][Bin][content]
          #if  var == 'alphaSup':
          #  value_dn = yields[sig]['alphaSdn'][Bin][content]
          #  diff = ( math.fabs(value/value_nom - 1.) +  math.fabs(value_dn/value_nom - 1.) ) / 2.
          #else:
          diff = (value - value_nom ) / value_nom
          if diff == 0 :
            print '%10s'%'-,',
          elif ApplyThreshold and (math.fabs(diff) < 0.005):
            print '%10s'%'-,',
          elif content == 'Entry':
            print '%9d,'%diff,
          else:
            print '%9.6f,'%diff,
      print '|'
  f.close()

def DrawYields_PDF_Diff_Raw(content, yields, orderbins, signals, postfix = 'pdf'):
  can_width = 1200
  can_height = 600
  # fill the histos
  weight_show   = [
    'Nominal',
    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
    'alphaSup', 'alphaSdn',
  ]
  if doNorm:
    print 'Should not use nom for PDF'
    exit()
  nbinsy=len(weight_show)
  hist2D={}
  nbinsx_all=len(orderbins)
  for sig in signals:
    Binx_sig = []
    c1 = ROOT.TCanvas('PDFDiff'+sig,'PDFDiff'+sig,can_width,can_height)
    for x_bin in xrange(nbinsx_all):
      Bin = orderbins[x_bin]
      '''check name'''
      checkname = signals[sig][0]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      Binx_sig.append(Bin)
    nbinsx_sig = len(Binx_sig)
    # create 
    hist2D[sig] = ROOT.TH2D('PDFDiff2D_'+sig, 'PDFDiff_'+sig, nbinsx_sig, 0, nbinsx_sig, nbinsy, 0, nbinsy,)
    # set label
    for ix_bin_sig in xrange(nbinsx_sig):
      xBinSig_name = Binx_sig[ix_bin_sig]
      hist2D[sig].GetXaxis().SetBinLabel(ix_bin_sig+1,xBinSig_name)
    for var in weight_show:
      pos_y = weight_show.index(var)
      hist2D[sig].GetYaxis().SetBinLabel(pos_y+1,var)
      if var in  [ 'Nominal', 'alphaSdn' ]: continue
      # build histo
      for x_bin in xrange(nbinsx_all):
        Bin = orderbins[x_bin]
        '''check name'''
        checkname = signals[sig][0]
        if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
        value = yields[sig][var][Bin][content]
        value_nom = yields[sig]['Nominal'][Bin][content]
        if  var == 'alphaSup':
          value_dn = yields[sig]['alphaSdn'][Bin][content]
          diff = ( math.fabs(value/value_nom - 1.) +  math.fabs(value_dn/value_nom - 1.) ) / 2.
        else:
          diff = (value - value_nom ) / value_nom
        #if math.fabs(diff)>0.005:
        #  x_bin_sig = Binx_sig.index(Bin)
        #  hist2D[sig].SetBinContent( x_bin_sig+1, pos_y+1, diff )
        x_bin_sig = Binx_sig.index(Bin)
        hist2D[sig].SetBinContent( x_bin_sig+1, pos_y+1, diff )
    hist2D[sig].Draw('TEXT')
    outputplot = '%s/PDFDiff2D.%s.%s.%s.%s'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix)
    #c1.Print('%s/PDFDiff.%s.%s.%s.%s.root'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix))
    c1.Print(outputplot+'.%s.png'%sig)
  outputfile = ROOT.TFile(outputplot+'.root','recreate')
  Merge2DHist( hist2D['qqWlvH125M'], hist2D['qqWlvH125P'], signals['qqWlvH125M'][2], signals['qqWlvH125P'][2], 'WH',   outputplot ).Write()
  Merge2DHist( hist2D['qqZllH125'],  hist2D['qqZvvH125'],  signals['qqZllH125'][2],  signals['qqZvvH125'][2],  'qqZH', outputplot ).Write()
  Merge2DHist( hist2D['ggZllH125'],  hist2D['ggZvvH125'],  signals['ggZllH125'][2],  signals['ggZvvH125'][2],  'ggZH', outputplot ).Write()
  outputfile.Close()
    

def Merge2DHist(Hist1,Hist2,xs1,xs2,histName,outputplot):
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  nX2 = Hist2.GetXaxis().GetNbins()
  nY2 = Hist2.GetYaxis().GetNbins()
  if nX1 != nX2:
    print 'The numbers of x axis are not equal! Existing'
    exit()
  if nY1 != nY2:
    print 'The numbers of y axis are not equal! Existing'
    exit()
  hist2D = ROOT.TH2D('PDFDiff2DMerge_'+histName, 'PDFDiff2DMerge_'+histName+';truth bin;PDF EV;', nX1, 0, nX1, nY1, 0, nY1)
  for ix in xrange(nX1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    if ix == 0:
      hist2D.GetXaxis().SetBinLabel(nX1,xBinName)
    else: 
      hist2D.GetXaxis().SetBinLabel(ix,xBinName)
    for iy in xrange(nY1):
      if ix == 0:
        yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
        hist2D.GetYaxis().SetBinLabel(iy+1,yBinName)
      content1 = Hist1.GetBinContent(ix+1,iy+1)
      content2 = Hist2.GetBinContent(ix+1,iy+1)
      contentMerge = ( content1*xs1 + content2*xs2 )/( xs1 + xs2 )
      #hist2D.SetBinContent(ix+1,iy+1,contentMerge)
      #if math.fabs(contentMerge)>0.005:
      if ix == 0:
        hist2D.SetBinContent( nX1, iy+1, contentMerge )
      else:
        hist2D.SetBinContent( ix, iy+1, contentMerge )
  can_width = 1600
  can_height = 800
  RightMargin = 0.2
  BotMargin = 0.2
  c1 = ROOT.TCanvas('PDFDiff'+histName,'PDFDiff'+histName,can_width,can_height)
  hist2D.Draw('TEXT')
  c1.SetRightMargin(RightMargin)
  c1.SetBottomMargin(BotMargin)
  c1.Print(outputplot+'.%s.png'%histName)
  return hist2D

def DrawYields_PDF_Diff(content, yields, orderbins, signals, postfix = 'pdf'):
  can_width = 1200
  can_height = 600
  c1 = ROOT.TCanvas('PDFDiff','PDFDiff',can_width,can_height)
  # fill the histos
  weight_show   = [
    'Nominal',
    'PDF1',  'PDF2',  'PDF3',  'PDF4',  'PDF5',  'PDF6',  'PDF7',  'PDF8',  'PDF9',  'PDF10', 
    'PDF11', 'PDF12', 'PDF13', 'PDF14', 'PDF15', 'PDF16', 'PDF17', 'PDF18', 'PDF19', 'PDF20', 
    'PDF21', 'PDF22', 'PDF23', 'PDF24', 'PDF25', 'PDF26', 'PDF27', 'PDF28', 'PDF29', 'PDF30', 
    'alphaSup', 'alphaSdn',
  ]
  if doNorm:
    print 'Should not use nom for PDF'
    exit()
  if len(signals) != 1:
    print 'To draw the diff, all signal should be already merged!'
    exit()
  sig = 'Merged'
  Hists = []
  nbinsy=len(weight_show)
  nbinsx=len(orderbins)
  hist2D = ROOT.TH2D('PDFDiff2D_'+sig, 'PDFDiff_'+sig, nbinsx, 0, nbinsx, nbinsy, 0, nbinsy,)
  for var in weight_show:
    pos_y = weight_show.index(var)
    hist2D.GetYaxis().SetBinLabel(pos_y+1,var)
    if var in  [ 'Nominal', 'alphaSdn' ]: continue
    # build histo
    hist = ROOT.TH1D('PDFDiff_'+sig+'_'+var, 'PDFDiff_'+sig+'_'+var,nbinsx, 0, nbinsx)
    for x_bin in xrange(nbinsx):
      Bin = orderbins[x_bin]
      hist.GetXaxis().SetBinLabel(x_bin+1,Bin)
      '''check name'''
      checkname = signals[sig][0]
      if not (Bin.startswith(checkname) or Bin == 'UNKNOWN') : continue
      value = yields[sig][var][Bin][content]
      value_nom = yields[sig]['Nominal'][Bin][content]
      if  var == 'alphaSup':
        value_dn = yields[sig]['alphaSdn'][Bin][content]
        diff = ( math.fabs(value/value_nom - 1.) +  math.fabs(value_dn/value_nom - 1.) ) / 2.
      else:
        diff = (value - value_nom ) / value_nom
      hist.SetBinContent(x_bin+1,diff)
      if math.fabs(diff)>0.005: hist2D.SetBinContent(x_bin+1,pos_y+1,diff+1)
    Hists.append(hist) 
  #Hists[0].SetMaximum(0.1)
  #Hists[0].SetMinimum(-0.1)
  '''
  hist_maxi = ROOT.TH1D('PDFDiff_'+sig+'_maxi', 'PDFDiff_'+sig+'_maxi; truth bin; diff. w.r.t PDF0;',nbinsx, 0, nbinsx)
  hist_mini = ROOT.TH1D('PDFDiff_'+sig+'_mini', 'PDFDiff_'+sig+'_mini',nbinsx, 0, nbinsx)
  for x_bin in xrange(nbinsx):
    minimum = 100
    maximum = -100
    for h in Hists:
      C = h.GetBinContent(x_bin+1)
      if C < minimum: minimum = C
      if C > maximum: maximum = C
    hist_maxi.SetBinContent(x_bin+1,maximum)
    hist_mini.SetBinContent(x_bin+1,minimum)
    hist_maxi.GetXaxis().SetBinLabel(x_bin+1,orderbins[x_bin])

  minimum_overall = hist_mini.GetMinimum()
  maximum_overall = hist_maxi.GetMaximum()
  hist_maxi.GetYaxis().SetRangeUser(1.2*minimum_overall,1.2*maximum_overall)
  ROOT.gStyle.SetPaintTextFormat('+0.3f')
  hist_maxi.Draw("TEXT0")
  hist_mini.Draw("TEXT0same")
  for h in Hists:
    h.Draw('same')
  '''
  # draw 2D
  for xbin in xrange(nbinsx):
    hist2D.GetXaxis().SetBinLabel(xbin+1,orderbins[xbin])
  hist2D.Draw('colzTEXT')
  #c1.Print('%s/PDFDiff.%s.%s.%s.%s.root'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix))
  c1.Print('%s/PDFDiff.%s.%s.%s.%s.png'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix))
  #c1.Print('%s/PDFDiff.%s.%s.%s.%s.pdf'%(OutputName,content,('NOEW'if WithOutEWCorrection else 'EW'),MCType,postfix))

def DefMergeScheme_22():
  MergeDict = collections.OrderedDict()
  MergeDict['qqZH_FWDH']           = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH' ] # 2
  MergeDict['qqZH_0_150PTV_0J'] = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_75_150_0J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_75_150_0J", ]
  MergeDict['qqZH_0_150PTV_GE1J'] = [ "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['qqZH_150_250PTV_0J'] = [ "QQ2HLL_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_0J", ]
  MergeDict['qqZH_150_250PTV_GE1J'] = [ "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['qqZH_GT250PTV_0J'] = [ "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_GT400_0J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_GT400_0J", ]
  MergeDict['qqZH_GT250PTV_GE1J'] = [ "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['qqWH_FWDH']           = [ 'QQ2HLNU_FWDH' ] # 1
  MergeDict['qqWH_0_150PTV_0J'] = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_75_150_0J", ]
  MergeDict['qqWH_0_150PTV_GE1J'] = [ "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['qqWH_150_250PTV_0J'] = [ "QQ2HLNU_PTV_150_250_0J", ]
  MergeDict['qqWH_150_250PTV_GE1J'] = [ "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", ]
  MergeDict['qqWH_GT250PTV_0J'] = [ "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_GT400_0J", ]
  MergeDict['qqWH_GT250PTV_GE1J'] = [ "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ggZH_FWDH']           = [ 'GG2HNUNU_FWDH', 'GG2HLL_FWDH' ] # 2
  MergeDict['ggZH_0_150PTV_0J'] = [ "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_75_150_0J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_75_150_0J", ]
  MergeDict['ggZH_0_150PTV_GE1J'] = [ "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ggZH_150_250PTV_0J'] = [ "GG2HLL_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_0J", ]
  MergeDict['ggZH_150_250PTV_GE1J'] = [ "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['ggZH_GT250PTV_0J'] = [ "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_GT400_0J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_GT400_0J", ]
  MergeDict['ggZH_GT250PTV_GE1J'] = [ "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]

  #MergeDict['UNKNOWN']        = [ 'UNKNOWN',]
  MergeDict['TOTAL']          = [ 'TOTAL',]
  return MergeDict

def DefMergeScheme_14():
  MergeDict = collections.OrderedDict()
  MergeDict['GGZHFWD'] = [ "GG2HLL_FWDH", "GG2HNUNU_FWDH", ]
  MergeDict['GGZH0_150PTV'] = [ "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_0J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_0J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['GGZHGT150PTV0J'] = [ "GG2HLL_PTV_150_250_0J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_GT400_0J", "GG2HNUNU_PTV_150_250_0J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_GT400_0J", ]
  MergeDict['GGZHGT150PTVGE1J'] = [ "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['QQWHFWD'] = [ "QQ2HLNU_FWDH", ]
  MergeDict['QQWH0_150PTV'] = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_0J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['QQWH150_250PTV0J'] = [ "QQ2HLNU_PTV_150_250_0J", ]
  MergeDict['QQWH150_250PTVGE1J'] = [ "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", ]
  MergeDict['QQWHGT250PTV'] = [ "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_0J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['QQZHFWD'] = [ "QQ2HLL_FWDH", "QQ2HNUNU_FWDH", ]
  MergeDict['QQZH0_150PTV'] = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_0J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['QQZH150_250PTV0J'] = [ "QQ2HLL_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_0J", ]
  MergeDict['QQZH150_250PTVGE1J'] = [ "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['QQZHGT250PTV'] = [ "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_0J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_0J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['TOTAL']          = [ 'TOTAL',]
  return MergeDict

def main():
  yields,bins = FillYields(signal_names)
  GenerateYields('Entry',yields,bins,signal_names)
  GenerateYields('Value',yields,bins,signal_names)
  GenerateYields_PDF_Diff('Value', yields, bins, signal_names, 'Nominal')
  DrawYields_PDF_Diff_Raw('Value', yields, bins, signal_names, 'Nominal')

  ## let's do merging
  #yieldsMerged, binsMerged = MergeYields(yields, bins, DefMergeScheme_22() )
  #signal_merge = collections.OrderedDict( [('Merged',  ['']), ] )
  #GenerateYields('Entry', yieldsMerged, binsMerged, signal_merge, 'Merged22')
  #GenerateYields('Value', yieldsMerged, binsMerged, signal_merge, 'Merged22')

  ## let's do merging
  #yieldsMerged, binsMerged = MergeYields(yields, bins, DefMergeScheme_14() )
  #signal_merge = collections.OrderedDict( [('Merged',  ['']), ] )
  #GenerateYields_PDF('Value', yieldsMerged, binsMerged, signal_merge, 'Merged14')
  #GenerateYields_PDF_Diff('Value', yieldsMerged, binsMerged, signal_merge, 'Merged14')
  #DrawYields_PDF_Diff('Value', yieldsMerged, binsMerged, signal_merge, 'Merged14')

if __name__ == '__main__':
  main()
