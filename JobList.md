Fit inputs: 
* STXS signals (stage1++), reproduced
* None-split signals (ggZvv/qqZvv/qqWlv/ggZll/qqZllH), directly from ICHEP inputs
* Backgrounds, directly from ICHEP inputs

WSMaker:
* Perform fit with ICHEP backgrounds  together with either of
 * STXS signals
 * None-split signals
* POI schemes for STXS signals: (left table)
 * 1 POI
 * 2 POIs
 * 5 POIs + 1 normSys on WH0-150
 * 9 POIs + 1 normSys on WH0-150
- Signal theoretical systematics:
    - ICHEP
        - Possible to assigned on both signals
    - Re-evaluated for STXS splitting (denoted as Updated Sys.)
        - QCD scale on XS
        - PDF variation on XS
        - Only possible to be assign on STXS signals

| No.  | status | Job |
| ---  | ------ | ------------- |
|   1  |  done  | Complete uncertainties for 5 POIs.  |
|   2  |  done  | Comparison for fit with 5 POIs vs. ICHEP2018. |
|   3  |  done  | Investigation results on the suspicious NPs. |
|   4  |  done  | Fit scheme 2 is identical to fit scheme 1as one POI VH, we could modify FIt scheme 2 as 2 POIs: WH/ZH. |
|   5  |  done  | Use fit scheme 2 do the fit, compared breakdown with ICHEP2018. |
|   6  |  Wei  | 9 POI ws converted to 1 POI (VH) and then compared with ICHEP2018. |
|   7  |  ?  | 9 POI ws converted to 2 POI (WH/ZH) and then compared with ICHEP2018 WH/ZH breakdown. |
|   8  |  ?  | 9 POI ws converted to 5 POI and then compared with 5POI directly from WSM. |
|   9  |  ?  | New mode of the breakdown in the WSMCore to just show: Total / Stat. / Syst. / QCD scale on XS / PDF variation on XS | 
|  10  |  ?  | compare the breakdown and ranking of the signal sys between STXS signal sys and ICHEP sys for POI 2 |
|  11  |  ?  | compare the breakdown and ranking of the signal sys between STXS signal sys and ICHEP sys for POI 5 |



