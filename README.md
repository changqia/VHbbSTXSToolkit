# Toolkit for STXS study in VHbb analysis
[CxAODReader_VHbb]:https://gitlab.cern.ch/CxAODFramework/CxAODReader_VHbb
[WSMaker_VHbb]:https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker_VHbb
[CxAODMaker_STXS]: https://gitlab.cern.ch/changqia/CxAODMaker_STXS
[TupleProcessorSTXS]: https://gitlab.cern.ch/changqia/TupleProcessorSTXS
[twiki for WorkspaceCombiner]:https://twiki.cern.ch/twiki/bin/view/AtlasProtected/WorkspaceCombiner
[forked workspaceCombiner]: https://gitlab.cern.ch/changqia/workspaceCombiner
[config file for 5 POI vhbb]: https://gitlab.cern.ch/changqia/workspaceCombiner/blob/changqia-vhbb/cfg/hbb/mu_5poi_from_baseline.xml#L2

# Here show the toolkits used to read and diagonise the DxAOD files
## [CxAODMaker_STXS]
It process unskimmed DxAOD files for signals and gether together the STXS information and the weight variation into trees.

## [TupleProcessorSTXS]
It process NTuples from [CxAODMaker_STXS]. It works together with CxAOD framework.

For mc16a, the total number of events is 12128850. It takes 1:51:56 real time to process all of them. Too long, we need optimise it. Maybe add a batch driver like hsg5frameworkReader?

## [forked WorkspaceCombiner]
The basedline workspace is created with [WSMaker_VHbb], it has 9 POIs and 1 NormSys as:
```
WH0-150,WH150-250,WHGT250
qqZH0-150,qqZH150-250,qqZHGT250
ggZH0-150,ggZH150-250,ggZHGT250

NormSys on WH0-150
```
We use [forked workspaceCombiner] to convert our baseline workspace into 1/3/5/etc POIs scheme. A short instruction:
```
git clone https://:@gitlab.cern.ch:8443/changqia/workspaceCombiner.git
cd workspaceCombiner/
git checkout changqia-vhbb
source setup.sh
make
manager -w organize -x cfg/hbb/mu_5poi_from_baseline.xml
```
Then as described at [config file for 5 POI vhbb], the baseline workspace is converted into a 5 POIs (see below) workspace, which can be found at `workspace/`. 
```
SigXsecOverSMWHx150x250PTV
SigXsecOverSMWHxGT250PTV
SigXsecOverSMZHx0x150PTV
SigXsecOverSMZHx150x250PTV
SigXsecOverSMZHxGT250PTV
```
Further details can be found here: [twiki for WorkspaceCombiner]

Reminded by Nicolas, if we create our 5 POIs workspace with [forked workspaceCombiner] rather than create them directly in [WSMaker_VHbb], we may lost the benifits from nice features in [WSMaker_VHbb], like pruning and smoothing, given some of our gouping of the STXS may contain very few signal events, the effect is quite large. Meanwhile, the advantage with [forked workspaceCombiner], we don't need to create our STXS workspace so frenquently and all our work is based on a same workspace avoiding confusion due to different versions. Still the discussion is on-going.

# Some scripts for ploting and diagnose

## PlotAccPurYie.newVersion.AddMerge.py
Once run [CxAODReader_VHbb] for the STXS production, STXS 2D histograms will be created, for instance, `qqZvvH125_Stage1Bin_RecoMapEPSNicest`.

These 2D histograms record how many signal events from each STXS bin survive in each analysis category (for now, 2lepton topemu control regions are not recorded due to negligible signals in them).

They will be used to create Yields, Purity plots. Have a look at:
```
root -l /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.vvbb.13TeV.mc16a.AcademiaSinica.v06fixed2.MVA.STXS.v04.root
root [1] .ls *_Stage1Bin_RecoMapEPSNicest
TFile**		/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.vvbb.13TeV.mc16a.AcademiaSinica.v06fixed2.MVA.STXS.v04.root	
 TFile*		/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.vvbb.13TeV.mc16a.AcademiaSinica.v06fixed2.MVA.STXS.v04.root	
  KEY: TH2F	ggZllH125_Stage1Bin_RecoMapEPSNicest;1	ggZllH125_Stage1Bin_RecoMapEPSNicest
  KEY: TH2F	ggZvvH125_Stage1Bin_RecoMapEPSNicest;1	ggZvvH125_Stage1Bin_RecoMapEPSNicest
  KEY: TH2F	qqWlvH125_Stage1Bin_RecoMapEPSNicest;1	qqWlvH125_Stage1Bin_RecoMapEPSNicest
  KEY: TH2F	qqZllH125_Stage1Bin_RecoMapEPSNicest;1	qqZllH125_Stage1Bin_RecoMapEPSNicest
  KEY: TH2F	qqZvvH125_Stage1Bin_RecoMapEPSNicest;1	qqZvvH125_Stage1Bin_RecoMapEPSNicest
```

Also the initial sum of weight for each of STXS bin are required. They are collected by [TupleProcessorSTXS]. Have a look at this output of the package:
```
root -l /eos/user/c/changqia/STXS_VHbb/submitDir_Adv_mc16d_ICHEP_update/hist-sample.root
root [1] .ls *_Weight0NOEW_Stage1Nicest
TFile**		/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_mc16d_ICHEP_update/hist-sample.root	
 TFile*		/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_mc16d_ICHEP_update/hist-sample.root	
  KEY: TH1D	qqZllH125_Weight0NOEW_Stage1Nicest;1	qqZllH125_Weight0NOEW_Stage1Nicest
  KEY: TH1D	qqWlvH125M_Weight0NOEW_Stage1Nicest;1	qqWlvH125M_Weight0NOEW_Stage1Nicest
  KEY: TH1D	qqWlvH125P_Weight0NOEW_Stage1Nicest;1	qqWlvH125P_Weight0NOEW_Stage1Nicest
  KEY: TH1D	qqZvvH125_Weight0NOEW_Stage1Nicest;1	qqZvvH125_Weight0NOEW_Stage1Nicest
  KEY: TH1D	ggZllH125_Weight0NOEW_Stage1Nicest;1	ggZllH125_Weight0NOEW_Stage1Nicest
  KEY: TH1D	ggZvvH125_Weight0NOEW_Stage1Nicest;1	ggZvvH125_Weight0NOEW_Stage1Nicest

```
The default weight NNPDF3 without EW correction will be used, corresponding to `Weight0NOEW`.

The split scheme is defined like `SplitScheme = Nicest`. If you want stage1++, put `Nicest` here. Other option `Fine` or `Base` is also there, but not tested by myself.

<img src="plots.ICHEP2018.mc16a.v04/Yields.mc16a.png" width="950"/>

### You are not happy with 81 bins. Instead you just want 3 bins like (VH0-150/VH150-250/VHGT250) for instance.

If you set use `Nicest` as the split scheme, merging of the bins feature can be used, which is based on `Nicest`. See `DefMergeScheme_18` and `DefMergeScheme_6` method, it provides you the possiblility to show Yields/Purity/Acceptance for any grouping scheme (based on the template in your input file) as you want. Friendly reminder, you may want to change the width and height, search for `(nX,nY)`, you can define the plot setting for the numbers of your useddefiled analysis category and STXS bins.

<img src="plots.ICHEP2018.mc16a.v04/Yields_Merged.mc16a.png" width="950"/>

## group.py
In stage1++ split, we have 81 bins, people want to group them into 3/6/9/etc. Then this script can help.

You define the group with `Reqlist`, for instance:
```
Reqlist['ggZH0x150PTVxGE1J']=     {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150,   'njet':1, 'njet_infi': True}
```
We create a group called `ggZH0x150PTVxGE1J`, with all ggZHll and ggZHvv described in `pro_mode`, pTV in [0,150] with `ptv_low` and `ptv_high`, and njet >= 1 with `'njet':1`, `'njet_infi': True`.

It contains three checks:
* how many members in each of group
* if my grouping picks all signals
* if my gouping doesn't have overlap

By defaul setting in current script (02-Aug-2018), it will show:
```
no one picks QQ2HLNUxFWDH
no one picks QQ2HLLxFWDH
no one picks QQ2HNUNUxFWDH
no one picks GG2HLLxFWDH
no one picks GG2HNUNUxFWDH
```
Means no groop includes these bins.

### Yes, it is good, but I want to put these groups into my workspace maker or for other application situation.
Like the keywords definition in [WSMaker_VHbb] and the merging scheme used by `PlotAccPurYie.newVersion.AddMerge.py`, we want to make our printout directly used by the script/package. This two methods might be useful:
* generate_group_for_wsm
* generate_group_for_PlotAccPurYie

For suer, you can do your userdefined method to print the gouping in the format you want.

## NicePlot_SimpleMu.py from Valerio
The brekdown tables from [WSMaker_VHbb] records the systematics impact on the signal strength, for instance:
```
/eos/user/c/changqia/STXS_VHbb/VHbb_5mu_from_BaseLine/plots/breakdown/
```
Now you run the following command to read the above files:
```
python NicePlot_SimpleMu.py 24 1
python NicePlot_SimpleMu.py 25 1
```
This script will extract the information and show them like the below plot:

<img src="NicePlot.STXS.v04/Plot_mu_24_VH.png" width="425"/> <img src="NicePlot.STXS.v04/Plot_mu_25_VH.png" width="425"/> 

Search for `mode=="25"`, you will know how to set them. :)

## QCDScaleError.py
This script is used to analysis the output from [TupleProcessorSTXS]. It has the following features:
* Extract the initial yields for each bin in each weight variation and put them into a dictionary, see `weight_var` and `Yields = {}`
* Print out the initial yields, see `def GenerateYields()`
* Calculate the acceptance (I would prefer call it fraction of each bin) with `Yields`, see `def GenerateAcceptance()`
* QCD scale variation systematics evaluation calculation. The calculation follows the discussion with Milene, Thomas, Giovanni, Paolo, Frank, Carlo and other friends from WG1/2. See `class SigQCDError` and `def perform_calculation_for_variation(self)`.
* The prinout/plots of the QCD scale variation systematics calculation, see `drawRelativeErrors()`, `drawAbsoluteErrors()` and `drawRelativeErrorFromEachDelta()`

<img src="QCDScaleVariationCalculation.Nicest.v16/qqZvvH125_RelativeErrorFromEachDeltaPTVwithCorrFact0.5.png" width="425"/>  <img src="QCDScaleVariationCalculation.Nicest.v16/qqZvvH125_RelativeErrorFromEachDeltaNJET.png" width="425"/>
* Get the final impact for the parameters, see `Collect_Delta_group_for_ws()`
* Merge the results from `W+H`, `W-H`, `qqZllH`, `qqZvvH`, `ggZllH` and `qqZvvH` into `WH` (xs weighted averaged), `qqZH` (flat avergaed) and `ggZH` (flat avergaed).
* Printout the format compatible with [WSMaker_VHbb], value and the corresponding group of systematics
* To validate the calculation, a demo is created, you can compare the results from your own calculation and from this script. `PrepareQCDScaleVariationWithDemo` and `TestTheDemo`.

The calculation agree with the results from another approach. And also the template is not hardcoded, so in princeple, if you following the naming convention `PTV` and `NJET` as in our signal, the results could be automatically generated with the same logic used in VHbb group.

I should admit this script is not elegant, it has much room to improve.


