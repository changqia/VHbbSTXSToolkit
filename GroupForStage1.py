import group as group

# Merging scheme 1
group.GroupScheme.clear() # clear the scheme in group
# define the group scheme
group.GroupScheme['VH0x75']    = {'ptv_low':0, 'ptv_high':75,}
group.GroupScheme['VH75x150']  = {'ptv_low':75, 'ptv_high':150,}
group.GroupScheme['VH150x250'] = {'ptv_low':150, 'ptv_high':250,}
group.GroupScheme['VHGT250']   = {'ptv_low':250,}
group.main(group.GroupScheme, outputForPlot = True)


# Merging scheme 2
group.GroupScheme.clear() # clear the scheme in group
# define the group scheme
group.GroupScheme['ZvvH_0J']    = {'pro_mode':['QQ2HNUNU','GG2HNUNU'], 'njet':0}
group.GroupScheme['ZvvH_GE1J']  = {'pro_mode':['QQ2HNUNU','GG2HNUNU'], 'njet':1, 'njet_infi': True}
group.GroupScheme['WH_0J']      = {'pro_mode':['QQ2HLNU'], 'njet':0}
group.GroupScheme['WH_GE1J']    = {'pro_mode':['QQ2HLNU'], 'njet':1, 'njet_infi': True}
group.GroupScheme['ZllH_0J']    = {'pro_mode':['QQ2HLL','GG2HLL'], 'njet':0}
group.GroupScheme['ZllH_GE1J']  = {'pro_mode':['QQ2HLL','GG2HLL'], 'njet':1, 'njet_infi': True}
group.main(group.GroupScheme, outputForPlot = True)

# Merging scheme 2
group.GroupScheme.clear() # clear the scheme in group
# define the group scheme
group.GroupScheme['WHx0x150PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
group.GroupScheme['WHx150x250PTVx0J']      = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0, }
group.GroupScheme['WHx150x250PTVxGE1J']    = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True, }
group.GroupScheme['WHxGT250PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
group.GroupScheme['QQZHx0x150PTV']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':0, 'ptv_high':150}
group.GroupScheme['QQZHx150x250PTVx0J']    = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0, }
group.GroupScheme['QQZHx150x250PTVxGE1J']  = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True, }
group.GroupScheme['QQZHxGT250PTV']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':250}
group.GroupScheme['GGZHx0x150PTV']         = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':0,   'ptv_high':150}
group.GroupScheme['GGZHxGT150x0J']         = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'njet':0, }
group.GroupScheme['GGZHxGT150xGE1J']       = {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'njet':1, 'njet_infi': True, }
group.main(group.GroupScheme, outputForWSM = True)

group.GroupScheme.clear() # clear the scheme in group
# define the group scheme
group.GroupScheme['WHx0x150PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
group.GroupScheme['WHx150x250PTV']         = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250}
group.GroupScheme['WHxGT250PTV']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
group.GroupScheme['ZHx0x150PTV']           = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150}
group.GroupScheme['ZHx150x250PTVx0J']      = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250}
group.GroupScheme['ZHxGT250PTV']           = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':250}
group.main(group.GroupScheme, outputForPlot = True)

