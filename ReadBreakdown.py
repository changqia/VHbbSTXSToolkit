from collections import OrderedDict

path = '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/'
tasks_Reduced = [
  ['ggZH', path+'3mu/XS/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM'],
  ['qqZH', path+'3mu/XS/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM'],
  ['WH',   path+'3mu/XS/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM'],
]

tasks_Run2 = [
  ['ggZH', path+'3mu/XS/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM'],
  ['qqZH', path+'3mu/XS/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM'],
  ['WH',   path+'3mu/XS/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM'],
]

def getResults(tasks):
  Result_collection = OrderedDict()
  for obj in tasks:
    theF=file(obj[1]+obj[0]+'.txt')
    lines=theF.readlines()
    index=1
    incremental=0
    info = OrderedDict()
    readThis=False
    for line in lines:
      if "muhat" in line:
        incremental+=1
        if incremental==index: readThis=True
        else                 : readThis=False
        #print line
        if readThis: info['mu']=float(line.split(":")[1])
      elif "Total" in line and readThis:
        if 'total up' in info: continue
        info['total up']=float(line.split()[2])
        info['total dn']=float(line.split()[4])
      elif "DataStat" in line and readThis:
        if 'stat up' in info: continue
        info['stat up']=float(line.split()[2])
        info['stat dn']=float(line.split()[4])
      elif "Signal" in line and readThis:
        if 'sig up' in info: continue
        info['sig up']=float(line.split()[4])
        info['sig dn']=float(line.split()[6])
      elif "Background" in line and readThis:
        if 'bkg up' in info: continue
        info['bkg up']=float(line.split()[4])
        info['bkg dn']=float(line.split()[6])
      elif "Experimental" in line and readThis:
        if 'exp up' in info: continue
        info['exp up']=float(line.split()[3])
        info['exp dn']=float(line.split()[5])
      elif "Lumi" in line and readThis:
        if 'lumi up' in info: continue
        info['lumi up']=float(line.split()[2])
        info['lumi dn']=float(line.split()[4])
    Result_collection[obj[0]]=info
    theF.close()
  return Result_collection

def checkResults(Result):
  for res in Result:
    print res
    res_info = Result[res]
    for item in res_info:
      value = res_info[item]
      print item,value

def show(sig,segment,resout,resin,sf = 1.0):
  info_run2 = resout[sig]
  info_redu = resin[sig]
  value_run2_up = info_run2[segment +' up'] * sf
  value_run2_dn = info_run2[segment +' dn'] * sf
  value_redu_up = info_redu[segment +' up'] * sf
  value_redu_dn = info_redu[segment +' dn'] * sf
  output = '&\ ^{+%.3f}_{-%.3f}(^{+%.3f}_{-%.3f})'%(value_run2_up,value_run2_dn,value_redu_up,value_redu_dn)
  return output

def show_secquence(sig,res_run2,res_redu,doXS=True):
  xs = 1.0
  if sig == 'WH': xs = 0.877
  elif sig == 'qqZH': xs = 0.488
  elif sig == 'ggZH': xs = 0.084
  scale_factor = xs if doXS else 1.

  # 1
  first_line = '\mathrm{\sigma}(%s) \\times \mathrm{B}(H\\to b\\bar{b}) = %.3f'%(sig.replace('ZH','\\to ZH'),xs)
  print first_line
  pb_sufix = '\,\\mathrm{pb} \\\\'

  # 2
  print show(sig,'total', res_run2, res_redu, scale_factor)+pb_sufix

  # 3
  print '= %.3f'%xs

  # following 
  print show(sig,'stat', res_run2, res_redu, scale_factor)+'\,(\\mathrm{%s})\\\\'%('stat')
  print show(sig,'exp',  res_run2, res_redu, scale_factor)+'\,(\\mathrm{%s})\\\\'%('exp')
  print show(sig,'sig',  res_run2, res_redu, scale_factor)+'\,(\\mathrm{%s})\\\\'%('sig')
  print show(sig,'bkg',  res_run2, res_redu, scale_factor)+'\,(\\mathrm{%s})\\\\'%('bkg')
  print show(sig,'lumi', res_run2, res_redu, scale_factor)+'\,(\\mathrm{%s})'%('lumi')+pb_sufix

def show_table(sig,res_run2,res_redu):
  info_run2 = res_run2[sig]
  info_redu = res_redu[sig]
  print sig
  output_S1 =  '& HL-LHC w. Scenario S1             & $^{+%.3f}_{-%.3f}$      & $^{+%.3f}_{-%.3f}$   &  $^{+%.3f}_{-%.3f}$  & $^{+%.3f}_{-%.3f}$       & $^{+%.3f}_{-%.3f}$  &  $^{+%.3f}_{-%.3f}$\\\\'%(info_run2['total up'],info_run2['total dn'],info_run2['stat up'],info_run2['stat dn'],info_run2['exp up'],info_run2['exp dn'],info_run2['bkg up'],info_run2['bkg dn'],info_run2['sig up'],info_run2['sig dn'],0,0)
  print output_S1
  print '&&&&&&\\\\'
  output_S2 =  '& HL-LHC w. Scenario S2             & $^{+%.3f}_{-%.3f}$      & $^{+%.3f}_{-%.3f}$   &  $^{+%.3f}_{-%.3f}$  & $^{+%.3f}_{-%.3f}$       & $^{+%.3f}_{-%.3f}$  &  $^{+%.3f}_{-%.3f}$\\\\'%(info_redu['total up'],info_redu['total dn'],info_redu['stat up'],info_redu['stat dn'],info_redu['exp up'],info_redu['exp dn'],info_redu['bkg up'],info_redu['bkg dn'],info_redu['sig up'],info_redu['sig dn'],0,0)
  print output_S2
  print '&&&&&&\\\\'

def main():
  # get the Run2:
  res_run2 = getResults(tasks_Run2)
  # get the reduced:
  res_redu = getResults(tasks_Reduced)
  show_secquence('WH',res_run2,res_redu)
  show_secquence('qqZH',res_run2,res_redu)
  show_secquence('ggZH',res_run2,res_redu)

  # show table
  show_table('WH',res_run2,res_redu)
  show_table('qqZH',res_run2,res_redu)
  show_table('ggZH',res_run2,res_redu)

if __name__ == '__main__':
  main()
