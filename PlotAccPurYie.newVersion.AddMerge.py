#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import collections
import array
import numpy as np
import math
import os
import socket
doYieldsForInitial = True # a new switch for plotting XS and Yields, only when it is true, the acceptance has correct meaning
KillBlank = False # if the figure in the cell is 0, then replace it by 0.0001, so the color of the cell is light violet matching 0.0, this is only valid for merged case, leave the raw case for validation
BlankTiny = True # if the figure in the cell is below 0.05, then replace it by 0, so the color of the cell is blank, this is only valid for merged case, leave the raw case for validation
RunSys = False
if RunSys:
  #SysName = 'SysVHNLOEWK__1down'
  SysName = 'SysVHNLOEWK__1up'
LocalRun = True
hostname =  socket.gethostname()
if hostname.startswith('lxplus'):
  LocalRun = False
  print 'Run on lxplus'
  print hostname
else:
  print 'You run locally, you need define the localtion of the inputs'
  print hostname

#ROOT.gStyle.SetPaintTextFormat("1.5f")
ROOT.gStyle.SetPaintTextFormat(".1f")
ROOT.gStyle.SetOptStat(0)
#ROOT.gROOT.LoadMacro("macros/AtlasStyle.C")
#ROOT.SetAtlasStyle(0.03)
#ROOT.gStyle.SetPalette(7)

NRGBs = 3;
NCont = 512;
# stops = [ 0.00, 0.50, 1.00 ];
# red   = [ 0.00, 1.00, 1.00 ];
# green = [ 0.00, 1.00, 0.00 ];
# blue  = [ 0.00, 1.00, 0.00 ];
# stops = [0.00, 0.34, 0.61, 0.84, 1.00]
# red   = [1.00, 0.90, 0.80, 0.70, 0.60]
# green = [0.90, 0.60, 0.40, 0.10, 0.00]
# blue  = [0.90, 0.60, 0.40, 0.10, 0.00]

# stops = [0.00, 0.20, 0.5, 0.8, 1.00]
# blue  = [1.00, 0.90, 0.82, 0.76, 0.70]
# green = [0.90, 0.60, 0.40, 0.30, 0.30]
# red   = [0.90, 0.60, 0.40, 0.30, 0.30]

stops = [0.00, 0.25, 0.5, 0.8, 1.00]
blue  = [1.00, 0.90, 0.80, 0.81, 0.76]
green = [0.90, 0.67, 0.55, 0.35, 0.30]
red   = [0.90, 0.67, 0.55, 0.35, 0.30]

s = array.array('d', stops)
r = array.array('d', red)
g = array.array('d', green)
b = array.array('d', blue)
#nContours = len(s)
ROOT.TColor.CreateGradientColorTable(NRGBs, s, r, g, b, NCont);
ROOT.gStyle.SetNumberContours(NCont)

#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

debug = True
MCType = 'mc16ad'
#output_dir = 'plots.from.Luca'
output_dir = 'plots.ICHEP2018.'+MCType+'.paper'
SplitScheme = 'Nicest'

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

signal_names = [
  'qqZvvH125', 'qqZllH125', 'ggZllH125', 'ggZvvH125', 'qqWlvH125'
]

# set the input files
if LocalRun:
  InitialYieldsFile = '/Users/changqiao/Workspace/STXS/DxAOD_output_stage16/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'
  ReconsYieldsFiles = {
  '0lep':'/Users/changqiao/Workspace/STXS/inputs_for_ICHEP2018/LimitHistograms.VH.vvbb.13TeV.STXS.LPNHE.'+MCType+'.v04.root',
  '1lep':'/Users/changqiao/Workspace/STXS/inputs_for_ICHEP2018/LimitHistograms.VH.lvbb.13TeV.STXS.LPNHE.'+MCType+'.v04.root',
  '2lep':'/Users/changqiao/Workspace/STXS/inputs_for_ICHEP2018/STXS_2l_'+MCType+'_syst.root',
  }
else:
  InitialYieldsFile = '/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'
  ReconsYieldsFiles = {
  '0lep':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.vvbb.13TeV.'+MCType+'.AcademiaSinica.v06fixed2.MVA.STXS.v04.root',
  '1lep':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.lvbb.13TeV.'+MCType+'.LAL.v06.STXS.v04.root',
  '2lep':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/LimitHistograms.VH.llbb.13TeV.'+MCType+'.LiverpoolBmham.v11.STXS.v04.root',
  }

# check if all inputs are in place
if not os.path.isfile(InitialYieldsFile):
  print 'Your file for initial yields (%s) does not exist'%InitialYieldsFile
  print 'Exiting!!'
  exit()
for lep in ReconsYieldsFiles:
  if not os.path.isfile(ReconsYieldsFiles[lep]):
    print 'Your file for reco. yields (%s) does not exist'%ReconsYieldsFiles[lep]
    print 'Exiting!!'
    exit()

sigs = []

class Signal(object):
  def __init__(self, H, hs, name):
    self.H = clone(H)
    self.name = name
    self.showname = 'default'
    self.bins = {}
    self.vector = None
    self.LabelX = []
    self.LabelY = []
    self.matrices = {}
    self.InitializeReconHistos(hs)
    self.FillTheBins(hs)

  def InitializeReconHistos(self,hs):
    for key in hs:
      if debug: print key
      h = hs[key]
      #x_axis = h.GetXaxis()
      #y_axis = h.GetYaxis()
      if debug:
        print ' Entries:',h.GetEntries()
        #mymatrix = ROOT.TMatrix(x_axis.GetNbins()+2,y_axis.GetNbins()+2,h.GetArray(),"D")
        #mymatrix.Print()
      matrix = Convert2DHistToMatrix(h)
      if debug:
        #np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
        #np.set_printoptions(precision=3, strip_zeros=False)
        np.set_printoptions(precision=2, suppress=True)
        print (matrix)
      self.matrices[key] = matrix
      if not self.LabelY:
        y_axis=h.GetYaxis()
        Nbins=y_axis.GetNbins()
        for count in range(0, Nbins):
          self.LabelY.append(y_axis.GetBinLabel(count+1))
      if debug: print 'the length of self.LabelY is: ',len(self.LabelY)

  def FillTheBins(self,hs):
    if debug: print ' Integral:',self.H.Integral()
    #xaxis = self.H.GetXaxis()
    xaxis = (hs.values()[0]).GetXaxis()
    Nbins = xaxis.GetNbins()
    bin_vals = []
    if debug:
      print 'NBins: ',Nbins
    for count in range(0, Nbins):
      if debug: print xaxis.GetBinLabel(count+1),self.H.GetBinContent(count+1)
      self.bins[xaxis.GetBinLabel(count+1)]=self.H.GetBinContent(count+1)
      self.LabelX.append(xaxis.GetBinLabel(count+1))
      bin_vals.append(self.H.GetBinContent(count + 1))
    self.vector = np.array(bin_vals)
  
  def CheckTheBins(self):
    if debug:
      print 'Now check the bins'
    for BIN in self.LabelX:
      print self.bins[BIN]

  def GetVector(self):
    return self.vector

  def GetMatrices(self):
    ms = []
    for m_name in self.matrices:
      ms.append(self.matrices[m_name])
    return ms

  def GetLabelX(self):
    return self.LabelX

  def GetLabelY(self):
    return self.LabelY

def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

def Convert2DHistToMatrix(hist):
  x_bins = hist.GetNbinsX()
  y_bins = hist.GetNbinsY()
  bins = np.zeros((x_bins,y_bins))
  for y_bin in xrange(y_bins):
    for x_bin in xrange(x_bins):
      bins[x_bin,y_bin] = hist.GetBinContent(x_bin + 1,y_bin + 1)
  print bins
  return bins

def ConvertMatrixTo2DHist(matrix,name):
  nbinsx,nbinsy = matrix.shape
  hist = ROOT.TH2D(name, name, nbinsx, 0, nbinsx, nbinsy, 0., nbinsy);
  for y_bin in xrange(nbinsy):
    for x_bin in xrange(nbinsx):
      hist.SetBinContent(x_bin + 1,y_bin + 1, matrix[x_bin,y_bin])
  return hist

def Set2DHistLabels(hist,labelx,labely):
  nbinsx = hist.GetNbinsX()
  nbinsy = hist.GetNbinsY()
  for x_bin in xrange(nbinsx):
    hist.GetXaxis().SetBinLabel(x_bin+1,labelx[x_bin])
  for y_bin in xrange(nbinsy):
    hist.GetYaxis().SetBinLabel(y_bin+1,labely[y_bin])

def Merge_WpH_WmH(histP,histM):
  hP = histP.Clone()
  hM = histM.Clone()
  hP.Add(hM)
  return hP

def HistRescale(hist,name):
  sumofweights = hist.Integral()/2. # divided by 2 due to 1 event filled by twice, one for the totoal one for certain bin
  scale_factor = 1.0
  br = 0.582
  luminosity = 1.
  if MCType == "mc16ad" : luminosity *= 79.8
  elif MCType == "mc16a" : luminosity *= 36.2
  elif MCType == "mc16d" : luminosity *= 43.6
  scale_factor *= br * 1000 #* luminosity 
  # the cross section in pb
  if name == 'qqWlvH125M':   scale_factor *= 0.17949
  elif name == 'qqWlvH125P': scale_factor *= 0.28278
  elif name == 'qqZllH125':  scale_factor *= 0.07704
  elif name == 'qqZvvH125':  scale_factor *= 0.15305
  elif name == 'ggZllH125':  scale_factor *= 0.01242
  elif name == 'ggZvvH125':  scale_factor *= 0.02457
  if doYieldsForInitial:
    hist.Scale(luminosity*scale_factor/sumofweights)
  else:
    hist.Scale(scale_factor/sumofweights)

def Draw2DPlot( histo, LabelX, LabelY, LabelZ, outputname, ymin=0., ymax=100., yramin=0., yramax=100.):
  hist = histo.Clone()
  Set2DHistLabels(hist,LabelX,LabelY)
  nX = len(LabelX)
  nY = len(LabelY)
  ATLAS_pos   = 0.39
  RightMargin = 0.09
  LabelSize_Z = 0.04
  
  # do some pruning for the numbers in the cell
  BlankThrehold = 0.01 if 'Acceptance' in outputname else 0.1
  for ix in range(0, nX):
    for iy in range(0, nY):
      raw = hist.GetBinContent(ix+1, iy+1)
      if BlankTiny: fill_val = 0 if math.fabs(raw) < BlankThrehold else raw
      elif KillBlank: fill_val = 1./1e6 if raw == 0 else raw
      else: fill_val = raw
      hist.SetBinContent(ix+1, iy+1, fill_val)

  if (nX,nY) == (9,8):
    can_width = 1600
    can_height = 1200
    BotMargin  = 0.15
    LeftMargin = 0.2
    NumSize = 1.2
    LabelSize_X = 0.04
    LabelSize_Y = 0.028
    ATLAS_pos   = 0.75
  elif (nX,nY) == (8,8):
    can_width = 1600
    can_height = 900
    BotMargin  = 0.2
    LeftMargin = 0.28
    RightMargin = 0.15
    NumSize = 2.0
    LabelSize_X = 0.05
    LabelSize_Y = 0.05
    LabelSize_Z = 0.045
  elif (nX,nY) == (7,8):
    can_width = 1600
    can_height = 900
    BotMargin  = 0.2
    LeftMargin = 0.28
    RightMargin = 0.15
    NumSize = 2.0
    LabelSize_X = 0.05
    LabelSize_Y = 0.05
    LabelSize_Z = 0.045
  elif (nX,nY) == (5,8):
    can_width = 1600
    can_height = 900
    BotMargin  = 0.2
    LeftMargin = 0.28
    RightMargin = 0.15
    NumSize = 2.0
    LabelSize_X = 0.05
    LabelSize_Y = 0.05
    LabelSize_Z = 0.045
  elif (nX,nY) == (15,8):
    can_width = 2000
    can_height = 1200
    BotMargin  = 0.12
    LeftMargin = 0.16
    RightMargin = 0.09
    NumSize = 1.2
    LabelSize_X = 0.03
    LabelSize_Y = 0.028
    ATLAS_pos   = 0.6
  elif (nX,nY) == (22,8):
    can_width = 2000
    can_height = 1200
    BotMargin  = 0.12
    LeftMargin = 0.16
    RightMargin = 0.09
    NumSize = 1.2
    LabelSize_X = 0.03
    LabelSize_Y = 0.028
  elif (nX,nY) == (81,10):
    can_width = 2400
    can_height = 800
    BotMargin  = 0.30
    LeftMargin = 0.09
    RightMargin = 0.075
    NumSize = 0.75
    LabelSize_X = 0.03
    LabelSize_Y = 0.025
    ATLAS_pos   = 0.75
  else:
    can_width = 2000
    can_height = 1200
    BotMargin  = 0.30
    LeftMargin = 0.12
    RightMargin = 0.09
    NumSize = 0.75
    LabelSize_X = 0.03
    LabelSize_Y = 0.025
  c1 = ROOT.TCanvas(outputname,outputname,can_width,can_height)
  #ROOT.gStyle.SetPaintTextFormat("4.1f")
  c1.SetGrid()
  c1.SetBottomMargin(BotMargin)
  c1.SetLeftMargin(LeftMargin)
  c1.SetRightMargin(RightMargin)
  hist.SetTitle('')
  hist.GetZaxis().SetTitle(LabelZ)
  hist.GetZaxis().SetTitleOffset(0.75)
  hist.GetZaxis().SetTitleSize(LabelSize_Z)
  hist.SetMarkerSize(NumSize)
  hist.GetXaxis().SetLabelSize(LabelSize_X)
  hist.GetXaxis().SetLabelOffset(0.012)
  hist.GetYaxis().SetLabelSize(LabelSize_Y)
  hist.SetMinimum(ymin);
  print "YOLOOOOOO"
  print ymax
  hist.SetMaximum(ymax);
  hist.GetZaxis().SetRangeUser(yramin,yramax)
  #hist.Draw("COLZTEXT45")
  hist.Draw("COLZTEXT")

  c1.Modified()
  l = ROOT.TLatex()
  l.SetNDC();
  l.SetTextFont(72);
  if LabelZ is "events": l.DrawLatex(ATLAS_pos-0.105,0.92,"#font[72]{ATLAS} #font[42]{Simulation Internal, #sqrt{s} = 13 TeV, 79.8 fb^{-1}}")
  else: l.DrawLatex(ATLAS_pos,0.92,"#font[72]{ATLAS} #font[42]{Simulation Internal, #sqrt{s} = 13 TeV}")
  
  #l2 = ROOT.TLatex()
  #l2.SetNDC();
  #l2.SetTextFont(42);
  #l2.DrawLatex(0.65,0.8,"Simulation Internal");
  #c1.Modified()
  #c1.Update()

  c1.Print('%s/%s_%s.png'%(output_dir,outputname,MCType))
  c1.Print('%s/%s_%s.eps'%(output_dir,outputname,MCType))
  c1.Print('%s/%s_%s.root'%(output_dir,outputname,MCType))

def Draw1DPlot( hist, outputname):
  nX = hist.GetNbinsX()
  ATLAS_pos = 0.55
  if nX == 80:
    can_width = 2400
    can_height = 800
    BotMargin  = 0.32
    TopMargin  = 0.02
    LeftMargin = 0.06
    RightMargin = 0.02
    NumSize = 0.75
    LabelSize_X = 0.03
    ATLAS_pos = 0.75
  elif nX == 21:
    can_width = 2200
    can_height = 1200
    BotMargin  = 0.25
    TopMargin  = 0.02
    LeftMargin = 0.06
    RightMargin = 0.02
    NumSize = 0.75
    LabelSize_X = 0.03
  elif nX == 14:
    can_width = 2000
    can_height = 1200
    BotMargin  = 0.25
    TopMargin  = 0.02
    LeftMargin = 0.08
    RightMargin = 0.06
    NumSize = 0.75
    LabelSize_X = 0.03
    ATLAS_pos   = 0.55
  else :
    can_width = 2000
    can_height = 1200
    BotMargin  = 0.25
    TopMargin  = 0.02
    LeftMargin = 0.12
    RightMargin = 0.09
    NumSize = 0.75
    LabelSize_X = 0.03
    #LabelSize_Y = 0.04
  c1 = ROOT.TCanvas(outputname,outputname,can_width,can_height)
  #c1.SetGrid()

  c1.SetTopMargin(TopMargin)
  c1.SetBottomMargin(BotMargin)
  c1.SetLeftMargin(LeftMargin)
  c1.SetRightMargin(RightMargin)
  hist.SetTitle('')
  hist.SetMarkerSize(NumSize)
  hist.SetLineColor(ROOT.kBlack)
  hist.GetXaxis().LabelsOption('v')
  hist.SetLineWidth(2)
  hist.GetXaxis().SetLabelSize(LabelSize_X)
  #hist.GetYaxis().SetLabelSize(LabelSize_Y)
  hist.GetYaxis().SetTitle("cross section [fb]")
  if debug: print hist.GetMaximum()
  hist.SetMaximum(hist.GetMaximum()*1.35)
  hist.Draw()
  hist.Draw("TEXTsame")

  c1.Modified()
  l = ROOT.TLatex()
  l.SetNDC();
  #l.SetTextFont(72);
  l.DrawLatex(ATLAS_pos,0.9,"#font[72]{ATLAS} #font[42]{Simulation Internal}");
  
  #l2 = ROOT.TLatex()
  #l2.SetNDC();
  #l2.SetTextFont(42);
  #l2.DrawLatex(0.65,0.8,"Simulation Internal");
  #c1.Modified()
  #c1.Update()

  c1.Print('%s/%s_%s.png'%(output_dir,outputname,MCType))
  c1.Print('%s/%s_%s.eps'%(output_dir,outputname,MCType))
  c1.Print('%s/%s_%s.root'%(output_dir,outputname,MCType))

def DefMergeScheme_3():
  MergeDict = collections.OrderedDict()
  MergeDict['|y_{Higgs}| > 2.5']            = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH', 'QQ2HLNU_FWDH', 'GG2HNUNU_FWDH', 'GG2HLL_FWDH', ] # 5
  MergeDict['WH, p_{T}^{W} > 150 GeV']      = [ "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_0J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ZH, 75 < p_{T}^{Z} < 150 GeV'] = [ "QQ2HLL_PTV_75_150_0J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", "GG2HLL_PTV_75_150_0J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_75_150_0J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ZH, p_{T}^{Z} > 150 GeV']      = [ "QQ2HLL_PTV_150_250_0J", "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_0J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_0J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", "GG2HLL_PTV_150_250_0J", "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_0J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_0J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]
  return MergeDict

def DefMergeScheme_3_Rich():
  MergeDict = collections.OrderedDict()
  #MergeDict['|y_{Higgs}| > 2.5']            = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH', 'QQ2HLNU_FWDH', 'GG2HNUNU_FWDH', 'GG2HLL_FWDH', ] # 5
  MergeDict['WH, p_{T}^{W} < 150 GeV']      = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_0J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['WH, p_{T}^{W} > 150 GeV']      = [ "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_0J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ZH, p_{T}^{Z} < 75 GeV']      = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", ]
  MergeDict['ZH, 75 < p_{T}^{Z} < 150 GeV'] = [ "QQ2HLL_PTV_75_150_0J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", "GG2HLL_PTV_75_150_0J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_75_150_0J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ZH, p_{T}^{Z} > 150 GeV']      = [ "QQ2HLL_PTV_150_250_0J", "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_0J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_0J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", "GG2HLL_PTV_150_250_0J", "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_0J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_0J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]
  return MergeDict

def DefMergeScheme_6():
  MergeDict = collections.OrderedDict()
  #MergeDict['|y_{Higgs}| > 2.5']           = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH', 'QQ2HLNU_FWDH', 'GG2HNUNU_FWDH', 'GG2HLL_FWDH', ] # 5

  MergeDict['WH, p_{T}^{W} < 150 GeV']   = [ 'QQ2HLNU_PTV_0_75_0J', 'QQ2HLNU_PTV_0_75_1J', 'QQ2HLNU_PTV_0_75_GE2J', 'QQ2HLNU_PTV_75_150_0J', 'QQ2HLNU_PTV_75_150_1J', 'QQ2HLNU_PTV_75_150_GE2J', ] # 6
  MergeDict['WH, 150 < p_{T}^{W} < 250 GeV'] = [ 'QQ2HLNU_PTV_150_250_0J', 'QQ2HLNU_PTV_150_250_1J', 'QQ2HLNU_PTV_150_250_GE2J', ] # 3
  MergeDict['WH, p_{T}^{W} > 250 GeV']   = [ 'QQ2HLNU_PTV_250_400_0J', 'QQ2HLNU_PTV_250_400_1J', 'QQ2HLNU_PTV_250_400_GE2J', 'QQ2HLNU_PTV_GT400_0J', 'QQ2HLNU_PTV_GT400_1J', 'QQ2HLNU_PTV_GT400_GE2J', ] # 6
  MergeDict['ZH, p_{T}^{Z} < 75 GeV']   = [ 'QQ2HNUNU_PTV_0_75_0J', 'QQ2HNUNU_PTV_0_75_1J', 'QQ2HNUNU_PTV_0_75_GE2J',
                                 'QQ2HLL_PTV_0_75_0J', 'QQ2HLL_PTV_0_75_1J', 'QQ2HLL_PTV_0_75_GE2J',
                                 'GG2HNUNU_PTV_0_75_0J', 'GG2HNUNU_PTV_0_75_1J', 'GG2HNUNU_PTV_0_75_GE2J',
                                 'GG2HLL_PTV_0_75_0J', 'GG2HLL_PTV_0_75_1J', 'GG2HLL_PTV_0_75_GE2J', ] # 24

  MergeDict['ZH, 75 < p_{T}^{Z} < 150 GeV']   = [ 'QQ2HNUNU_PTV_75_150_0J', 'QQ2HNUNU_PTV_75_150_1J', 'QQ2HNUNU_PTV_75_150_GE2J',
                                   'QQ2HLL_PTV_75_150_0J', 'QQ2HLL_PTV_75_150_1J', 'QQ2HLL_PTV_75_150_GE2J',
                                   'GG2HNUNU_PTV_75_150_0J', 'GG2HNUNU_PTV_75_150_1J', 'GG2HNUNU_PTV_75_150_GE2J',
                                   'GG2HLL_PTV_75_150_0J', 'GG2HLL_PTV_75_150_1J', 'GG2HLL_PTV_75_150_GE2J', ] # 24

  MergeDict['ZH, 150 < p_{T}^{Z} < 250 GeV'] = [ 'QQ2HNUNU_PTV_150_250_0J', 'QQ2HNUNU_PTV_150_250_1J', 'QQ2HNUNU_PTV_150_250_GE2J',
                                'QQ2HLL_PTV_150_250_0J', 'QQ2HLL_PTV_150_250_1J', 'QQ2HLL_PTV_150_250_GE2J',
                                'GG2HNUNU_PTV_150_250_0J', 'GG2HNUNU_PTV_150_250_1J', 'GG2HNUNU_PTV_150_250_GE2J',
                                'GG2HLL_PTV_150_250_0J', 'GG2HLL_PTV_150_250_1J', 'GG2HLL_PTV_150_250_GE2J', ] # 12

  MergeDict['ZH, p_{T}^{Z} > 250 GeV']   = [ 'QQ2HNUNU_PTV_250_400_0J', 'QQ2HNUNU_PTV_250_400_1J', 'QQ2HNUNU_PTV_250_400_GE2J', 'QQ2HNUNU_PTV_GT400_0J', 'QQ2HNUNU_PTV_GT400_1J', 'QQ2HNUNU_PTV_GT400_GE2J',
                                'QQ2HLL_PTV_250_400_0J', 'QQ2HLL_PTV_250_400_1J', 'QQ2HLL_PTV_250_400_GE2J', 'QQ2HLL_PTV_GT400_0J', 'QQ2HLL_PTV_GT400_1J', 'QQ2HLL_PTV_GT400_GE2J',
                                'GG2HNUNU_PTV_250_400_0J', 'GG2HNUNU_PTV_250_400_1J', 'GG2HNUNU_PTV_250_400_GE2J', 'GG2HNUNU_PTV_GT400_0J', 'GG2HNUNU_PTV_GT400_1J', 'GG2HNUNU_PTV_GT400_GE2J',
                                'GG2HLL_PTV_250_400_0J', 'GG2HLL_PTV_250_400_1J', 'GG2HLL_PTV_250_400_GE2J', 'GG2HLL_PTV_GT400_0J', 'GG2HLL_PTV_GT400_1J', 'GG2HLL_PTV_GT400_GE2J', ] # 24
   

  #MergeDict['UNKNOWN']        = [ 'UNKNOWN',]
  #MergeDict['TOTAL']          = [ 'TOTAL',]
  return MergeDict

def DefMergeScheme_22():
  MergeDict = collections.OrderedDict()
  MergeDict['qqZH_FWDH']           = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH' ] # 2
  MergeDict['qqZH_0_150PTV_0J'] = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_75_150_0J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_75_150_0J", ]
  MergeDict['qqZH_0_150PTV_GE1J'] = [ "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['qqZH_150_250PTV_0J'] = [ "QQ2HLL_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_0J", ]
  MergeDict['qqZH_150_250PTV_GE1J'] = [ "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['qqZH_GT250PTV_0J'] = [ "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_GT400_0J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_GT400_0J", ]
  MergeDict['qqZH_GT250PTV_GE1J'] = [ "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['qqWH_FWDH']           = [ 'QQ2HLNU_FWDH' ] # 1
  MergeDict['qqWH_0_150PTV_0J'] = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_75_150_0J", ]
  MergeDict['qqWH_0_150PTV_GE1J'] = [ "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['qqWH_150_250PTV_0J'] = [ "QQ2HLNU_PTV_150_250_0J", ]
  MergeDict['qqWH_150_250PTV_GE1J'] = [ "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", ]
  MergeDict['qqWH_GT250PTV_0J'] = [ "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_GT400_0J", ]
  MergeDict['qqWH_GT250PTV_GE1J'] = [ "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ggZH_FWDH']           = [ 'GG2HNUNU_FWDH', 'GG2HLL_FWDH' ] # 2
  MergeDict['ggZH_0_150PTV_0J'] = [ "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_75_150_0J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_75_150_0J", ]
  MergeDict['ggZH_0_150PTV_GE1J'] = [ "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ggZH_150_250PTV_0J'] = [ "GG2HLL_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_0J", ]
  MergeDict['ggZH_150_250PTV_GE1J'] = [ "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['ggZH_GT250PTV_0J'] = [ "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_GT400_0J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_GT400_0J", ]
  MergeDict['ggZH_GT250PTV_GE1J'] = [ "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]

  #MergeDict['UNKNOWN']        = [ 'UNKNOWN',]
  MergeDict['TOTAL']          = [ 'TOTAL',]
  return MergeDict

def DefMergeScheme_15():
  MergeDict = collections.OrderedDict()
  MergeDict['qqZH_FWDH']           = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH' ] # 2
  MergeDict['qqZH_0_150PTV'] = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_75_150_0J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['qqZH_150_250PTV_0J'] = [ "QQ2HLL_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_0J", ]
  MergeDict['qqZH_150_250PTV_GE1J'] = [ "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['qqZH_GT250PTV'] = [ "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_GT400_0J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_GT400_0J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['qqWH_FWDH']           = [ 'QQ2HLNU_FWDH' ] # 1
  MergeDict['qqWH_0_150PTV'] = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_75_150_0J", "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['qqWH_150_250PTV_0J'] = [ "QQ2HLNU_PTV_150_250_0J", ]
  MergeDict['qqWH_150_250PTV_GE1J'] = [ "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", ]
  MergeDict['qqWH_GT250PTV'] = [ "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_GT400_0J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ggZH_FWDH']           = [ 'GG2HNUNU_FWDH', 'GG2HLL_FWDH' ] # 2
  MergeDict['ggZH_0_150PTV_0J'] = [ "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_75_150_0J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_75_150_0J", "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ggZH_GT150PTV_0J'] = [ "GG2HLL_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_0J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_GT400_0J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_GT400_0J", ]
  MergeDict['ggZH_GT150PTV_GE1J'] = [ "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J",  "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]

  #MergeDict['UNKNOWN']        = [ 'UNKNOWN',]
  MergeDict['TOTAL']          = [ 'TOTAL',]
  return MergeDict


def DefMergeScheme_8():
  MergeDict = collections.OrderedDict()
  MergeDict['FWDH']      = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH', 'QQ2HLNU_FWDH', 'GG2HNUNU_FWDH', 'GG2HLL_FWDH' ] # 5
  MergeDict['ZvvH_0J']   = [ "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HNUNU_PTV_150_250_0J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_GT400_0J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_75_150_0J", "GG2HNUNU_PTV_150_250_0J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_GT400_0J", ]
  MergeDict['ZvvH_GE1J'] = [ "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['WlvH_0J']     = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_75_150_0J", "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_GT400_0J", ]
  MergeDict['WlvH_GE1J']   = [ "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ZllH_0J']   = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_75_150_0J", "QQ2HLL_PTV_150_250_0J", "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_GT400_0J", "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_75_150_0J", "GG2HLL_PTV_150_250_0J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_GT400_0J", ]
  MergeDict['ZllH_GE1J'] = [ "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", ]
  MergeDict['TOTAL']      = [ 'TOTAL',]
  return MergeDict

def DefMergeScheme_Central():
  MergeDict = collections.OrderedDict()
  MergeDict['FWDH']      = [ 'QQ2HNUNU_FWDH', 'QQ2HLL_FWDH', 'QQ2HLNU_FWDH', 'GG2HNUNU_FWDH', 'GG2HLL_FWDH' ] # 5
  MergeDict['WH_0_150PTV'] = [ "QQ2HLNU_PTV_0_75_0J", "QQ2HLNU_PTV_0_75_1J", "QQ2HLNU_PTV_0_75_GE2J", "QQ2HLNU_PTV_75_150_0J", "QQ2HLNU_PTV_75_150_1J", "QQ2HLNU_PTV_75_150_GE2J", ]
  MergeDict['WH_150_250PTV'] = [ "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_150_250_1J", "QQ2HLNU_PTV_150_250_GE2J", ]
  MergeDict['WH_GT250PTV'] = [ "QQ2HLNU_PTV_250_400_0J", "QQ2HLNU_PTV_250_400_1J", "QQ2HLNU_PTV_250_400_GE2J", "QQ2HLNU_PTV_GT400_0J", "QQ2HLNU_PTV_GT400_1J", "QQ2HLNU_PTV_GT400_GE2J", ]
  MergeDict['ZH_0_150PTV'] = [ "QQ2HLL_PTV_0_75_0J", "QQ2HLL_PTV_0_75_1J", "QQ2HLL_PTV_0_75_GE2J", "QQ2HLL_PTV_75_150_0J", "QQ2HLL_PTV_75_150_1J", "QQ2HLL_PTV_75_150_GE2J", "QQ2HNUNU_PTV_0_75_0J", "QQ2HNUNU_PTV_0_75_1J", "QQ2HNUNU_PTV_0_75_GE2J", "QQ2HNUNU_PTV_75_150_0J", "QQ2HNUNU_PTV_75_150_1J", "QQ2HNUNU_PTV_75_150_GE2J", "GG2HLL_PTV_0_75_0J", "GG2HLL_PTV_0_75_1J", "GG2HLL_PTV_0_75_GE2J", "GG2HLL_PTV_75_150_0J", "GG2HLL_PTV_75_150_1J", "GG2HLL_PTV_75_150_GE2J", "GG2HNUNU_PTV_0_75_0J", "GG2HNUNU_PTV_0_75_1J", "GG2HNUNU_PTV_0_75_GE2J", "GG2HNUNU_PTV_75_150_0J", "GG2HNUNU_PTV_75_150_1J", "GG2HNUNU_PTV_75_150_GE2J", ]
  MergeDict['ZH_150_250PTV'] = [ "QQ2HLL_PTV_150_250_0J", "QQ2HLL_PTV_150_250_1J", "QQ2HLL_PTV_150_250_GE2J", "QQ2HNUNU_PTV_150_250_0J", "QQ2HNUNU_PTV_150_250_1J", "QQ2HNUNU_PTV_150_250_GE2J", "GG2HLL_PTV_150_250_0J", "GG2HLL_PTV_150_250_1J", "GG2HLL_PTV_150_250_GE2J", "GG2HNUNU_PTV_150_250_0J", "GG2HNUNU_PTV_150_250_1J", "GG2HNUNU_PTV_150_250_GE2J", ]
  MergeDict['ZH_GT250PTV'] = [ "QQ2HLL_PTV_250_400_0J", "QQ2HLL_PTV_250_400_1J", "QQ2HLL_PTV_250_400_GE2J", "QQ2HLL_PTV_GT400_0J", "QQ2HLL_PTV_GT400_1J", "QQ2HLL_PTV_GT400_GE2J", "QQ2HNUNU_PTV_250_400_0J", "QQ2HNUNU_PTV_250_400_1J", "QQ2HNUNU_PTV_250_400_GE2J", "QQ2HNUNU_PTV_GT400_0J", "QQ2HNUNU_PTV_GT400_1J", "QQ2HNUNU_PTV_GT400_GE2J", "GG2HLL_PTV_250_400_0J", "GG2HLL_PTV_250_400_1J", "GG2HLL_PTV_250_400_GE2J", "GG2HLL_PTV_GT400_0J", "GG2HLL_PTV_GT400_1J", "GG2HLL_PTV_GT400_GE2J", "GG2HNUNU_PTV_250_400_0J", "GG2HNUNU_PTV_250_400_1J", "GG2HNUNU_PTV_250_400_GE2J", "GG2HNUNU_PTV_GT400_0J", "GG2HNUNU_PTV_GT400_1J", "GG2HNUNU_PTV_GT400_GE2J", ]
  MergeDict['TOTAL']      = [ 'TOTAL',]
  return MergeDict

def ConvertVectorTo1DHist(vector, xlabels, name):
  if len(xlabels) != len(vector):
    print 'Label length does not equal vector length!'
    print 'Existing convert!'
    exit()
  nbinsx = len(xlabels)-1 # we don't want to keep TOTAL
  n_vector   = len(vector)
  hist = ROOT.TH1D(name, name, nbinsx, 0, nbinsx);
  for x_bin in xrange(nbinsx):
    hist.SetBinContent(x_bin + 1, vector[x_bin])
    hist.GetXaxis().SetBinLabel(x_bin+1,xlabels[x_bin])
  return hist

def MergeVector(vector, xlabels_org, MergeDict):
  nbinsx_org = len(xlabels_org)
  #hist_cov = ROOT.TH2D( name, name, len(MergeDict.keys()), 0, len(MergeDict.keys()), len(Y_Axis), 0., len(Y_Axis) );
  merge_bins = []
  if debug:
    print 'original X bins:',nbinsx_org
  for x_index_cov,x_name_cov in enumerate(MergeDict.keys()):
    merge_bin_content = 0
    if debug: print 'Filling %s with index %d'%(x_name_cov,x_index_cov)
    for org_cell in MergeDict[x_name_cov]:
      x_index_org = xlabels_org.index(org_cell)
      merge_bin_content += vector[x_index_org]
    merge_bins.append(merge_bin_content)
  vector_merged = np.array(merge_bins)
  return vector_merged

def RegionNameChange(name):
  name_dict = {
    '1lep,2tag2jet,150<PTV,WhfSR':'1-lep,2-jet,p_{T}^{V,r}>150 GeV,SR',
    '1lep,2tag3jet,150<PTV,WhfSR':'1-lep,3-jet,p_{T}^{V,r}>150 GeV,SR',
    '0lep,2tag2jet,150<PTV,SR':'0-lep,2-jet,p_{T}^{V,r}>150 GeV,SR',
    '0lep,2tag3jet,150<PTV,SR':'0-lep,3-jet,p_{T}^{V,r}>150 GeV,SR',
    #'2lep,2tag2jet,75<PTV<150,SR', '2lep,2tag2jet,150<PTV,SR', 
    #'2lep,2tag3pjet,75<PTV<150,SR', '2lep,2tag3pjet,150<PTV,SR'
    '2lep,2tag2jet,150<PTV,SR':'2-lep,2-jet,p_{T}^{V,r}>150 GeV,SR',
    '2lep,2tag3pjet,150<PTV,SR':'2-lep,#geq3-jet,p_{T}^{V,r}>150 GeV,SR',
    '2lep,2tag2jet,75<PTV<150,SR':'2-lep,2-jet,75<p_{T}^{V,r}<150 GeV,SR',
    '2lep,2tag3pjet,75<PTV<150,SR':'2-lep,#geq3-jet,75<p_{T}^{V,r}<150 GeV,SR',
  }
  return name_dict[name]

def Merge2DHist(hist, xlabels_org, ylabels_org, name, MergeDict):
  Y_Axis                    = [
                               '1lep,2tag2jet,150<PTV,WhfSR', '1lep,2tag3jet,150<PTV,WhfSR', 
                               #'2lep,2tag2jet,75<PTV<150,SR', '2lep,2tag2jet,150<PTV,SR', 
                               #'2lep,2tag3pjet,75<PTV<150,SR', '2lep,2tag3pjet,150<PTV,SR'
                               '2lep,2tag2jet,75<PTV<150,SR',
                               '2lep,2tag3pjet,75<PTV<150,SR',
                               '2lep,2tag2jet,150<PTV,SR',
                               '2lep,2tag3pjet,150<PTV,SR',
                               '0lep,2tag2jet,150<PTV,SR', '0lep,2tag3jet,150<PTV,SR',
                              ]
  Y_AxisPrime               = []
  nbinsx_org = hist.GetNbinsX()
  nbinsy_org = hist.GetNbinsY()
  hist_cov = ROOT.TH2D( name, name, len(MergeDict.keys()), 0, len(MergeDict.keys()), len(Y_Axis), 0., len(Y_Axis) );
  if debug:
    print 'original X bins:',nbinsx_org
    print 'original Y bins:',nbinsy_org
  for y_index_cov,y_name_cov in enumerate(Y_Axis):
    y_index_org = ylabels_org.index(y_name_cov)
    #hist_cov.GetYaxis().SetBinLabel(y_index_cov,RegionNameChange(y_name_cov))
    Y_AxisPrime.append(RegionNameChange(y_name_cov))
    for x_index_cov,x_name_cov in enumerate(MergeDict.keys()):
      cov_bin_content = 0
      if debug: print 'Filling %s with index %d for region %s'%(x_name_cov,x_index_cov,y_name_cov)
      for org_cell in MergeDict[x_name_cov]:
        x_index_org = xlabels_org.index(org_cell)
        cov_bin_content += hist.GetBinContent(x_index_org+1, y_index_org+1)
      hist_cov.SetBinContent( x_index_cov + 1, y_index_cov + 1, cov_bin_content)
  return hist_cov,MergeDict.keys(),Y_AxisPrime

rootFile = ROOT.TFile(InitialYieldsFile)
#HIST = rootFile.Get(name)
for signame in signal_names:

  # find the initial binned histograms
  # ggZllH125_Weight0NOEW_Stage1Nicest
  histoname = signame+'_Nominal_Stage1'+SplitScheme 
  if debug: print ' The initial histogram is:',histoname
  if signame == 'qqWlvH125':
    print 'Reading initial file for qqWlvH125'
    hNameP = signame+'P_Nominal_Stage1'+SplitScheme
    hNameM = signame+'M_Nominal_Stage1'+SplitScheme
    histo_P = rootFile.Get(hNameP)
    histo_M = rootFile.Get(hNameM)
    HistRescale(histo_P,signame+'P')
    HistRescale(histo_M,signame+'M')
    if histo_P:
      print 'histo_P exists'
      if histo_M:
        print 'histo_M exists'
        histo_initial = clone(Merge_WpH_WmH(histo_P,histo_M))
    if histo_initial:
      print 'merge successfully'
      if debug: print 'The Entries of merged histogram: %d, and the total yields %f'%(histo_initial.GetEntries(),histo_initial.GetBinContent(82))
    else: print 'Merging is not successfully with %s and %s from %s'%(hNameP,hNameM,InitialYieldsFile)
  else:
    histo_initial = rootFile.Get(histoname)
    HistRescale(histo_initial,signame)
    if histo_initial:
      if debug: print 'The Entries of merged histogram: %d, and the total yields %f'%(histo_initial.GetEntries(),histo_initial.GetBinContent(82))
    else:
      print 'The initial histogram is %s not found in %s'%(histoname,InitialYieldsFile)

  # find the Recons histograms
  #histoname += '_RecoCategory'
  #histoname += 'Bin_RecoCategory_NoWeight_1lep'
  #histoname = signame+'_Stage1Bin_RecoMapEPS'+SplitScheme.replace('Nicest','Nicety')
  histoname = signame+'_Stage1Bin_RecoMapEPS'+SplitScheme
  if debug: print ' name of the recon. histo.:',histoname
  histo_reco = {}
  for name in ReconsYieldsFiles: 
    if debug: print ' name of the recon. file:',name
    recon_file = ROOT.TFile(ReconsYieldsFiles[name])
    h = recon_file.Get(histoname)
    if h :
      histo_reco[name]= clone(h)
      if debug: print ' Entries:',h.GetEntries()
    else :
      print '%s doesnt exist in the file %s'%(histoname,ReconsYieldsFiles[name])
      continue

  if len(histo_reco) == 0:
    if debug : print 'skip %s due to no histo found in any ReconsYieldsFiles'%signame
    continue
  sig = Signal(histo_initial, histo_reco, signame)
  sig.CheckTheBins()
  sigs.append(sig)

vectors = []
matrics = []
for sig in sigs:
  vectors.append(sig.GetVector())
  for m in sig.GetMatrices():
    if debug: print m.shape
    matrics.append(m)

yields = sum(matrics)
if debug: 
  print (yields)

# Yields
hist_yields = clone(ConvertMatrixTo2DHist(yields,'Yields'))

if debug:
  print 'The X-label',sigs[0].GetLabelX()
  print 'The Y-label',sigs[0].GetLabelY()

if RunSys:
  # create the syst yields
  hist_yields_Sys = clone(hist_yields)
  hist_yields_Sys.Reset()
  for ir,reg in enumerate(sigs[0].GetLabelY()):
    #print 'lept. channel: ',reg.split(',')[0]
    lept = reg.split(',')[0]
    tagnjet = reg.split(',')[1]
    ptv = reg.split(',')[2].replace('150<PTV','150ptv').replace('75<PTV<150','75_150ptv')
    SRCR = reg.split(',')[3]
    # get the name of the histograms in the region
    HistNameInRegion = tagnjet+'_'+ptv+'_'+SRCR
    #print HistNameInRegion
    recon_file = ROOT.TFile(ReconsYieldsFiles[lept])
    for it,truthBin in enumerate(sigs[0].GetLabelX()):
      #HistNameComplete = truthBin.replace('_','x')+'_'+HistNameInRegion+'_mva'+('R21' if (lept == '1lep') else '')
      HistNameComplete = 'Systematics/'+truthBin.replace('_','x')+'_'+HistNameInRegion+'_mva'+('R21' if (lept == '1lep') else '') + '_'+SysName
      hSys = recon_file.Get(HistNameComplete)
      if hSys :
        #print '%s exists in the file %s'%(HistNameComplete,ReconsYieldsFiles[lept])
        content = hSys.Integral()
        hist_yields_Sys.SetBinContent(it+1,ir+1,content)
  
  Draw2DPlot(hist_yields_Sys, sigs[0].GetLabelX(), sigs[0].GetLabelY(), 'YieldsSys'+SysName, 'YieldsSys'+SysName)

Draw2DPlot(hist_yields, sigs[0].GetLabelX(), sigs[0].GetLabelY(), 'Yields', 'Yields')

# purity
categories = yields.sum(axis=0)
if debug:
  print categories
  print 'shape of the categories: ',categories.shape
purity = yields/(categories*0.5)*100 # double couting due to the TOTL colum
hist_purity = clone(ConvertMatrixTo2DHist(purity,'Purity'))
Draw2DPlot(hist_purity, sigs[0].GetLabelX(),sigs[0].GetLabelY(), 'Purity', 'Purity')

# acceptance
print sum(vectors)
hist_initial=ConvertVectorTo1DHist(sum(vectors),sigs[0].GetLabelX(),'Initial')
Draw1DPlot(hist_initial, 'Initial'+('_Yields' if doYieldsForInitial else '_XS'))

if debug:
  print 'shape of the yields: ',yields.shape
  print 'shape of the vector: ',sum(vectors).shape
if doYieldsForInitial:
  acceptance = (yields.T/sum(vectors)).T*100
  hist_acc = clone(ConvertMatrixTo2DHist(acceptance,'Acceptance'))
  Draw2DPlot(hist_acc, sigs[0].GetLabelX(),sigs[0].GetLabelY(), 'Acceptance', 'Acceptance')
#c1.Delete()
#hist.Delete()

def RelativeTH2D(hist_nominal,hist_sys):
  nbinsx_nominal = hist_nominal.GetNbinsX()
  nbinsy_nominal = hist_nominal.GetNbinsY()

  nbinsx_syst = hist_sys.GetNbinsX()
  nbinsy_syst = hist_sys.GetNbinsY()

  if nbinsx_nominal != nbinsx_syst: 
    print 'Failed the nBinsX check for the RelativeTH2D(), exit'
    exit(0)

  if nbinsy_nominal != nbinsy_syst: 
    print 'Failed the nBinsY check for the RelativeTH2D(), exit'
    exit(0)
  hist_relative = clone(hist_nominal)
  hist_relative.Reset()
  for ir in range(0,nbinsy_nominal):
    for it in range(0,nbinsx_nominal):
      nominal = hist_nominal.GetBinContent(it+1,ir+1)
      syst = hist_sys.GetBinContent(it+1,ir+1)
      if nominal == 0:
        if syst != 0:
          print 'for bin (%i,%i) we have 0 nominal but non-zero syst'%(ir+1,it+1)
      else:
        relative = (1.0*syst/nominal-1.)*100.
        hist_relative.SetBinContent(it+1,ir+1,relative)
  return hist_relative

# merging session
def DoMergePlot(MergingScheme,suffix):
  hist_yields_merge, Label_X_cov, Label_Y_cov  = Merge2DHist(hist_yields, sigs[0].GetLabelX(), sigs[0].GetLabelY(), 'Yields'+'_'+suffix, MergingScheme)
  Draw2DPlot( hist_yields_merge, Label_X_cov, Label_Y_cov, "events", 'Yields'+'_'+suffix, 0., 100., 0., 80.)

  # add one for the hSys
  if RunSys:
    hist_yields_merge_Sys = Merge2DHist(hist_yields_Sys, sigs[0].GetLabelX(), sigs[0].GetLabelY(), 'YieldsSys'+'_'+suffix, MergingScheme)[0]
    Draw2DPlot( hist_yields_merge_Sys, Label_X_cov, Label_Y_cov, "events", 'Yields_'+SysName+'_'+suffix, 0., 100., 0., 80.)
    print 'Type of the hist_yields_merge_Sys', type(hist_yields_merge_Sys)
    hist_relative = clone(RelativeTH2D(hist_yields_merge,hist_yields_merge_Sys))
    #Draw2DPlot( hist_relative, Label_X_cov, Label_Y_cov, SysName+" relative (%)", 'YieldsRelative_'+SysName+'_'+suffix, -10., 10., -2., 2. )
    Draw2DPlot( hist_relative, Label_X_cov, Label_Y_cov, SysName+" relative (%)", 'YieldsRelative_'+SysName+'_'+suffix, yramin=-2., yramax=2.)
  
  hist_purity_cov, Label_X_cov, Label_Y_cov  = Merge2DHist(hist_purity, sigs[0].GetLabelX(), sigs[0].GetLabelY(), 'Purity'+'_'+suffix, MergingScheme)
  Draw2DPlot( hist_purity_cov, Label_X_cov, Label_Y_cov, "signal fraction (%)", 'Purity'+'_'+suffix, 0., 120., 0., 100.)
  
  yields_merge = Convert2DHistToMatrix(hist_yields_merge)
  print yields_merge
  vector_merge = MergeVector(sum(vectors), sigs[0].GetLabelX(), MergingScheme)

  # here draw the yield/cros section plot for the merged scheme
  hist_initial_cov = ConvertVectorTo1DHist(vector_merge, Label_X_cov,'Initial'+'_'+suffix)
  Draw1DPlot(hist_initial_cov, 'Initial'+'_'+suffix+('_Yields' if doYieldsForInitial else 'XS'))

  if doYieldsForInitial:
    accept_merge = (yields_merge.T/vector_merge).T*100
    print accept_merge
    hist_acc_merge = clone(ConvertMatrixTo2DHist(accept_merge,'Acceptance'+'_'+suffix))
    ROOT.gStyle.SetPaintTextFormat(".2f")
    Draw2DPlot(hist_acc_merge, Label_X_cov, Label_Y_cov, "acceptance #times efficiency (%)", 'Acceptance'+'_'+suffix, 0., 10., 0., 7.)
  # change back to .1f as the global setting
  ROOT.gStyle.SetPaintTextFormat(".1f")

# DoMergePlot(DefMergeScheme_15(),'Stage1')
# DoMergePlot(DefMergeScheme_Central(),'5POI')
DoMergePlot(DefMergeScheme_6(),'5POI')
DoMergePlot(DefMergeScheme_3_Rich(),'3POI')
