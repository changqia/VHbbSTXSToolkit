##########################################################################
#
# this ia actually an effective macro to make the fancy plot!!!!
#
##########################################################################
import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
import math
##########################################################################

gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
SetAtlasStyle()
gStyle.SetEndErrorSize(7.0)
gROOT.SetBatch(True)

#########################################################################
outDir="STXSFit.Full.v01/"
mainFolder=""
noInputs  =5

upperLim= 4
lowerLim= 0

#if len(sys.argv)!=1:
#    print " USAGE:  python NicePlot_SimpleMu.py <mode>"
#    print " at the moment Mode needs to be expanded "
#    exit(-1)

#if   sys.argv[1]=="Asimov":
#    isObserved=False
#elif sys.argv[1]=="Data":
#    isObserved=True
#else:
#    print "Parameter: "+sys.argv[1]+" NOT recognised .... use either Asimov/Data"
#    exit(-1)

def myTextBold(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(62);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

####################################################################################################
mode="4"
doVH=True
value=[]

mode=sys.argv[1]
doVH=True


if not doVH:
    ## in descending order
    ## please ignore the second argument, just set the title and the fileName
    value=[
        ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
        ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
        ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
        ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
    ]


    if mode=="-1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson/breakdown/muHatTable_SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVZ/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Ranking_fullRes_VHbbRun2_13TeV_Observed_Ranking_1_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNDBMVA.04_fullRes_VHbbRun2_13TeV_SNDBMVA.04_0_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
        ]

    upperLim=  8 ### newValerio
    lowerLim= -1 ### newValerio
                       
    if mode=="1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["ZZ  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWZZZ_fullRes_VHbbRun2_13TeV_ObservedWZZZ_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSMZZ.txt",1],
            ["WZ  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWZZZ_fullRes_VHbbRun2_13TeV_ObservedWZZZ_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSMWZ.txt",1],
        ]


    if mode=="2":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson/breakdown/muHatTable_SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVZ/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Ranking_fullRes_VHbbRun2_13TeV_Observed_Ranking_1_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNDBMVA.04_fullRes_VHbbRun2_13TeV_SNDBMVA.04_0_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    
    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: CBA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/1POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_1POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_1POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: CBA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: CBA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: CBA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]

    if mode=="4":
        value=[
            ["m_{{\ell}{\ell}} [GeV]",  0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["m_{\\mathscr{l}\\mathscr{l}} [GeV]",  0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ##["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
            
        ]            
        #upperLim=  8  ### newValerio
        #lowerLim= -1
        upperLim=  10
        lowerLim= -2
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
else:
    if mode=="30":
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/MU/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC/"
    if mode=="31":
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/MU/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC/"
    if mode=="32":
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/XS/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC/"
    if mode=="33":
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/XS/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC/"

Nobj=len(value)

####################################################################################################
os.system("mkdir -p "+outDir)
           
for obj in value:
    if "txt" not in obj[7]: continue
    theF=file(obj[7])
    lines=theF.readlines()
    index=obj[8]
    incremental=0
    mu=1
    errT_u=0
    errT_d=0
    errS_u=0
    errS_d=0
    readThis=False
    for line in lines:
        if "muhat" in line:
            incremental+=1
            if incremental==index: readThis=True
            else                 : readThis=False
            ##print line
            if readThis: mu=float(line.split(":")[1])
        elif "Total" in line and readThis:
            ##print line.split()
            errT_u=float(line.split()[2])
            errT_d=float(line.split()[4])
        elif "DataStat" in line and errS_u==0 and readThis:
            errS_u=float(line.split()[2])
            errS_d=float(line.split()[4])
    theF.close()
    obj[2]=mu
    obj[3]=errT_u
    obj[4]=errT_d
    obj[5]=errS_u
    obj[6]=errS_d

####################################################################################################################
    
for obj in value:
    print obj
    
print "info on final fit: "
print " Mu val: "+str( value[0][2] )
print " Stat Err:  UP= "+str( value[0][5] )+"   DO= "+str( value[0][6] )
print " Sys  Err:  UP= "+str( math.sqrt( value[0][3]*value[0][3]-value[0][5]*value[0][5]) )+"   DO= "+str( math.sqrt(value[0][4]*value[0][4] - value[0][6]*value[0][6]) )
print " TOT  Err:  UP= "+str( value[0][3] )+"   DO= "+str( value[0][4] )

    
#####################################################################################################################
value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )
if Nobj>4: value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

theHisto2=TH2D("plot2","plot2",20,lowerLim,upperLim,len(value),-0.5,len(value) )  ##0.5)
theHisto2.GetYaxis().SetTickSize(0)
theHisto2.GetYaxis().SetTickLength(0)
#if doVH: theHisto2.GetXaxis().SetTitle("#mu_{VH}^{b#bar{b}}")   ##"best fit #mu_{VH}^{b#bar{b}}"
#else   : theHisto2.GetXaxis().SetTitle("#mu_{VZ}^{b#bar{b}}")
if doVH: 
  if mode in ['32', '33']:
    theHisto2.GetXaxis().SetTitle("Best fit #sigma/#sigma_{SM} for m_{H}=125 GeV")
  elif mode in ['30', '31']:
    theHisto2.GetXaxis().SetTitle("Best fit #mu_{VH}^{b#bar{b}} for m_{H}=125 GeV")
else   : theHisto2.GetXaxis().SetTitle("Best fit #mu_{VZ}^{b#bar{b}}")

##best fit #mu=#sigma^{t#bar{t}H}/#sigma^{t#bar{t}H}_{SM} for m_{H}=125 GeV")
#theHisto2.GetYaxis().SetLabelSize(0.06)
theHisto2.GetXaxis().SetTitleOffset(1.0)
theHisto2.GetXaxis().SetTitleSize(0.06)
LabelSize = 0.06
if Nobj == 3 :
  LabelSize = 0.05
elif Nobj == 4 :
  LabelSize = 0.04
elif Nobj == 5 :
  LabelSize = 0.04
elif Nobj == 6 :
  LabelSize = 0.04
elif Nobj == 8 :
  LabelSize = 0.04
elif Nobj == 10 :
  LabelSize = 0.04
elif Nobj == 11 :
  LabelSize = 0.03 #0.03
theHisto2.GetYaxis().SetLabelSize(LabelSize)

count=0
for obj in value:
    if "Fake" in obj[0]: continue
    count+=1
    ##print "setting: "+str(obj[1])
    theHisto2.GetYaxis().SetBinLabel(count,obj[0])

c2 = TCanvas("test2","test2",800,600)
##c2 = TCanvas("test2","test2",600,600)
gPad.SetLeftMargin(0.22)
gPad.SetTopMargin(0.05)
theHisto2.Draw()

tmp5=TH1D("tmp5", "tmp5", 1,1,2)
tmp6=TH1D("tmp6", "tmp6", 1,1,2)
tmp5.SetLineColor(1)
tmp6.SetLineWidth(4)
tmp5.SetLineWidth(3)
colorS=8
if not doVH: colorS=kAzure+7##4
tmp6.SetLineColor(colorS)


g = TGraphAsymmErrors()
g.SetLineWidth(4)
g.SetMarkerColor(1)
g.SetMarkerStyle(20)
g.SetMarkerSize(1.6)

g2 = TGraphAsymmErrors()
g2.SetLineWidth(3)
g2.SetMarkerColor(1)
g2.SetMarkerStyle(20)
g2.SetMarkerSize(1.6)
g2.SetLineColor(colorS)

count=-1.0
for obj in value:
    count+=1.
    if "Fake" in obj[0]: continue
    scale=1.15 ## was 1.1
    if Nobj==4: scale=1.09  #was 1.05
    elif Nobj==5: scale=1.09
    elif Nobj==11: scale= 1.00
    elif Nobj>=6: scale=1.05
    g.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g.SetPointEXhigh( int(count), obj[3])
    g.SetPointEXlow(  int(count), obj[4])
    g.SetPointEYhigh( int(count), float(0) )
    g.SetPointEYlow(  int(count), float(0) )
    g2.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g2.SetPointEYhigh( int(count), float(0) )
    g2.SetPointEYlow(  int(count), float(0) )
    g2.SetPointEXhigh( int(count), obj[5] )
    g2.SetPointEXlow(  int(count), obj[6] )
    print " --> Setting point: "+str(obj[2])+"  +/- "+str(obj[3])
    sysUp=0
    if obj[3]*obj[3]-obj[5]*obj[5]>0:
        sysUp= math.sqrt(obj[3]*obj[3]-obj[5]*obj[5])
    sysDo=0
    if obj[4]*obj[4]-obj[6]*obj[6]>0:
        sysDo= math.sqrt(obj[4]*obj[4]-obj[6]*obj[6])

    mystring1 = "%.2f  {}^{+%.2f}_{#minus%.2f}" % (obj[2], obj[3], obj[4])
    #mystring2 ="{}^{+%.1f}_{#minus%.1f}  , {}^{+%.1f}_{#minus%.1f}            " % (obj[5],obj[6], sysUp, sysDo) ## add a
    mystring2 = "                    {}^{+%.2f}_{#minus%.2f} , " % (obj[5],obj[6]) ## add a
    mystring3 = "                             {}^{+%.2f}_{#minus%.2f}           " % (sysUp, sysDo) ## add a
    mystring4 = "                   (                 )         "

    textsize=0.051
    if mode=="3": textsize=0.044 
    offset=0
    startX=0.57
    slope=0.157 ## was0.16
    startY=0.22+slope*count+offset
    if Nobj==3: startY=0.23+0.20*count+offset #slope=0.21 ##was 0.2
    elif Nobj==4: startY=0.22+slope*count+offset
    elif Nobj==5: startY=0.20+0.115*count+offset
    elif Nobj==6: startY=0.19+0.100*count+offset
    elif Nobj==8: startY=0.18+0.079*count+offset
    elif Nobj==9: startY=0.18+0.075*count+offset
    elif Nobj==10: startY=0.18+0.066*count+offset
    elif Nobj==11:
      startY=0.18+0.058*count+offset
      textsize=0.04

    if mode=="3": startY=0.19+0.079*count+offset
    myTextBold(startX     ,startY,1, mystring1, textsize)  # 0.144
    myText(    startX+0.01,startY,1, mystring2, textsize)  
    myText(    startX+0.01,startY,1, mystring3, textsize)
    myText(    startX+0.01,startY,1, mystring4, textsize)

slope=0.12
if Nobj==3: slope=0.14
elif Nobj==4 : slope=0.12
elif Nobj==5 : slope=0.07
elif Nobj==6 : slope=0.07
elif Nobj==10 : slope=0.047
elif Nobj==11 : slope=0.0425

if mode=="3": slope=0.055

yValLabel=0.32+(len(value)-1)*slope#-0.02
if mode!="3":
    textsize=0.045
    myTextBold(0.65, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.76, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
else:
    textsize=0.040
    myTextBold(0.64, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.74, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
    
legend4=TLegend(0.25,0.77,0.57,0.87)
legend4.SetTextFont(42)
legend4.SetTextSize(0.047)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
legend4.SetNColumns(2)
legend4.AddEntry(tmp5,"Total" ,"l")
legend4.AddEntry(tmp6,"Stat.","l")
legend4.Draw("SAME")

subtract=+1.25
if Nobj>4: subtract+=1
line0=TLine(0., -0.5, 0., len(value)-subtract)
line0.SetLineWidth(3)
line0.SetLineStyle(3)
line0.SetLineColor(kGray+1)
if mode not in ['30', '31', '32', '33']:
  line0.Draw("SAMEL") 

line1=TLine(1., -0.5, 1., len(value)-subtract)
line1.SetLineWidth(2)
line1.SetLineColor(kBlack)
line1.Draw("SAMEL")

yVal=0.5
if mode=="3" : yVal=1.5
lineC=None
if mode=="3":
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal+0.05,theHisto2.GetXaxis().GetXmax(), yVal+0.05)
else:
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal,theHisto2.GetXaxis().GetXmax(), yVal)
lineC.SetLineWidth(2)
lineC.SetLineStyle(2)
lineC.SetLineColor(kBlack)
if mode in [ "28",  "29" ] :
  lineC.Draw("SAMEL")

#lall.Draw("SAME")
g.Draw("SAMEP")
g2.Draw("SAMEP")


#myText(0.15,0.65,1,"(13.3 fb^{-1} )",0.03)
#myText(0.15,0.50,1,"(13.2 fb^{-1} )",0.03)
#myText(0.15,0.34,1,"(13.2 fb^{-1} )",0.03)

##############ATLAS_LABEL(0.27,0.88,1)
##myText(     0.40,0.88,1,"Preliminary",0.05)

ATLAS_LABEL(0.22,0.88,1)
myText(     0.34,0.88,1,"Internal",0.05)
#if doVH: myText(  0.40,0.88,1,"VH Analysis",0.048)
#else   : myText(  0.40,0.88,1,"VZ Analysis",0.048)
if doVH: myText(  0.48,0.88,1,"VH, H(bb)",0.048)
else   : myText(  0.48,0.88,1,"VZ, Z(bb)",0.048)

##myText(     0.60,0.93,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt=3.2 fb^{-1}")
#myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}",0.045)
if mode in  ['30', '31', '32', '33']:
  myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 3000 fb^{-1}",0.045)
else:
  myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 79.8 fb^{-1}",0.045)
##8.3 fb^{-1}")

c2.Update()
if mode in ['32', '33']:
  name = "Plot_xs_"
elif mode in [ '30', '31']:
  name = "Plot_mu_"

if mode in ['31', '33']:
  name += 'Reduced'
elif mode in [ '30', '32']:
  name += 'Run2'



if doVH: name+="_VH"
else   : name+="_VZ"
c2.Print( outDir+name+".pdf" )
c2.Print( outDir+name+".eps" )
c2.Print( outDir+name+".png" )

#############################################################################
#############################################################################

