import group as group

group.GroupScheme.clear() # clear the scheme in group
# define the group scheme
group.GroupScheme['FWDH']                   = {'bin_type':'FWDH'}
group.GroupScheme['ZH_PTV_0_75']            = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':75}
group.GroupScheme['ZH_PTV_75_150']          = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':75, 'ptv_high':150}
group.GroupScheme['ZH_PTV_150_250']         = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250}
group.GroupScheme['ZH_PTV_GT250']           = {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':250}
group.GroupScheme['WH_PTV_0_150']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
group.GroupScheme['WH_PTV_150x250']         = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250}
group.GroupScheme['WH_PTV_GT250']           = {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
group.main(group.GroupScheme, outputForPlot = True)
