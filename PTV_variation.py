#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import numpy as np
import math
import colorsys
import random
import socket


MCType = 'mc16ad'


LocalRun = True
hostname =  socket.gethostname()
if hostname.startswith('lxplus'):
  LocalRun = False
  print 'Run on lxplus'
  print hostname
else:
  print 'You run locally, you need define the localtion of the inputs'
  print hostname

ROOT.gStyle.SetPaintTextFormat("1.2f")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

debug = True
logscale = True
drawsuf = 'E'

signal_names = [
  'qqZvvH125', 'qqWlvH125M', 'qqWlvH125P', 'qqZllH125', 'ggZllH125', 'ggZvvH125',
#  'qqZvvH125'
]

weight_var   = [
  '_Weight0NOEW', # nominal
#  '_PDF0',
#  '_PDF1',
#  '_PDF2',
#  '_PDF3',
#  '_PDF4',
#  '_PDF5',
#  '_PDF6',
#  '_PDF7',
#  '_PDF8',
#  '_PDF9',
#  '_PDF10',
#  '_PDF11',
#  '_PDF12',
#  '_PDF13',
#  '_PDF14',
#  '_PDF15',
#  '_PDF16',
#  '_PDF17',
#  '_PDF18',
#  '_PDF19',
#  '_PDF20',
#  '_PDF21',
#  '_PDF22',
#  '_PDF23',
#  '_PDF24',
#  '_PDF25',
#  '_PDF26',
#  '_PDF27',
#  '_PDF28',
#  '_PDF29',
#  '_QCD0',
#  '_QCD1',
#  '_QCD2',
#  '_QCD3',
#  '_QCD4',
#  '_QCD5',
#  '_QCD6',
#  '_QCD7',
#  '_alphaSdn',
#  '_alphaSup',
]

color_list = [
'#1f77b4',
'#aec7e8',
'#ff7f0e',
'#ffbb78',
'#2ca02c',
'#98df8a',
'#d62728',
'#ff9896',
'#9467bd',
'#c5b0d5',
'#8c564b',
'#c49c94',
'#e377c2',
'#f7b6d2',
'#7f7f7f',
'#c7c7c7',
'#bcbd22',
'#dbdb8d',
'#17becf',
'#9edae5',
'#393b79',
'#5254a3',
'#6b6ecf',
'#9c9ede',
'#637939',
'#8ca252',
'#b5cf6b',
'#cedb9c',
'#8c6d31',
'#bd9e39',
'#e7ba52',
'#e7cb94',
'#843c39',
'#ad494a',
'#d6616b',
'#e7969c',
'#7b4173',
'#a55194',
'#ce6dbd',
'#de9ed6',
#'#3182bd',
'#e6550d',
]

'''
def GetNColor(N):
  colors = []
  if debug:print 'You will have %d colors'%N
  for i in np.arange( 0., 360., 360./N):
    print 'hue %f'%i
    hue=i
    lightness = (50 + np.random.rand() * 10)/100.
    saturation = (90 + np.random.rand() * 10)/100.
    r,g,b=colorsys.hsv_to_rgb(hue,saturation,lightness)
    colorNum = ROOT.TColor.GetColor(r,g,b)
    if debug: print 'color number is: %d'%colorNum
    colors.append(colorNum)
  return colors
'''
    

#SplitScheme = 'Fine'
SplitScheme = 'Nicest'

# set the input files
if LocalRun:
  InitialYieldsFile = '/Users/changqiao/Workspace/STXS/DxAOD_output_stage16/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'
else:
  InitialYieldsFile = '/eos/user/c/changqia/STXS_VHbb/submitDir_Adv_'+MCType+'_ICHEP_update/hist-sample.root'


def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

def SetupTopFrame(hist):
  hist.GetYaxis().SetTitleOffset(1.0)
  hist.GetXaxis().SetTitleOffset(1.4)
  hist.SetLabelOffset(0.005)

  hist.GetYaxis().SetTitleSize(0.04)
  hist.GetXaxis().SetTitleSize(0.04)

  Size = 0.02
  hist.GetXaxis().SetLabelSize(Size)
  hist.GetYaxis().SetLabelSize(0.04)

def SetupTopFrameAdv(hist,xtitle,ytitle,maximum):
  hist_frame = clone(hist)
  hist_frame.SetTitle("")
  SetupTopFrame(hist_frame)
  hist_frame.GetXaxis().SetTitle(xtitle)
  hist_frame.GetYaxis().SetTitle(ytitle)
  if logscale :
    bit = math.floor(math.log10(maximum))
    factor = math.pow(10,math.floor(0.3*bit))
    maximum_log = maximum*factor
    print "raw maximum ",maximum
    print "scale factor ",factor
    print "log maximum ",maximum_log
    hist_frame.SetMaximum(maximum_log)
  else :
    hist_frame.SetMaximum(maximum*1.5)
  return hist_frame

def SetupPad(TopPad,BotPad):
  #TopPad.SetBottomMargin(0.02)
  #TopPad.SetTopMargin(0.05)
  TopPad.SetRightMargin(0.0)
  TopPad.SetBottomMargin(0.14)

  #BotPad.SetTopMargin(0.03)
  #BotPad.SetBottomMargin(0.3)
  BotPad.SetLeftMargin(0.0)
  BotPad.SetRightMargin(0.0)

def SetErrorZero(hist):
    Nbins = hist.GetXaxis().GetNbins()
    if debug:
      print 'NBins: ',Nbins
    for count in range(0, Nbins):
      hist.SetBinError(count+1,0.00000000000000001)
      #hist.SetBinError(count+1,0.)

can_width=2400
can_height = 1200
c1 = ROOT.TCanvas("Yields","Yields",can_width,can_height)
p_l = ROOT.TPad("pl","pl",0.,0.,0.9,1.)
p_r = ROOT.TPad("pr","pr",0.9,0.,1.,1.)
SetupPad(p_l,p_r)
p_r.Draw()
p_l.Draw()
if logscale : p_l.SetLogy()
HistVar={}
rootFile = ROOT.TFile(InitialYieldsFile)
leg = ROOT.TLegend( 0.2, 0.1, 0.8, 0.9 )
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextSize(0.08)
#HIST = rootFile.Get(name)
maximum  = -999
icolor   = 1
#color_list_adv = GetNColor(len(weight_var))
for var in weight_var:
  if debug: print 'collecting: ',var.replace('_','')
  HistSum = None
  for signame in signal_names:
    # find the initial binned histograms
    # ggZllH125_wEW_Stage1
    histoname = signame+var+'_Stage1'+SplitScheme
    if debug: print 'histname: %s'%histoname
    histo_initial = rootFile.Get(histoname)
    if debug: 
      print 'histname: %s with Entries %i'%(histoname, histo_initial.GetEntries())
      print 'histname: %s with Yields  %f'%(histoname, histo_initial.Integral())
    if HistSum == None: 
      print 'First for ',var.replace('_','')
      HistSum = clone(histo_initial)
    else : HistSum.Add(histo_initial)
  if debug:
    print 'Variation %s with Entries %i'%(var.replace('_',''), HistSum.GetEntries())
    print 'Variation %s with Yields  %f'%(var.replace('_',''), HistSum.Integral())
  if var == '':
    histName = 'Nominal'
  else:
    histName = var.replace('_','')
  SetErrorZero(HistSum)
  HistVar[histName]=HistSum
  #colorNum = color_list_adv[icolor-1]
  colorNum = ROOT.TColor.GetColor(color_list[icolor-1])
  HistSum.SetLineColor(colorNum)
  icolor=icolor+1
  if maximum < HistSum.GetMaximum() : maximum = HistSum.GetMaximum()
  leg.AddEntry(HistSum, histName, 'l')

HistFrame=SetupTopFrameAdv(HistVar['Weight0NOEW'],'Bins','Yields',maximum)
#HistFrame.SetLineWidth(0)
p_l.cd()
HistFrame.Draw(drawsuf)
for var in weight_var:
  if var == '':
    continue
  else:
    histName = var.replace('_','')
  if debug: 
    print 'Draw %s'%histName
  HistVar[histName].Draw(drawsuf+'same')

p_r.cd()
leg.Draw()

c1.Print('PTV_Var.pdf')
