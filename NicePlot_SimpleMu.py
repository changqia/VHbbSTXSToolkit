##########################################################################
#
# this ia actually an effective macro to make the fancy plot!!!!
#
##########################################################################
import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
import math
##########################################################################

gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
SetAtlasStyle()
gStyle.SetEndErrorSize(7.0)
gROOT.SetBatch(True)

#########################################################################
outDir="STXSFit.Full.v01/"
mainFolder=""
noInputs  =5

upperLim= 4
lowerLim= 0

#if len(sys.argv)!=1:
#    print " USAGE:  python NicePlot_SimpleMu.py <mode>"
#    print " at the moment Mode needs to be expanded "
#    exit(-1)

#if   sys.argv[1]=="Asimov":
#    isObserved=False
#elif sys.argv[1]=="Data":
#    isObserved=True
#else:
#    print "Parameter: "+sys.argv[1]+" NOT recognised .... use either Asimov/Data"
#    exit(-1)

def myTextBold(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(62);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

####################################################################################################
mode="4"
doVH=True
value=[]

mode=sys.argv[1]
if sys.argv[2]=="0":
    doVH=False


if not doVH:
    ## in descending order
    ## please ignore the second argument, just set the title and the fileName
    value=[
        ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
        ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
        ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
        ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
    ]


    if mode=="-1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson/breakdown/muHatTable_SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVZ/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Ranking_fullRes_VHbbRun2_13TeV_Observed_Ranking_1_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNDBMVA.04_fullRes_VHbbRun2_13TeV_SNDBMVA.04_0_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
        ]

    upperLim=  8 ### newValerio
    lowerLim= -1 ### newValerio
                       
    if mode=="1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["ZZ  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWZZZ_fullRes_VHbbRun2_13TeV_ObservedWZZZ_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSMZZ.txt",1],
            ["WZ  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWZZZ_fullRes_VHbbRun2_13TeV_ObservedWZZZ_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSMWZ.txt",1],
        ]


    if mode=="2":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson/breakdown/muHatTable_SM_2L_v66.YDBA.04_fullRes_VHbbRun2_13TeV_YDBA.04_2_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVZ/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Ranking_fullRes_VHbbRun2_13TeV_Observed_Ranking_1_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNDBMVA.04_fullRes_VHbbRun2_13TeV_SNDBMVA.04_0_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    
    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: CBA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/1POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_1POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_1POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: CBA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: CBA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: CBA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVZ/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VZ_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VZ_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]

    if mode=="4":
        value=[
            ["m_{{\ell}{\ell}} [GeV]",  0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["m_{\\mathscr{l}\\mathscr{l}} [GeV]",  0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ##["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed8POI_fullRes_VHbbRun2_13TeV_Observed8POI_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
            
        ]            
        #upperLim=  8  ### newValerio
        #lowerLim= -1
        upperLim=  10
        lowerLim= -2
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
else:
    value=[
        ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
        ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
        ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
        ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
    ]

    if mode=="-1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVZ/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YMVA.04_fullRes_VHbbRun2_13TeV_YMVA.04_2_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SM_2L_v66.YMVA.04_fullRes_VHbbRun2_13TeV_YMVA.04_2_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVZ/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Ranking_fullRes_VHbbRun2_13TeV_Observed_Ranking_1_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNDBMVA.04_fullRes_VHbbRun2_13TeV_SNDBMVA.04_0_125_Systs_use1tagFalse_mvadiboson_mode1_Asimov0_SigXsecOverSM.txt",1],
        ]

    upperLim=  8
    lowerLim= -1

    if mode=="1":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["ZH  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWHZH_fullRes_VHbbRun2_13TeV_ObservedWHZH_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMZH.txt",1],
            ["WH  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/WZ/muHatTable_SMVHVZ_LHCP17_MVA_v07.ObservedWHZH_fullRes_VHbbRun2_13TeV_ObservedWHZH_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMWH.txt",1],
        ]
    
    if mode=="2":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/2LAlone/breakdown/SM_2L_v66.YMVA.04_fullRes_VHbbRun2_13TeV_YMVA.04_2_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SM_2L_v66.YMVA.04_fullRes_VHbbRun2_13TeV_YMVA.04_2_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/1LAlone/MVAVH/muHatTable_SMVHVZ_LHCP17_MVA_v06.Observed_Shapes_fullRes_VHbbRun2_13TeV_Observed_Shapes_1_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/muHatTable_L0_v30MVA.SNVHMVA.04_fullRes_VHbbRun2_13TeV_SNVHMVA.04_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]

    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: CBA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/1POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_1POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_1POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: CBA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: CBA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: CBA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]


    if mode=="4":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
        ]
        upperLim=  10
        lowerLim= -2

    if mode=="5":
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/Results/fit_results_SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva/breakdown/"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["WH, Vp_{T} #in [150,250]" ,2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXSWH150x250PTV.txt",1],
            ["WH, Vp_{T} #in [250,#infty]" ,3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXSWHGT250PTV.txt",1],
            ["ZVV, Vp_{T} #in [150,250]" ,4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXSZVVH150x250PTV.txt",1],
            ["ZVV, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_SMVH_STXS_v04.FitS3.01_fullRes_VHbbRun2_13TeV_FitS3.01_01_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXSZVVHGT250PTV.txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1

    if mode=="6":
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/Results/fit_results_SMVH_STXS_v04.FitS2.04_fullRes_VHbbRun2_13TeV_FitS2.04_01_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS2.04_fullRes_VHbbRun2_13TeV_FitS2.04_01_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v04.FitS2.04_fullRes_VHbbRun2_13TeV_FitS2.04_01_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["WH, Vp_{T} #in [150,250], #geq1J",   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTVGE1J"   +".txt",1],
            ["WH, Vp_{T} #in [150,250], 0J",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV0J"     +".txt",1],

            ["WH, Vp_{T} #in [250,#infty], #geq1J",     3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTVGE1J"     +".txt",1],
            ["WH, Vp_{T} #in [250,#infty], 0J",       3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV0J"       +".txt",1],

            ["ZVV, Vp_{T} #in [150,250], #geq1J", 4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTVGE1J" +".txt",1],
            ["ZVV, Vp_{T} #in [150,250], 0J",   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTV0J"   +".txt",1],

            ["ZVV, Vp_{T} #in [250,#infty], #geq1J",   5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTVGE1J"   +".txt",1],
            ["ZVV, Vp_{T} #in [250,#infty], 0J",     5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTV0J"     +".txt",1],

#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  45
        lowerLim= -14
        
    if mode=="7":
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/fit_results_SMVH_STXS_v04.FitS3.FullL2.01_fullRes_VHbbRun2_13TeV_FitS3.FullL2.01_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS3.FullL2.01_fullRes_VHbbRun2_13TeV_FitS3.FullL2.01_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v04.FitS3.FullL2.01_fullRes_VHbbRun2_13TeV_FitS3.FullL2.01_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["ZVV, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTV"+".txt",1],
            ["ZVV, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTV"  +".txt",1],
            ["WH,  Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV"  +".txt",1],
            ["WH,  Vp_{T} #in [250,#infty]" ,3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV"    +".txt",1],
            ["ZLL, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLH150x250PTV"+".txt",1],
            ["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v01/"
    if mode=="8":
        txtpath = "../fit_results_SMVH_STXS_v04.FitS2.POINP.01_fullRes_VHbbRun2_13TeV_FitS2.POINP.01_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS2.POINP.01_fullRes_VHbbRun2_13TeV_FitS2.POINP.01_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v04.FitS2.POINP.01_fullRes_VHbbRun2_13TeV_FitS2.POINP.01_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["ZLL, Vp_{T} #in [150,250], #geq1J", 4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLH150x250PTVGE1J" +".txt",1],
            ["ZLL, Vp_{T} #in [150,250], 0J",   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLH150x250PTV0J"   +".txt",1],

            ["WH, Vp_{T} #in [150,250], #geq1J",   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTVGE1J"   +".txt",1],
            ["WH, Vp_{T} #in [150,250], 0J",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV0J"     +".txt",1],

            ["WH, Vp_{T} #in [250,#infty], #geq1J",     3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTVGE1J"     +".txt",1],
            ["WH, Vp_{T} #in [250,#infty], 0J",       3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV0J"       +".txt",1],

            ["ZVV, Vp_{T} #in [150,250], #geq1J", 4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTVGE1J" +".txt",1],
            ["ZVV, Vp_{T} #in [150,250], 0J",   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTV0J"   +".txt",1],

            ["ZVV, Vp_{T} #in [250,#infty], #geq1J",   5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTVGE1J"   +".txt",1],
            ["ZVV, Vp_{T} #in [250,#infty], 0J",     5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTV0J"     +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  45
        lowerLim= -14
        #outDir="STXSFit.S2.10POI2NP.v01/"
        outDir="STXSFit.Full.v01/"
    if mode=="9":
        txtpath = "../fit_results_SMVH_STXS_v04.FitS3.POINP.01_fullRes_VHbbRun2_13TeV_FitS3.POINP.01_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS3.POINP.01_fullRes_VHbbRun2_13TeV_FitS3.POINP.01_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v04.FitS3.POINP.01_fullRes_VHbbRun2_13TeV_FitS3.POINP.01_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["ZVV, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVH150x250PTV"+".txt",1],
            ["ZVV, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZVVHGT250PTV"  +".txt",1],
            ["WH,  Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV"  +".txt",1],
            ["WH,  Vp_{T} #in [250,#infty]" ,3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV"    +".txt",1],
            ["ZLL, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLH150x250PTV"+".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v01/"
    if mode=="10":
        txtpath = "../fit_results_SMVH_STXS_v04.FitS4.POINP.02_fullRes_VHbbRun2_13TeV_FitS4.POINP.02_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v04.FitS4.POINP.02_fullRes_VHbbRun2_13TeV_FitS4.POINP.02_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v04.FitS4.POINP.02_fullRes_VHbbRun2_13TeV_FitS4.POINP.02_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["ZH, Vp_{T} #in [75,150]" ,    4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH75x150PTV"+".txt",1],
            ["ZH, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHGT250PTV"  +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV"  +".txt",1],
            ["WH, Vp_{T} #in [250,#infty]" ,3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV"    +".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  30
        lowerLim= -1
        outDir="STXSFit.Full.v01/"

    if mode=="11":
        txtpath = "../fit_results_SMVH_STXS_v05.FitS4.POINP.05_fullRes_VHbbRun2_13TeV_FitS4.POINP.05_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v05.FitS4.POINP.05_fullRes_VHbbRun2_13TeV_FitS4.POINP.05_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v05.FitS4.POINP.05_fullRes_VHbbRun2_13TeV_FitS4.POINP.05_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["ZH, Vp_{T} #in [75,150]" ,    4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH75x150PTV"+".txt",1],
            ["ZH, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHGT250PTV"  +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH150x250PTV"  +".txt",1],
            ["WH, Vp_{T} #in [250,#infty]" ,3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHGT250PTV"    +".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  8
        lowerLim= -1
        outDir="STXSFit.Full.v01/"

    if mode=="12":
        txtpath = "../fit_results_SMVH_STXS_v05.FitS6.03_fullRes_VHbbRun2_13TeV_FitS6.03_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v05.FitS6.03_fullRes_VHbbRun2_13TeV_FitS6.03_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v05.FitS6.03_fullRes_VHbbRun2_13TeV_FitS6.03_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXSVH"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["VH, Vp_{T} #in [75,150]" ,    4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"75x150PTV"+".txt",1],
            ["VH, Vp_{T} #in [150,250]" ,   4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"150x250PTV"+".txt",1],
            ["VH, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GT250PTV"  +".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  8
        lowerLim= -1
        outDir="STXSFit.Full.v01/"
    if mode=="13":
        txtpath = "../fit_results_SMVH_STXS_v05.STAGE1.05_fullRes_VHbbRun2_13TeV_STAGE1.05_012_125_Systs_use1tagFalse_mva/SMVH_STXS_v05.STAGE1.05_fullRes_VHbbRun2_13TeV_STAGE1.05_012_125_Systs_use1tagFalse_mva/breakdown/muHatTable_SMVH_STXS_v05.STAGE1.05_fullRes_VHbbRun2_13TeV_STAGE1.05_012_125_Systs_use1tagFalse_mva_mode1_Asimov1_SigXsecOverSMSTXS"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["qqWH, Vp_{T} #in [0,150]", 11, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH0x150PTV" +".txt",1],
            ["qqWH, Vp_{T} #in [150,250], 0J",   10, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH150x250PTV0J"   +".txt",1],
            ["qqWH, Vp_{T} #in [150,250], #geq1J",   9, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH150x250PTVGE1J"   +".txt",1],
            ["qqWH, Vp_{T} #in [250,#infty]",     8, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWHGT250PTV"     +".txt",1],
            ["qqZH, Vp_{T} #in [0,150]", 7, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH0x150PTV" +".txt",1],
            ["qqZH, Vp_{T} #in [150,250], 0J",   6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH150x250PTV0J"   +".txt",1],
            ["qqZH, Vp_{T} #in [150,250], #geq1J",   5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH150x250PTVGE1J"   +".txt",1],
            ["qqZH, Vp_{T} #in [250,#infty]",     4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZHGT250PTV"     +".txt",1],
            ["ggZH, Vp_{T} #in [0,150]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZH0x150PTV" +".txt",1],
            ["ggZH, Vp_{T} #in [150,#infty], 0J",   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHGT150PTV0J"   +".txt",1],
            ["ggZH, Vp_{T} #in [150,#infty], #geq1J",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHGT150PTVGE1J"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 120
        lowerLim= -35
        #outDir="STXSFit.S2.10POI2NP.v01/"
        outDir="STXSFit.Full.v01/"

    if mode=="14":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov0_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["WH, Vp_{T} #in [0,150]" ,     1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx0x150PTV"  +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"

    if mode=="15":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["WH, Vp_{T} #in [0,150]" ,     1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx0x150PTV"  +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"

    if mode=="16":
        txtpath = "../EPS.mixed.STXS.01.Stable.02_fullRes_VHbbRun2_13TeV_Stable.02_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_3/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.02_fullRes_VHbbRun2_13TeV_Stable.02_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_3_mode1_Asimov1_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["qqWH, Vp_{T} #in [0,150]", 11, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH0x150PTV" +".txt",1],
            ["qqWH, Vp_{T} #in [150,250], 0J",   10, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH150x250PTV0J"   +".txt",1],
            ["qqWH, Vp_{T} #in [150,250], #geq1J",   9, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWH150x250PTVGE1J"   +".txt",1],
            ["qqWH, Vp_{T} #in [250,#infty]",     8, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQWHGT250PTV"     +".txt",1],
            ["qqZH, Vp_{T} #in [0,150]", 7, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH0x150PTV" +".txt",1],
            ["qqZH, Vp_{T} #in [150,250], 0J",   6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH150x250PTV0J"   +".txt",1],
            ["qqZH, Vp_{T} #in [150,250], #geq1J",   5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZH150x250PTVGE1J"   +".txt",1],
            #["qqZH, Vp_{T} #in [250,#infty]",     4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZHGT250PTV"     +".txt",1],
            ["ggZH, Vp_{T} #in [0,150]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZH0x150PTV" +".txt",1],
            ["ggZH, Vp_{T} #in [150,#infty], 0J",   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHGT150PTV0J"   +".txt",1],
            ["ggZH, Vp_{T} #in [150,#infty], #geq1J",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHGT150PTVGE1J"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 120
        lowerLim= -35
        #outDir="STXSFit.S2.10POI2NP.v01/"
        outDir="STXSFit.Full.v01/"

### compare with combination
    txtpath_com = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.VH/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.VH_mode2_Asimov0_SigXsecOverSM"

    if mode=="17":
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.WH.ZH/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.WH.ZH_mode2_Asimov0_SigXsecOverSM"
        value=[
            ["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com+"VH"  +".txt",1],
            ["ZH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH"  +".txt",1],
            ["WH",     3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"  +".txt",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"

    if mode=="18":
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.3ptvmus/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.3ptvmus_mode1_Asimov0_SigXsecOverSMVH"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com+"VH"  +".txt",1],
            ["VH, Vp_{T} #in [0,150]" ,    2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"0x150"+".txt",1],
            ["VH, Vp_{T} #in [150,250]" ,   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"150x250"+".txt",1],
            ["VH, Vp_{T} #in [250,#infty]" ,4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GT250"  +".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="19":
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.3ptvmus/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.3ptvmus_mode2_Asimov1_SigXsecOverSMVH"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com+"VH"  +".txt",1],
            ["VH, Vp_{T} #in [0,150]" ,    2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"0x150"+".txt",1],
            ["VH, Vp_{T} #in [150,250]" ,   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"150x250"+".txt",1],
            ["VH, Vp_{T} #in [250,#infty]" ,4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GT250"  +".txt",1],
            #["ZLL, Vp_{T} #in [250,#infty]" ,5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZLLHGT250PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="20":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.floatWH0x250/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.floatWH0x250_mode2_Asimov0_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com+"VH"  +".txt",1],
            ["WH, Vp_{T} #in [0,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx0x250PTV"+".txt",1],
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="21":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../fit_results_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.floatWH0x250/breakdown/muHatTable_EPS.mixed.STXS.02.Test.Jul.13_fullRes_VHbbRun2_13TeV_Test.Jul.13_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2.converted.floatWH0x250_mode2_Asimov1_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            ["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com+"VH"  +".txt",1],
            ["WH, Vp_{T} #in [0,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx0x250PTV"+".txt",1],
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="22":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../VHbb_3mu_from_BaseLine/plots/breakdown/muHatTable_mode2_Asimov1_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
            ["VH, Vp_{T} #in [250,#infty]", 1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"VHxGT250PTV"   +".txt",1],
            ["VH, Vp_{T} #in [150,250]" ,     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"VHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="23":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "../VHbb_3mu_from_BaseLine/plots/breakdown/muHatTable_mode2_Asimov0_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
            ["VH, Vp_{T} #in [250,#infty]", 1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"VHxGT250PTV"   +".txt",1],
            ["VH, Vp_{T} #in [150,250]" ,     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"VHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="STXSFit.Full.v02.new/"
    if mode=="24":
        txtpath = "/afs/cern.ch/work/g/gdigrego/Hbb_Pisa/WSMaker_VHbb_STXS/output/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.STXS_FitScheme3_fullRes_VHbb_STXS_FitScheme3_012_mc16ad_Systs_mva_STXS_FitScheme_3/plots/breakdown/muHatTable_mode10_Asimov1_SigXsecOverSM"
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        #txtpath = "/eos/user/c/changqia/STXS_VHbb/VHbb_5mu_from_BaseLine/plots/breakdown/muHatTable_mode2_Asimov1_SigXsecOverSM"
        value=[
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="NicePlot.STXS.v04/"
    if mode=="25":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "/eos/user/c/changqia/STXS_VHbb/VHbb_5mu_from_BaseLine/plots/breakdown/muHatTable_mode2_Asimov0_SigXsecOverSM"
        value=[
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="NicePlot.STXS.v04/"
    if mode=="26":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.Note.01_fullRes_VHbb_Note.01_012_mc16ad_Systs_mva_STXS_FitScheme_3_QCDUpdated_PDFUpdated/plots/breakdown/muHatTable_mode2_Asimov0_SigXsecOverSM"
        value=[
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="NicePlot.STXS.v05.ICHEP/"
    if mode=="27":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.Note.02_fullRes_VHbb_Note.02_012_mc16ad_Systs_mva_STXS_FitScheme_2_QCDUpdated_PDFUpdated/plots/breakdown/muHatTable_mode2_Asimov0_SigXsecOverSM"
        value=[
            ["ZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZH"   +".txt",1],
            ["WH",   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="NicePlot.STXS.v05.ICHEP/"
    if mode=="28":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath_com_local= '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/Reduced_1mu/muHatTable_mode10_Asimov1_SigXsecOverSM'
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/Reduced_3mu/muHatTable_mode10_Asimov1_SigXsecOverSM"
        value=[
            ["Comb.",  4, 1.0,  0.0, 0.0, 0.0, 0.0, txtpath_com_local +".txt",1],
            ["ggZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["qqZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="NicePlot.STXS.v05.ICHEP/"
    if mode=="29":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath_com_local= '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/Run2Sys_1mu/muHatTable_mode10_Asimov1_SigXsecOverSM'
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/Run2Sys_3mu/muHatTable_mode10_Asimov1_SigXsecOverSM"
        value=[
            ["Comb.",  4, 1.0,  0.0, 0.0, 0.0, 0.0, txtpath_com_local +".txt",1],
            ["ggZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["qqZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="NicePlot.STXS.v05.ICHEP/"
    if mode=="30":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath_com_local= '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/1mu/XS/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM'
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/MU/Run2/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
           # ["Comb.",  4, 1.0,  0.0, 0.0, 0.0, 0.0, txtpath_com_local +".txt",1],
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC_MU/"
    if mode=="31":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        #txtpath_com_local= '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/1mu/MU/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM'
        #txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/MU/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM"
        txtpath_com_local= '/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/1mu/XS/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM'
        txtpath = "/Users/changqiao/Documents/ANA-HIGG-2018-21-INT1/figures/VHbb/3mu/MU/Reduced/muHatTable_mode13_Asimov1_SigXsecOverSM"
        value=[
           # ["Comb.",  4, 1.0,  0.0, 0.0, 0.0, 0.0, txtpath_com_local +".txt",1],
            ["gg#rightarrowZH",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ggZH"   +".txt",1],
            ["WH",     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WH"+     ".txt",1],
            ["q#bar{q}#rightarrowZH",   1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"qqZH"   +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim= 3.5
        lowerLim= 0.3
        outDir="HL_LHC_MU/"
    if mode=="32":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "/Users/changqiao/Workspace/STXS/FitResult/Result_5POI_FromGiulia/breakdown/muHatTable_mode10_Asimov0_SigXsecOverSM"
        value=[
            ["WH, Vp_{T} #in [250,#infty]", 3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["WH, Vp_{T} #in [150,250]" ,   2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [250,#infty]", 6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHxGT250PTV"   +".txt",1],
            ["ZH, Vp_{T} #in [150,250]",    5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx150x250PTV"+".txt",1],
            ["ZH, Vp_{T} #in [0,150]",      4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"ZHx0x150PTV"  +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  7
        lowerLim= -1
        outDir="NicePlot.STXS.v05.ICHEP/"
    if mode=="33":
        #txtpath = "../EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2/breakdown/muHatTable_EPS.mixed.STXS.01.Stable.01_fullRes_VHbbRun2_13TeV_Stable.01_012_125_Systs_use1tagFalse_mva_STXS_FitScheme_2_mode1_Asimov1_SigXsecOverSM"
        txtpath = "/Users/changqiao/Workspace/STXS/Mu_9POI/DOMU/muHatTable_mode2_Asimov0_SigXsecOverSM"
        value=[
            ["qqWH [250,#infty]",   3, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHxGT250PTV"   +".txt",1],
            ["qqWH [150,250]" ,     2, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx150x250PTV"+".txt",1],
            ["qqWH [0,150]",        4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"WHx0x150PTV"  +".txt",1],
            ["qqZH [250,#infty]",   6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZHxGT250PTV"   +".txt",1],
            ["qqZH [150,250]",      5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZHx150x250PTV"+".txt",1],
            ["qqZH [0,150]",        4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"QQZHx0x150PTV"  +".txt",1],
            ["ggZH [250,#infty]",   6, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHxGT250PTV"   +".txt",1],
            ["ggZH [150,250]",      5, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHx150x250PTV"+".txt",1],
            ["ggZH [0,150]",        4, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"GGZHx0x150PTV"  +".txt",1],
#            ["FWDH" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"muHatTable_L0_v01_STXS.TestSplit.07_fullRes_VHbbRun2_13TeV_TestSplit.07_0_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSMQQ2HNUNUxFWDH.txt",1],
            #["Comb.",  1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath_com_local+"VH"  +".txt",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
#            ["" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, txtpath+"",1],
        ]
        upperLim=  80
        lowerLim= -30
        outDir="NicePlot.STXS.v05/"
Nobj=len(value)

####################################################################################################
os.system("mkdir -p "+outDir)
           
for obj in value:
    if "txt" not in obj[7]: continue
    theF=file(obj[7])
    lines=theF.readlines()
    index=obj[8]
    incremental=0
    mu=1
    errT_u=0
    errT_d=0
    errS_u=0
    errS_d=0
    readThis=False
    for line in lines:
        if "muhat" in line:
            incremental+=1
            if incremental==index: readThis=True
            else                 : readThis=False
            ##print line
            if readThis: mu=float(line.split(":")[1])
        elif "Total" in line and readThis:
            ##print line.split()
            errT_u=float(line.split()[2])
            errT_d=float(line.split()[4])
        elif "DataStat" in line and errS_u==0 and readThis:
            errS_u=float(line.split()[2])
            errS_d=float(line.split()[4])
    theF.close()
    obj[2]=mu
    obj[3]=errT_u
    obj[4]=errT_d
    obj[5]=errS_u
    obj[6]=errS_d

####################################################################################################################
    
for obj in value:
    print obj
    
print "info on final fit: "
print " Mu val: "+str( value[0][2] )
print " Stat Err:  UP= "+str( value[0][5] )+"   DO= "+str( value[0][6] )
print " Sys  Err:  UP= "+str( math.sqrt( value[0][3]*value[0][3]-value[0][5]*value[0][5]) )+"   DO= "+str( math.sqrt(value[0][4]*value[0][4] - value[0][6]*value[0][6]) )
print " TOT  Err:  UP= "+str( value[0][3] )+"   DO= "+str( value[0][4] )

    
#####################################################################################################################
value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )
if Nobj>4: value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

theHisto2=TH2D("plot2","plot2",20,lowerLim,upperLim,len(value),-0.5,len(value) )  ##0.5)
theHisto2.GetYaxis().SetTickSize(0)
theHisto2.GetYaxis().SetTickLength(0)
#if doVH: theHisto2.GetXaxis().SetTitle("#mu_{VH}^{b#bar{b}}")   ##"best fit #mu_{VH}^{b#bar{b}}"
#else   : theHisto2.GetXaxis().SetTitle("#mu_{VZ}^{b#bar{b}}")
if doVH: theHisto2.GetXaxis().SetTitle("Best fit #mu_{VH}^{b#bar{b}} for m_{H}=125 GeV")
else   : theHisto2.GetXaxis().SetTitle("Best fit #mu_{VZ}^{b#bar{b}}")

##best fit #mu=#sigma^{t#bar{t}H}/#sigma^{t#bar{t}H}_{SM} for m_{H}=125 GeV")
#theHisto2.GetYaxis().SetLabelSize(0.06)
theHisto2.GetXaxis().SetTitleOffset(1.0)
theHisto2.GetXaxis().SetTitleSize(0.06)
LabelSize = 0.06
if Nobj == 3 :
  LabelSize = 0.05
elif Nobj == 4 :
  LabelSize = 0.04
elif Nobj == 5 :
  LabelSize = 0.04
elif Nobj == 6 :
  LabelSize = 0.04
elif Nobj == 8 :
  LabelSize = 0.04
elif Nobj == 10 :
  LabelSize = 0.04
elif Nobj == 11 :
  LabelSize = 0.03 #0.03
theHisto2.GetYaxis().SetLabelSize(LabelSize)

count=0
for obj in value:
    if "Fake" in obj[0]: continue
    count+=1
    ##print "setting: "+str(obj[1])
    theHisto2.GetYaxis().SetBinLabel(count,obj[0])

c2 = TCanvas("test2","test2",800,600)
##c2 = TCanvas("test2","test2",600,600)
gPad.SetLeftMargin(0.22)
gPad.SetTopMargin(0.05)
theHisto2.Draw()

tmp5=TH1D("tmp5", "tmp5", 1,1,2)
tmp6=TH1D("tmp6", "tmp6", 1,1,2)
tmp5.SetLineColor(1)
tmp6.SetLineWidth(4)
tmp5.SetLineWidth(3)
colorS=8
if not doVH: colorS=kAzure+7##4
tmp6.SetLineColor(colorS)


g = TGraphAsymmErrors()
g.SetLineWidth(4)
g.SetMarkerColor(1)
g.SetMarkerStyle(20)
g.SetMarkerSize(1.6)

g2 = TGraphAsymmErrors()
g2.SetLineWidth(3)
g2.SetMarkerColor(1)
g2.SetMarkerStyle(20)
g2.SetMarkerSize(1.6)
g2.SetLineColor(colorS)

count=-1.0
for obj in value:
    count+=1.
    if "Fake" in obj[0]: continue
    scale=1.15 ## was 1.1
    if Nobj==4: scale=1.09  #was 1.05
    elif Nobj==5: scale=1.09
    elif Nobj==11: scale= 1.00
    elif Nobj>=6: scale=1.05
    g.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g.SetPointEXhigh( int(count), obj[3])
    g.SetPointEXlow(  int(count), obj[4])
    g.SetPointEYhigh( int(count), float(0) )
    g.SetPointEYlow(  int(count), float(0) )
    g2.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g2.SetPointEYhigh( int(count), float(0) )
    g2.SetPointEYlow(  int(count), float(0) )
    g2.SetPointEXhigh( int(count), obj[5] )
    g2.SetPointEXlow(  int(count), obj[6] )
    print " --> Setting point: "+str(obj[2])+"  +/- "+str(obj[3])
    sysUp=0
    if obj[3]*obj[3]-obj[5]*obj[5]>0:
        sysUp= math.sqrt(obj[3]*obj[3]-obj[5]*obj[5])
    sysDo=0
    if obj[4]*obj[4]-obj[6]*obj[6]>0:
        sysDo= math.sqrt(obj[4]*obj[4]-obj[6]*obj[6])

    mystring1 = "%.2f  {}^{+%.2f}_{#minus%.2f}" % (obj[2], obj[3], obj[4])
    #mystring2 ="{}^{+%.1f}_{#minus%.1f}  , {}^{+%.1f}_{#minus%.1f}            " % (obj[5],obj[6], sysUp, sysDo) ## add a
    mystring2 = "                    {}^{+%.2f}_{#minus%.2f} , " % (obj[5],obj[6]) ## add a
    mystring3 = "                             {}^{+%.2f}_{#minus%.2f}           " % (sysUp, sysDo) ## add a
    mystring4 = "                   (                 )         "

    textsize=0.051
    if mode=="3": textsize=0.044 
    offset=0
    startX=0.57
    slope=0.157 ## was0.16
    startY=0.22+slope*count+offset
    if Nobj==3: startY=0.23+0.20*count+offset #slope=0.21 ##was 0.2
    elif Nobj==4: startY=0.22+slope*count+offset
    elif Nobj==5: startY=0.20+0.115*count+offset
    elif Nobj==6: startY=0.19+0.100*count+offset
    elif Nobj==8: startY=0.18+0.079*count+offset
    elif Nobj==9: startY=0.18+0.072*count+offset
    elif Nobj==10: startY=0.18+0.066*count+offset
    elif Nobj==11:
      startY=0.18+0.058*count+offset
      textsize=0.04

    if mode=="3": startY=0.19+0.079*count+offset
    myTextBold(startX     ,startY,1, mystring1, textsize)  # 0.144
    myText(    startX+0.01,startY,1, mystring2, textsize)  
    myText(    startX+0.01,startY,1, mystring3, textsize)
    myText(    startX+0.01,startY,1, mystring4, textsize)

slope=0.12
if Nobj==3: slope=0.14
elif Nobj==4 : slope=0.12
elif Nobj==5 : slope=0.07
elif Nobj==6 : slope=0.07
elif Nobj==9 : slope=0.05
elif Nobj==10 : slope=0.047
elif Nobj==11 : slope=0.0425

if mode=="3": slope=0.055

yValLabel=0.32+(len(value)-1)*slope#-0.02
if mode!="3":
    textsize=0.045
    myTextBold(0.65, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.76, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
else:
    textsize=0.040
    myTextBold(0.64, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.74, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
    
legend4=TLegend(0.25,0.77,0.57,0.87)
legend4.SetTextFont(42)
legend4.SetTextSize(0.047)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
legend4.SetNColumns(2)
legend4.AddEntry(tmp5,"Total" ,"l")
legend4.AddEntry(tmp6,"Stat.","l")
legend4.Draw("SAME")

subtract=+1.25
if Nobj>4: subtract+=1
line0=TLine(0., -0.5, 0., len(value)-subtract)
line0.SetLineWidth(3)
line0.SetLineStyle(3)
line0.SetLineColor(kGray+1)
if mode not in ["28", "29", '30', '31' ]:
  line0.Draw("SAMEL") 

line1=TLine(1., -0.5, 1., len(value)-subtract)
line1.SetLineWidth(2)
line1.SetLineColor(kBlack)
line1.Draw("SAMEL")

yVal=0.5
if mode=="3" : yVal=1.5
lineC=None
if mode=="3":
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal+0.05,theHisto2.GetXaxis().GetXmax(), yVal+0.05)
else:
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal,theHisto2.GetXaxis().GetXmax(), yVal)
lineC.SetLineWidth(2)
lineC.SetLineStyle(2)
lineC.SetLineColor(kBlack)
if mode in [ "28",  "29" ] :
  lineC.Draw("SAMEL")

#lall.Draw("SAME")
g.Draw("SAMEP")
g2.Draw("SAMEP")


#myText(0.15,0.65,1,"(13.3 fb^{-1} )",0.03)
#myText(0.15,0.50,1,"(13.2 fb^{-1} )",0.03)
#myText(0.15,0.34,1,"(13.2 fb^{-1} )",0.03)

##############ATLAS_LABEL(0.27,0.88,1)
##myText(     0.40,0.88,1,"Preliminary",0.05)

ATLAS_LABEL(0.22,0.88,1)
myText(     0.34,0.88,1,"Internal",0.05)
#if doVH: myText(  0.40,0.88,1,"VH Analysis",0.048)
#else   : myText(  0.40,0.88,1,"VZ Analysis",0.048)
if doVH: myText(  0.48,0.88,1,"VH, H(bb)",0.048)
else   : myText(  0.48,0.88,1,"VZ, Z(bb)",0.048)

##myText(     0.60,0.93,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt=3.2 fb^{-1}")
#myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}",0.045)
if mode in  ["28", '29', '30', '31']:
  myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 3000 fb^{-1}",0.045)
else:
  myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 79.8 fb^{-1}",0.045)
##8.3 fb^{-1}")

c2.Update()
name="Plot_mu_"+str(mode)
if doVH: name+="_VH"
else   : name+="_VZ"
c2.Print( outDir+name+".pdf" )
c2.Print( outDir+name+".eps" )
c2.Print( outDir+name+".png" )

#############################################################################
#############################################################################

