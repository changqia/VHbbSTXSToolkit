#!/usr/bin/env python
import collections

debug = True

sample_template = [ # stage1++
"QQ2HLNUxFWDH",
"QQ2HLNUxPTVx0x75x0J",
"QQ2HLNUxPTVx0x75x1J",
"QQ2HLNUxPTVx0x75xGE2J",
"QQ2HLNUxPTVx75x150x0J",
"QQ2HLNUxPTVx75x150x1J",
"QQ2HLNUxPTVx75x150xGE2J",
"QQ2HLNUxPTVx150x250x0J",
"QQ2HLNUxPTVx150x250x1J",
"QQ2HLNUxPTVx150x250xGE2J",
"QQ2HLNUxPTVx250x400x0J",
"QQ2HLNUxPTVx250x400x1J",
"QQ2HLNUxPTVx250x400xGE2J",
"QQ2HLNUxPTVxGT400x0J",
"QQ2HLNUxPTVxGT400x1J",
"QQ2HLNUxPTVxGT400xGE2J",

"QQ2HLLxFWDH",
"QQ2HLLxPTVx0x75x0J",
"QQ2HLLxPTVx0x75x1J",
"QQ2HLLxPTVx0x75xGE2J",
"QQ2HLLxPTVx75x150x0J",
"QQ2HLLxPTVx75x150x1J",
"QQ2HLLxPTVx75x150xGE2J",
"QQ2HLLxPTVx150x250x0J",
"QQ2HLLxPTVx150x250x1J",
"QQ2HLLxPTVx150x250xGE2J",
"QQ2HLLxPTVx250x400x0J",
"QQ2HLLxPTVx250x400x1J",
"QQ2HLLxPTVx250x400xGE2J",
"QQ2HLLxPTVxGT400x0J",
"QQ2HLLxPTVxGT400x1J",
"QQ2HLLxPTVxGT400xGE2J",

"QQ2HNUNUxFWDH",
"QQ2HNUNUxPTVx0x75x0J",
"QQ2HNUNUxPTVx0x75x1J",
"QQ2HNUNUxPTVx0x75xGE2J",
"QQ2HNUNUxPTVx75x150x0J",
"QQ2HNUNUxPTVx75x150x1J",
"QQ2HNUNUxPTVx75x150xGE2J",
"QQ2HNUNUxPTVx150x250x0J",
"QQ2HNUNUxPTVx150x250x1J",
"QQ2HNUNUxPTVx150x250xGE2J",
"QQ2HNUNUxPTVx250x400x0J",
"QQ2HNUNUxPTVx250x400x1J",
"QQ2HNUNUxPTVx250x400xGE2J",
"QQ2HNUNUxPTVxGT400x0J",
"QQ2HNUNUxPTVxGT400x1J",
"QQ2HNUNUxPTVxGT400xGE2J",

"GG2HLLxFWDH",
"GG2HLLxPTVx0x75x0J",
"GG2HLLxPTVx0x75x1J",
"GG2HLLxPTVx0x75xGE2J",
"GG2HLLxPTVx75x150x0J",
"GG2HLLxPTVx75x150x1J",
"GG2HLLxPTVx75x150xGE2J",
"GG2HLLxPTVx150x250x0J",
"GG2HLLxPTVx150x250x1J",
"GG2HLLxPTVx150x250xGE2J",
"GG2HLLxPTVx250x400x0J",
"GG2HLLxPTVx250x400x1J",
"GG2HLLxPTVx250x400xGE2J",
"GG2HLLxPTVxGT400x0J",
"GG2HLLxPTVxGT400x1J",
"GG2HLLxPTVxGT400xGE2J",

"GG2HNUNUxFWDH",
"GG2HNUNUxPTVx0x75x0J",
"GG2HNUNUxPTVx0x75x1J",
"GG2HNUNUxPTVx0x75xGE2J",
"GG2HNUNUxPTVx75x150x0J",
"GG2HNUNUxPTVx75x150x1J",
"GG2HNUNUxPTVx75x150xGE2J",
"GG2HNUNUxPTVx150x250x0J",
"GG2HNUNUxPTVx150x250x1J",
"GG2HNUNUxPTVx150x250xGE2J",
"GG2HNUNUxPTVx250x400x0J",
"GG2HNUNUxPTVx250x400x1J",
"GG2HNUNUxPTVx250x400xGE2J",
"GG2HNUNUxPTVxGT400x0J",
"GG2HNUNUxPTVxGT400x1J",
"GG2HNUNUxPTVxGT400xGE2J",
]

# this records the signal characteristic of each bin
sample_info = {}

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    #WARNING = '\033[93m'
    WARNING = '\033[91m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def ParseJetInfo(name):
  jet_infi = name.startswith('GE')
  njet = int(name.replace('GE', '').replace('J',''))
  return njet,jet_infi

def ParseBinName(name):
  components = name.split('x')
  Production_mode = '' # can be QQ2HLNU etc.
  Bin_type  = '' # can be PTV or FWDH
  PTV_low   = -1
  PTV_high  = -1
  Njet      = -1
  PTV_infi  = False
  Njet_infi = False
  if debug:
    print 'Input name is %s'%name
    for i,c in enumerate(components):
      print 'the %d of the name is %s'%(i,c)
  if len(components) < 2:
    print 'The length after cutting at _ of the name %s is less than 2, exit!'%name
    exit()
  Production_mode = components[0]
  Bin_type = components[1]
  if Bin_type == 'FWDH': # so this is the forward, we don't care so much
    pass
  elif Bin_type != 'PTV': # this bin we care about
    print 'we do not understand this bin type: %s'%Bin_type
    exit()
  else: # this is PTV we know the, can be X_X or GTX
    if len(components) < 3:
      print 'The structure of the name is strange, I can not parse it: %s, exit!'%name
      exit()
    if components[2].startswith('GT'): # so it is GTX, so the low edge is X
      if len(components) == 3 or len(components) == 4:
        ''' components[2] after GT is low edge '''
        low_edge = components[2].replace('GT', '')
        PTV_low = int(low_edge)
        PTV_infi = True
        ''' TODO consider the PTV_high and Njet '''
        if len(components) == 4: # njet splitting
          jet_info = components[3]
          Njet,Njet_infi = ParseJetInfo(jet_info)
      else:
        print 'The structure with GT is strange, I can not parse it: %s, exit!'%name
        exit()
    else: # so it is X_X case
      if len(components) == 4 or len(components) == 5:
        PTV_low  = int(components[2])
        PTV_high = int(components[3])
        if len(components) == 5: # njet splitting
          jet_info = components[4]
          Njet,Njet_infi = ParseJetInfo(jet_info)
  return {'pro_mode':Production_mode, 'bin_type':Bin_type, 'ptv_low':PTV_low, 'ptv_high':PTV_high, 'njet':Njet, 'ptv_infi': PTV_infi, 'njet_infi':Njet_infi}



#req = {}
#req = {
#'pro_mode': ['',''],
#'ptv_low': -1,
#'pro_mode':'GG2HNUNU',
#'bin_type': 'FWDH',
#'ptv_high': -1,
#'ptv_infi': False,
#'njet_infi': False,
#'njet': -1
#}


def checkSamplePassReq(sam,req):
  InReq = True
  for key in req:
    if key == 'pro_mode':
      InReq = (InReq and sample_info[sam][key] in req[key])
    if key == 'bin_type':
      InReq = (InReq and sample_info[sam][key] == req[key])
    elif key == 'ptv_low':
      InReq = (InReq and sample_info[sam][key] >= req[key])
    elif key == 'ptv_high':
      InReq = (InReq and sample_info[sam][key] <= req[key] and sample_info[sam]['ptv_infi'] == False)
    elif key == 'njet':
      if 'njet_infi' in req.keys(): # mean it is a >=
        InReq = (InReq and sample_info[sam][key] >= req[key])
      else: # mean ==
        InReq = (InReq and sample_info[sam][key] == req[key])
  return InReq


GroupScheme = collections.OrderedDict()
#GroupScheme6 = {
#'ZHx0x150PTV':   {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150},
#'ZHx150x250PTV': {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250},
#'ZHxGT250PTV':     {'pro_mode':['QQ2HLL', 'QQ2HNUNU', 'GG2HLL', 'GG2HNUNU'], 'ptv_low':250,},
#'WHx0x150PTV':   {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150},
#'WHx150x250PTV': {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250},
#'WHxGT250PTV':     {'pro_mode':['QQ2HLNU'], 'ptv_low':250},
#}

GroupScheme14 = collections.OrderedDict() # 11 + 3 fwd
GroupScheme14['GGZHFWD'] =            {'pro_mode':['GG2HLL', 'GG2HNUNU',], 'bin_type':'FWDH'}
GroupScheme14['GGZH0x150PTV'] =       {'pro_mode':['GG2HLL', 'GG2HNUNU',], 'ptv_low':0, 'ptv_high':150}
GroupScheme14['GGZHGT150PTV0J'] =     {'pro_mode':['GG2HLL', 'GG2HNUNU',], 'ptv_low':150, 'njet':0}
GroupScheme14['GGZHGT150PTVGE1J'] =   {'pro_mode':['GG2HLL', 'GG2HNUNU',], 'ptv_low':150, 'njet':1, 'njet_infi': True}
GroupScheme14['QQWHFWD'] =            {'pro_mode':['QQ2HLNU'], 'bin_type':'FWDH'}
GroupScheme14['QQWH0x150PTV'] =       {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150}
GroupScheme14['QQWH150x250PTV0J'] =   {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0}
GroupScheme14['QQWH150x250PTVGE1J'] = {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True}
GroupScheme14['QQWHGT250PTV'] =       {'pro_mode':['QQ2HLNU'], 'ptv_low':250}
GroupScheme14['QQZHFWD'] =            {'pro_mode':['QQ2HLL', 'QQ2HNUNU',], 'bin_type':'FWDH'}
GroupScheme14['QQZH0x150PTV'] =       {'pro_mode':['QQ2HLL', 'QQ2HNUNU',], 'ptv_low':0, 'ptv_high':150}
GroupScheme14['QQZH150x250PTV0J'] =   {'pro_mode':['QQ2HLL', 'QQ2HNUNU',], 'ptv_low':150, 'ptv_high':250, 'njet':0}
GroupScheme14['QQZH150x250PTVGE1J'] = {'pro_mode':['QQ2HLL', 'QQ2HNUNU',], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True}
GroupScheme14['QQZHGT250PTV'] =       {'pro_mode':['QQ2HLL', 'QQ2HNUNU',], 'ptv_low':250}

#GroupScheme3 = collections.OrderedDict() # 18 + 4 fwd
#GroupScheme3['VH0x150PTV'] =       {'pro_mode':['GG2HLL', 'GG2HNUNU', 'QQ2HLNU', 'QQ2HLL', 'QQ2HNUNU'], 'ptv_low':0, 'ptv_high':150},
#GroupScheme3['VH150x250PTV'] =     {'pro_mode':['GG2HLL', 'GG2HNUNU', 'QQ2HLNU', 'QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250},
#GroupScheme3['VHGT250PTV'] =       {'pro_mode':['GG2HLL', 'GG2HNUNU', 'QQ2HLNU', 'QQ2HLL', 'QQ2HNUNU'], 'ptv_low':250,},
#
#GroupScheme18 = collections.OrderedDict() # 18 + 4 fwd
#GroupScheme18['qqZH0x150PTVx0J']=       {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':0, 'ptv_high':150,   'njet':0}
#GroupScheme18['qqZH0x150PTVxGE1J']=     {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':0, 'ptv_high':150,   'njet':1, 'njet_infi': True}
#GroupScheme18['qqZH150x250PTVx0J']=     {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0}
#GroupScheme18['qqZH150x250PTVxGE1J']=   {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True}
#GroupScheme18['qqZHGT250PTVx0J']=       {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':250, 'njet':0}
#GroupScheme18['qqZHGT250PTVxGE1J']=     {'pro_mode':['QQ2HLL', 'QQ2HNUNU'], 'ptv_low':250, 'njet':1, 'njet_infi': True}
#
#GroupScheme18['qqWH0x150PTVx0J']=       {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150,   'njet':0}
#GroupScheme18['qqWH0x150PTVxGE1J']=     {'pro_mode':['QQ2HLNU'], 'ptv_low':0, 'ptv_high':150,   'njet':1, 'njet_infi': True}
#GroupScheme18['qqWH150x250PTVx0J']=     {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0}
#GroupScheme18['qqWH150x250PTVxGE1J']=   {'pro_mode':['QQ2HLNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True}
#GroupScheme18['qqWHGT250PTVx0J']=       {'pro_mode':['QQ2HLNU'], 'ptv_low':250, 'njet':0}
#GroupScheme18['qqWHGT250PTVxGE1J']=     {'pro_mode':['QQ2HLNU'], 'ptv_low':250, 'njet':1, 'njet_infi': True}
#
#GroupScheme18['ggZH0x150PTVx0J']=       {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150,   'njet':0}
#GroupScheme18['ggZH0x150PTVxGE1J']=     {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':0, 'ptv_high':150,   'njet':1, 'njet_infi': True}
#GroupScheme18['ggZH150x250PTVx0J']=     {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':0}
#GroupScheme18['ggZH150x250PTVxGE1J']=   {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':150, 'ptv_high':250, 'njet':1, 'njet_infi': True}
#GroupScheme18['ggZHGT250PTVx0J']=       {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':250, 'njet':0}
#GroupScheme18['ggZHGT250PTVxGE1J']=     {'pro_mode':['GG2HLL', 'GG2HNUNU'], 'ptv_low':250, 'njet':1, 'njet_infi': True}

sample_req_info = dict()

# generate the grouping for workspace maker
def generate_group_for_wsm(RL,groups):
  for req in RL:
    print '    {"%s", {'%(req),
    for name in groups[req]:
      print '"%s",'%name,
    print '}},'

def generate_group_for_PlotAccPurYie(RL,groups):
  print '------------ Below shows the merging dictionary for PlotAccPurYie ------------'
  for req in RL:
    print 'MergeDict[\'%s\'] = ['%(req),
    for name in groups[req]:
      print '"%s",'%name,
    print ']'
  print '-------------------------------    end   -------------------------------------'


# generate_group_for_wsm(GroupScheme)
def FillGroups(GroupScheme,groups):
  for req in GroupScheme:
    sample_in_req = []
    for sam in sample_template:
      InThisReq = checkSamplePassReq(sam, GroupScheme[req])
      if InThisReq:
        if debug: print 'put %25s into group: %s'%(sam,req)
        sample_in_req.append(sam)
        #sample_req_info[sam]
      if sam in sample_req_info:
        sample_req_info[sam][req] = InThisReq
      else:
        sample_req_info[sam] = dict()
        sample_req_info[sam][req] = InThisReq
    groups[req] = sample_in_req

def CheckHowManySamplesInEachGroup(GroupScheme,Groups):
  for req in GroupScheme:
    print 'Group %20s has %2d samples:'%(req,len(Groups[req]))

def CheckNonePickedSampleOrOverlap(GroupScheme):
  # to check if all STXS bins are picked by our group
  for sam in sample_template:
    samInListOfReq = []
    # check non-picked sample
    for req in GroupScheme:
      if sample_req_info[sam][req]: samInListOfReq.append(req)
    if len(samInListOfReq) == 0: print bcolors.WARNING + 'Warning: no one picks %20s'%sam + bcolors.ENDC
    # check double-picked sample
    elif len(samInListOfReq) > 1:
      print bcolors.WARNING + 'ERROR::more than one pick %s, they are:'%sam + bcolors.ENDC
      for rq in samInListOfReq:
        print rq

def ParseSampleInfo():
  for sam in sample_template:
    info = ParseBinName(sam)
    if debug:
      print 'name: %s'%sam
      print info
    sample_info[sam] = info

def main(groupscheme, outputForPlot = False, outputForWSM = False):
  ParseSampleInfo()
  Groups = collections.OrderedDict()
  FillGroups(groupscheme,Groups)
  CheckHowManySamplesInEachGroup(groupscheme,Groups)
  CheckNonePickedSampleOrOverlap(groupscheme)
  if outputForPlot: generate_group_for_PlotAccPurYie(groupscheme,Groups)
  if outputForWSM:  generate_group_for_wsm(groupscheme,Groups)
  return Groups

if __name__ == '__main__':
  main(GroupScheme14, outputForPlot = True)
